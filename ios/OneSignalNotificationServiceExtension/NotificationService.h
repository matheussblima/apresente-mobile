//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Matheus Lima on 11/05/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
