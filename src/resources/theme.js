const theme = {
  primaryColor: '#0093D0',
  secondColor: '#F4F4F4',
  thirdColor: '#D3D2D2',
  fourthColor: '#FFFFFF',
  fifthColor: '#E8EEF4',
  sixthColor: '#454545',
  seventhColors: '#6E6F74',
  eighthColor: '#EEEEEE',
  ninethColor: '#F45',
  tenthColor: '#000',
  eleventhColor: '#F0F0F0',
  twelfthColor: '#E7235F',
  thirteenthColor: '#E3C000',
  fourteenthColor: '#9D9D9D',
  fiveteenthColor: '#FDF8DA',
  sixteenthColor: '#129F58',
  eightteenthColor: '#FFF3B4',
};

export default theme;
