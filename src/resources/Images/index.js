const logo = require('./logo.png');
const close = require('./close.png');
const trash = require('./delete.png');
const filter = require('./filter.png');
const search = require('./search.png');
const creditCard = require('./credit-card.png');
const creditCardColor = require('./credit-card-color.png');
const barcode = require('./barcode.png');
const back = require('./back.png');
const arrowRight = require('./arrow-right.png');
const tickets = require('./tickets.png');
const user = require('./user.png');
const compass = require('./compass.png');
const faq = require('./faq.png');
const term = require('./term.png');
const plus = require('./plus.png');
const more = require('./more.png');
const minus = require('./minus.png');
const like = require('./like.png');
const share = require('./share.png');
const commerce = require('./commerce.png');
const entertainment = require('./entertainment.png');
const smileys = require('./smileys.png');
const play = require('./play.png');
const home = require('./home.png');
const cloud = require('./cloud.png');
const ticket = require('./ticket.png');
const mastercard = require('./mastercard.png');
const amex = require('./amex.png');
const cardDefault = require('./cardDefault.png');
const discovery = require('./discovery.png');
const costs = require('./costs.png');
const visa = require('./visa.png');
const seta = require('./seta.png');
const unlike = require('./unlike.png');
const exemple = require('./exemple.png');
const instagram = require('./instagram.png');
const arrowLeft = require('./arrow_left.png');
const vazio = require('./vazio.png');
const facebook = require('./facebook.png');
const google = require('./google.png');
const pc = require('./computer.png');
const men = require('./men.png');
const woman = require('./woman.png');
const interna = require('./interna.png');
const dowloand = require('./dowloand.png');
const step1 = require('./step1.png');
const step2 = require('./step2.png');
const step3 = require('./step3.png');
const step4 = require('./step4.png');
const next = require('./next.png');
const smileysBad = require('./smileys-bad.png');

export default {
  logo,
  close,
  filter,
  trash,
  search,
  creditCard,
  barcode,
  smileysBad,
  back,
  arrowRight,
  tickets,
  user,
  creditCardColor,
  compass,
  faq,
  more,
  term,
  plus,
  minus,
  step1,
  step2,
  step3,
  step4,
  like,
  share,
  commerce,
  entertainment,
  smileys,
  play,
  home,
  cloud,
  ticket,
  mastercard,
  amex,
  cardDefault,
  discovery,
  visa,
  seta,
  unlike,
  arrowLeft,
  exemple,
  instagram,
  vazio,
  facebook,
  google,
  pc,
  men,
  woman,
  interna,
  dowloand,
  costs,
  next,
};
