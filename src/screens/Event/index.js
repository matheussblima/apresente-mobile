import React, {useState, useEffect} from 'react';
import {Share, alert} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import HTML from 'react-native-render-html';
import moment from 'moment';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import {useSelector, useDispatch} from 'react-redux';
import {Title, Subtitle, HeaderClose, Division, Avatar} from '../../components';
import {findTickets, findEventByCod} from '../../redux/events';
import {setCart} from '../../redux/cart';
import {setLikes} from '../../redux/like';
import {theme, images} from '../../resources';
import {
  SectionInfo,
  ImageEvent,
  OverlayImage,
  Header,
  Container,
  SectionInfoArtist,
  SectionInfoEvent,
  ContentArtistInfo,
  ButtonBayTicket,
  ButtonBayTicketInfo,
  ButtonBayTicketValue,
  SubHeader,
  SectionIcons,
  Icons,
  ButtonIcons,
} from './styles';
import {texts} from '../../config';
import Skeleton from './Skeleton';
import currencyFormat from '../../utility/currencyFormat';

const Event = props => {
  const dispatch = useDispatch();
  const [like, setLike] = useState(false);
  const {ticket, event, isSuccessEvent, isSuccessTicket} = useSelector(
    state => state.events,
  );
  const {eventLike} = useSelector(state => state.like);
  const [isFetch, setIsFetch] = useState(false);
  const eventParam = props.route.params;

  useEffect(() => {
    setLike(
      !!eventLike.find(
        likeValue => eventParam.event.codEvento === likeValue.codeEvent,
      ),
    );
  }, []);

  if (!isFetch) {
    dispatch(findTickets(eventParam.event.codEvento));
    dispatch(findEventByCod(eventParam.event.codEvento));
    setIsFetch(true);
  }

  const onShare = async () => {
    try {
      await Share.share({
        message: eventParam.event.link,
      });
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <Container>
      <ParallaxScroll
        isHeaderFixed={false}
        parallaxHeight={hp('40%')}
        parallaxBackgroundScrollSpeed={5}
        parallaxForegroundScrollSpeed={2.5}
        renderHeader={() => (
          <HeaderClose
            onPress={() => props.navigation.goBack()}
            colorIcon={theme.fourthColor}
          />
        )}
        renderParallaxBackground={() => (
          <>
            <OverlayImage />
            <ImageEvent
              source={{
                uri: eventParam.event.linkImage,
              }}
            />
          </>
        )}>
        <SectionInfo>
          <Header>
            <Title
              numberOfLines={2}
              align="left"
              margin="0"
              color={theme.sixthColor}>
              {eventParam.event.nome}
            </Title>
            <Subtitle
              numberOfLines={1}
              color={theme.sixthColor}
              align="left"
              margin="0">
              {moment(eventParam.event.data).format('dddd, DD MMM YYYY HH:mm')}
            </Subtitle>
          </Header>

          {isSuccessTicket && ticket.length > 0 ? (
            <SubHeader>
              <ButtonBayTicket
                onPress={() => {
                  dispatch(
                    setCart({event: eventParam.event, ticket: ticket[0]}),
                  );
                  props.navigation.navigate('Payment');
                }}>
                <ButtonBayTicketValue>
                  <Subtitle
                    weight="bold"
                    color={theme.fourthColor}
                    margin="0px"
                    align="left"
                    size={`${wp('4%')}`}
                    numberOfLines={1}>
                    {currencyFormat(parseFloat(ticket[0].valor), true)}
                  </Subtitle>
                </ButtonBayTicketValue>
                <ButtonBayTicketInfo>
                  <Subtitle
                    weight="bold"
                    color={theme.primaryColor}
                    margin="0px"
                    align="left"
                    size={`${wp('4%')}`}
                    numberOfLines={1}>
                    {texts.bayTicket}
                  </Subtitle>
                </ButtonBayTicketInfo>
              </ButtonBayTicket>
              <SectionIcons>
                <ButtonIcons
                  onPress={() => {
                    dispatch(setLikes(eventParam.event.codEvento));
                    setLike(!like);
                  }}>
                  <Icons
                    source={like ? images.like : images.unlike}
                    resizeMode="contain"
                  />
                </ButtonIcons>
                <ButtonIcons onPress={onShare}>
                  <Icons source={images.share} resizeMode="contain" />
                </ButtonIcons>
              </SectionIcons>
            </SubHeader>
          ) : (
            undefined
          )}

          <Division />

          {isSuccessEvent ? (
            <>
              <SectionInfoEvent>
                <ContentArtistInfo>
                  <Avatar
                    size={wp('12%')}
                    source={{
                      uri:
                        'https://noverbal.es/uploads/blog/rostro-de-un-criminal.jpg',
                    }}
                  />
                  <SectionInfoArtist>
                    <Subtitle
                      weight="bold"
                      color={theme.sixthColor}
                      margin={`0px 0px 0px ${wp('2%')}px`}
                      align="left"
                      numberOfLines={1}>
                      Murilo Couto
                    </Subtitle>
                    <Subtitle
                      size={wp('4%')}
                      margin={`0px 0px ${wp('2%')}px ${wp('2%')}px`}
                      color={theme.sixthColor}
                      align="left"
                      numberOfLines={1}>
                      26 Lives | 234 espectadores
                    </Subtitle>
                  </SectionInfoArtist>
                </ContentArtistInfo>

                <HTML
                  html={`<div style="font-size: ${wp('8%')}px; color=${
                    theme.sixthColor
                  };"${event.releaseWeb}</div>`}
                />
              </SectionInfoEvent>
            </>
          ) : (
            <Skeleton />
          )}
        </SectionInfo>
      </ParallaxScroll>
    </Container>
  );
};

export default Event;
