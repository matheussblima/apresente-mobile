import styled from 'styled-components/native';

export const SectionInfo = styled.View`
  flex: 1;
  z-index: 10;
  padding: 24px;
  position: relative;
  padding-bottom: 140px;
  background-color: ${props => props.theme.fourthColor};
`;

export const OverlayImage = styled.View`
  background-color: rgba(0, 0, 0, 0.4);
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 20;
`;

export const ImageEvent = styled.Image`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
`;

export const Header = styled.View``;

export const SectionButton = styled.View`
  position: absolute;
  z-index: 100;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 40px 50px;
`;

export const SectionInfoEvent = styled.View`
  margin-top: 16px;
`;

export const ContentArtistInfo = styled.View`
  flex: 1;
  overflow: hidden;
  flex-direction: row;
  display: flex;
  margin-bottom: 24px;
`;

export const SectionInfoArtist = styled.View`
  flex: 1;
  display: flex;
  margin-top: 8px;
  justify-content: center;
`;

export const ButtonBayTicket = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  margin-top: 16px;
  margin-bottom: 16px;
`;

export const ButtonBayTicketValue = styled.View`
  background-color: ${props => props.theme.primaryColor};
  padding: 8px;
  border-top-left-radius: 8px;
  border-bottom-left-radius: 8px;
`;

export const ButtonBayTicketInfo = styled.View`
  border-color: ${props => props.theme.primaryColor};
  border-width: 1px;
  padding: 8px;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
`;

export const Container = styled.View``;

export const SubHeader = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export const ButtonIcons = styled.TouchableOpacity``;
export const SectionIcons = styled.View`
  display: flex;
  flex-direction: row;
`;
export const Icons = styled.Image`
  width: 32px;
  height: 32px;
  margin-left: 10px;
`;
