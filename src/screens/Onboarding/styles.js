import styled from 'styled-components/native';

export const ContainerImage = styled.ImageBackground`
  padding: 10px 0px;
  flex: 1;
`;

export const Section = styled.View`
  margin-top: 20%;
`;

export const Header = styled.View`
  align-items: center;
  justify-content: center;
  margin-top: 20px;
`;

export const Span = styled.View`
  background-color: ${props => props.theme.twelfthColor};
  border-radius: 60px;
  width: 48%;
  padding-horizontal: 8px;
`;

export const ButtonCircle = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  border-width: 4px;
  border-color: ${props => props.theme.fourthColor}
  border-radius: 50px;
  height: 90px;
  width: 90px;
`;

export const Icon = styled.Image`
  width: 50px;
  height: 50px;
`;
