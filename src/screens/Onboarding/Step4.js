import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Container, Title, Logo, Content, Button} from '../../components';
import {images, theme} from '../../resources';
import {ContainerImage, Header, Section} from './styles';
import {texts} from '../../config';

export default ({navigation}) => {
  return (
    <ContainerImage source={images.step4}>
      <Container backgroundColor="transparent">
        <Header>
          <Logo />
        </Header>
        <Content>
          <Section>
            <Title
              size={wp('12%')}
              color={theme.secondColor}
              align="left"
              margin="0px">
              {texts.andAll}
            </Title>
            <Title
              color={theme.secondColor}
              size={wp('12%')}
              align="left"
              margin="0px">
              {texts.whant}
            </Title>
          </Section>
          <Section>
            <Button
              onPress={() => navigation.navigate('Home')}
              color={theme.fourthColor}
              margin="16px"
              type="borded"
              backgroundActive="rgba(255,255,255,0.4)"
              borderColor={theme.fourthColor}
              title={texts.start}
            />
          </Section>
        </Content>
      </Container>
    </ContainerImage>
  );
};
