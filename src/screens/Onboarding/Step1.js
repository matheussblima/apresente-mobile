import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Container, Title, Logo, Content, Button} from '../../components';
import {images, theme} from '../../resources';
import {
  ContainerImage,
  Header,
  Section,
  Span,
  ButtonCircle,
  Icon,
} from './styles';
import {texts} from '../../config';

export default ({navigation}) => {
  return (
    <ContainerImage source={images.step1}>
      <Container backgroundColor="transparent">
        <Header>
          <Logo />
        </Header>
        <Content>
          <Section>
            <Title
              size={wp('12%')}
              color={theme.secondColor}
              align="left"
              margin="0px">
              {texts.musicShow}
            </Title>
            <Span>
              <Title
                color={theme.secondColor}
                size={wp('12%')}
                align="left"
                margin="0px">
                {texts.live}
              </Title>
            </Span>
          </Section>
          <Section
            style={{
              alignItems: 'center',
            }}>
            <ButtonCircle onPress={() => navigation.navigate('Onboarding-2')}>
              <Icon source={images.next} resizeMode="contain" />
            </ButtonCircle>
            <Button
              onPress={() => navigation.navigate('Home')}
              color={theme.fourthColor}
              margin="16px"
              title="Pular"
              type="transparent"
            />
          </Section>
        </Content>
      </Container>
    </ContainerImage>
  );
};
