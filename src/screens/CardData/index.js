import React, {useState, useRef, useEffect} from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {ScrollView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import VMasker from 'vanilla-masker';
import DropdownAlert from 'react-native-dropdownalert';
import {setCard} from '../../redux/card';
import {
  Container,
  Card,
  TextInput,
  Content,
  Button,
  HeaderClose,
  Title,
  Switch,
  Division,
} from '../../components';
import {Header, SectionInput, SectionButton, Icon, Row} from './styles';
import {texts} from '../../config';
import {images, theme} from '../../resources';

const CardData = props => {
  const card = useSelector(state => state.card);
  const {previousScreen} = props.route.params || {};
  const dispatch = useDispatch();
  const [isFetchCard, setIsFetchCard] = useState(false);
  const [numberCard, setNumberCard] = useState('');
  const [cpf, setCpf] = useState('');
  const [cep, setCep] = useState('');
  const [nameCard, setNameCard] = useState('');
  const [validateCard, setValidateCard] = useState('');
  const [cvv, setCvv] = useState('');
  const [isDefault, setIsDefault] = useState(true);
  const dropDownAlertRef = useRef();

  useEffect(() => {
    if (isFetchCard) {
      if (card.isSuccess) {
        if (card.card.length > 0) {
          if (previousScreen !== 'Cards') {
            props.navigation.navigate('SelectCard');
          } else {
            props.navigation.goBack();
          }
        } else {
          props.navigation.goBack();
        }
        setIsFetchCard(false);
      } else if (!card.isSuccess && !card.isFetch) {
        setIsFetchCard(false);
        dropDownAlertRef.current.alertWithType(
          'error',
          texts.errorAuth,
          card.message,
        );
      }
    }
  }, [card]);

  const createCard = item => {
    dispatch(
      setCard({
        numberCard: item.numberCard.split(' ').join(''),
        nameCard: item.nameCard,
        month: item.validateCard.split('/')[0],
        year: item.validateCard.split('/')[1],
        cvv: item.cvv,
        isDefault: item.isDefault,
        cpf: item.cpf
          .split('.')
          .join('')
          .split('-')
          .join(''),
        cep: item.cep.split('-').join(''),
      }),
    );
    setIsFetchCard(true);
  };

  return (
    <>
      <Container>
        <HeaderClose
          onPress={() => {
            props.navigation.goBack();
          }}
        />
        <ScrollView>
          <Content>
            <Card>
              <Header>
                <Icon source={images.commerce} resizeMode="contain" />
                <Title
                  weight="normal"
                  color={theme.sixthColor}
                  margin="16px 0px 0px 0px">
                  {texts.infoDataPayment}
                </Title>
              </Header>
              <Division />
              <KeyboardAwareScrollView>
                <SectionInput>
                  <Title
                    align="left"
                    color={theme.sixthColor}
                    margin="16px 0px 0px 0px">
                    {texts.holder}
                  </Title>
                  <TextInput
                    autoCapitalize="characters"
                    placeholder={texts.nameCard}
                    keyboardType="default"
                    value={nameCard}
                    onChangeText={value => setNameCard(value)}
                  />
                  <TextInput
                    placeholder={texts.holderCPFCard}
                    value={cpf}
                    keyboardType="numeric"
                    onChangeText={value => {
                      const mask = VMasker.toPattern(value, '999.999.999-99');
                      setCpf(mask);
                    }}
                  />
                  <TextInput
                    placeholder={texts.holderCEPCard}
                    value={cep}
                    keyboardType="numeric"
                    onChangeText={value => {
                      const mask = VMasker.toPattern(value, '99999-999');
                      setCep(mask);
                    }}
                  />
                  <Title
                    align="left"
                    color={theme.sixthColor}
                    margin="16px 0px 0px 0px">
                    {texts.creditCard}
                  </Title>
                  <TextInput
                    placeholder={texts.numberCard}
                    value={numberCard}
                    keyboardType="numeric"
                    onChangeText={value => {
                      const mask = VMasker.toPattern(
                        value,
                        '9999 9999 9999 9999',
                      );
                      setNumberCard(mask);
                    }}
                  />
                  <Row>
                    <TextInput
                      placeholder={texts.validateCard}
                      keyboardType="numeric"
                      value={validateCard}
                      onChangeText={value => {
                        const mask = VMasker.toPattern(value, '99/99');
                        setValidateCard(mask);
                      }}
                    />
                    <TextInput
                      marginLeft="16px"
                      placeholder={texts.CVV}
                      keyboardType="numeric"
                      value={cvv}
                      onChangeText={value => setCvv(value)}
                    />
                  </Row>
                  <Switch
                    title={texts.turnDefault}
                    value={isDefault}
                    onValueChange={value => setIsDefault(value)}
                  />
                </SectionInput>
                <SectionButton>
                  <Button
                    loading={card.isFetch}
                    title={texts.writeData}
                    type="borded"
                    onPress={() => {
                      createCard({
                        numberCard,
                        validateCard,
                        cvv,
                        nameCard,
                        cpf,
                        cep,
                        isDefault,
                      });
                    }}
                  />
                </SectionButton>
              </KeyboardAwareScrollView>
            </Card>
          </Content>
        </ScrollView>
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};

export default CardData;
