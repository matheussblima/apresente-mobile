import styled from 'styled-components/native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export const Header = styled.View`
  align-items: center;
  margin-top: ${hp('3%')}px;
  margin-bottom: ${hp('3%')}px;
`;

export const SectionInput = styled.View`
  margin-top: ${hp('5%')}px;
`;

export const SectionButton = styled.View`
  margin: ${hp('5%')}px 0;
`;

export const Icon = styled.Image`
  width: ${wp('16%')}px;
  height: ${wp('16%')}px;
`;

export const Row = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
