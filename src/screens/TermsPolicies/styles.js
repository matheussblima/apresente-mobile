import styled from 'styled-components/native';

export const Header = styled.View``;

export const SectionInfo = styled.View`
  margin-top: 16px;
`;

export const ContainerList = styled.TouchableOpacity`
  margin: 8px 4px;
  padding: 8px 16px;
  border-radius: 8px;
`;
