import React, {useEffect} from 'react';
import {ScrollView} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {useSelector, useDispatch} from 'react-redux';
import HTML from 'react-native-render-html';
import Skeleton from './Skeleton';
import {Container, HeaderBack, Content, Title} from '../../components';
import {getTerms} from '../../redux/terms';
import {theme} from '../../resources';
import {Header, SectionInfo} from './styles';

const TermsPolicies = props => {
  const dispatch = useDispatch();
  const termInfo = useSelector(states => states.terms);

  useEffect(() => {
    dispatch(getTerms());
  }, [getTerms]);

  return (
    <Container>
      <HeaderBack
        onPress={() => {
          props.navigation.goBack();
        }}
      />
      <ScrollView>
        <Content>
          {termInfo.isSuccess ? (
            <>
              <Header>
                <Title margin="8px 0 0 0" align="left" color={theme.sixthColor}>
                  {termInfo.terms.nome}
                </Title>
              </Header>
              <SectionInfo>
                <HTML
                  html={`<div style="font-size: ${wp('5%')}; color=${
                    theme.sixthColor
                  };">${termInfo.terms.documento}</div>`}
                />
              </SectionInfo>
            </>
          ) : (
            <Skeleton />
          )}
        </Content>
      </ScrollView>
    </Container>
  );
};

export default TermsPolicies;
