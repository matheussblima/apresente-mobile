import styled from 'styled-components/native';

export const ContainerList = styled.TouchableOpacity`
  margin: 8px 4px;
  padding: 8px 16px;
  background-color: #fff;
  shadow-color: ${props => props.theme.sixthColor};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.22;
  shadow-radius: 2.22px;
  elevation: 3;
  border-radius: 8px;
`;

export const Status = styled.View`
  margin-top: 8px;
  border-radius: 10px;
  display: flex;
  flex-direction: row;
  background-color: ${props => props.theme.twelfthColor};
`;

export const SectionBtn = styled.View`
  display: flex;
  padding: 10px 0px;
  flex-direction: row;
`

export const SectionCadst = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding-top: 10px;
`

export const SectionList = styled.View``;

export const Header = styled.View``;
