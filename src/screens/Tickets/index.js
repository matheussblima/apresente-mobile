import React, { useEffect } from 'react';
import {
  FlatList,
  ScrollView,
  TouchableOpacity,
  Image,
  View,
} from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import Skeleton from './Skeleton';
import {
  Container,
  EventInfo,
  Subtitle,
  Title,
  Content,
  Button,
} from '../../components';
import { findOrders } from '../../redux/order';
import {
  ContainerList,
  Status,
  Header,
  SectionList,
  SectionBtn,
  SectionCadst,
} from './styles';
import { theme, images } from '../../resources';
import { texts } from '../../config';

const Tickets = props => {
  const dispatch = useDispatch();
  const order = useSelector(states => states.order);

  useEffect(() => {
    dispatch(findOrders());
  }, [findOrders]);

  const renderItem = ({ item }) => {
    return (
      <ContainerList
        activeOpacity={0.8}>
        <EventInfo
          image={{ uri: item.linkImage }}
          title={item.nameEvent}
          height={wp('57%')}
          overTitle={moment(item.dateEvent).format('dddd DD MMM YYYY - HH:mm')}
          subTitle="#988938239823"
          bordes
          autor={texts.autorization}>
          <TouchableOpacity onPress={() => props.navigation.navigate('Live')}>
            <Status>
              <Image style={{
                marginTop: "7%",
                marginLeft: "10%"
              }}
                source={images.seta}
              />
              <Subtitle
                weight="bold"
                margin="10px 5px 10px 13px"
                color={theme.fourthColor}
                size={`${wp('6%')}`}>
                {texts.watch}
              </Subtitle>
            </Status>
          </TouchableOpacity>
        </EventInfo>
      </ContainerList>
    );
  };

  const renderKeys = item => item.idOrder.toString();

  return (
    <Container>
      <Content>
        <Header>
          <SectionCadst>
            <Title
              size={wp('9%')}
              color={theme.primaryColor}
              align="left"
              margin="0px">
              {`${texts.yourComp}\n${texts.yourComp2}`}
            </Title>
          </SectionCadst>
          <SectionBtn>
            <Button
              height="26px"
              padding="1px 12px"
              margin="0px 10px 0px 0px"
              sizeText={`${wp('3.5%')}px`}
              title={texts.todas}
              type="borded"
              fontWeight="bold"
            />
            <Button
              height="26px"
              padding="1px 12px"
              margin="0px 10px 0px 0px"
              sizeText={`${wp('3.5%')}px`}
              title={texts.proximas}
              type="borded"
              fontWeight="bold"
            />
            <Button
              height="26px"
              padding="1px 12px"
              sizeText={`${wp('3.5%')}px`}
              title={texts.anteriores}
              type="borded"
              fontWeight="bold"
            />
          </SectionBtn>
        </Header>
      </Content>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Content>
          {order.isSuccess ? (
            <SectionList>
              {order.orders.length > 0 ? (
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={order.orders}
                  renderItem={items => renderItem(items)}
                  keyExtractor={renderKeys}
                />
              ) : (
                  <Subtitle align="center" color={theme.sixthColor}>
                    {texts.ticketEmpty}
                  </Subtitle>
                )}
            </SectionList>
          ) : (
              <Skeleton />
            )}
        </Content>
      </ScrollView>
    </Container>
  );
};

export default Tickets;
