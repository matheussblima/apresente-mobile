import styled from 'styled-components/native';
import { TextInput } from '../../components'

export const ContainerList = styled.View`
  margin: 8px 4px;
  padding: 8px 16px;
  background-color: #fff;
  shadow-color: ${props => props.theme.sixthColor};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.22;
  shadow-radius: 2.22px;
  elevation: 3;
  border-radius: 8px;
`;

export const SepareContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

export const Boxs = styled.View`
  padding: 8px 0px;
`

export const Status = styled.View`
  border-radius: 10px;
  margin-top: 10px;
  padding: 10px 10px;
  background-color: ${props => props.theme.fiveteenthColor};
`;

export const Comprovante = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-top: 10px;
  margin-bottom: 15px;
`

export const Redes = styled.View`
  display: flex;
  padding-top: 15px;
  padding-bottom: 10px;
  flex-direction: row;
  justify-content: space-between;
`

export const Borde = styled.View`
border-bottom-width: 1px
padding-bottom: ${props => props.paddingBott || '6px'};
border-color: ${props => props.borderColor || '#707070'}
`

export const Graphi = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 10px 0px;
`

export const ContainerBody = styled.View`
`

export const SectionBtn = styled.View`
  display: flex;
  padding: 15px 0px 10px 0px;
  flex-direction: row;
`

export const SectionCadst = styled.View`
  display: flex;
  flex-direction: row;
  padding-top: 10px;
  padding-bottom: 20px;
`

export const SectionPersonal = styled.View`
  display: flex;
  flex-direction: row;
  padding-bottom: 10px;
  border-bottom-width: 1px;
`
export const ListSect = styled.View`
  margin-left: 10px;
  width: 65%;
`

export const ListDate = styled.View`
  display: flex;
  flex-direction: row;
`

export const RedesFlay = styled.View`
  display: flex;
  flex-direction: row;
`

export const SectionList = styled.View`

`;

export const Pain = styled.View`
   display: flex;
   justify-content: space-between;
   flex-direction: row;
`
export const Tables = styled.View`
  display: flex;
   justify-content: space-between;
   flex-direction: row;
   padding-top: 10px;
   border-bottom-width: 1px;
   border-color: #707070;
   padding-bottom: 10px;
`

export const SectionChangeImage = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 20px;
  margin-bottom: 16px;
`;


export const Input = styled.TextInput`
 color: black;
 border-color: #707070;
 font-size: ${props => props.fontSize || '10px'};
 border-bottom-width: 1px;
 padding: ${props => props.padding || '12px 0px'};
 margin: 12px 0px;
`

export const Header = styled.View``;
