import React, { useEffect, useState } from 'react';
import RNPickerSelect from 'react-native-picker-select';
import ImagePicker from 'react-native-image-picker';
import { BarChart, StackedBarChart } from 'react-native-chart-kit';
import { ScrollView, TouchableOpacity, Image, Dimensions } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { useDispatch, useSelector } from 'react-redux';
import {
  Container,
  Subtitle,
  Content,
  HeaderDash,
  Button,
  Avatar,
} from '../../components';
import { findOrders } from '../../redux/order';
import {
  ContainerList,
  SectionList,
  SepareContainer,
  Boxs,
  ContainerBody,
  Status,
  Comprovante,
  Borde,
  Redes,
  RedesFlay,
  Graphi,
  Pain,
  Tables,
  Input,
  SectionChangeImage,
} from './styles';
import { theme, images } from '../../resources';
import { texts } from '../../config';

const Dashboard = props => {
  const dispatch = useDispatch();
  const [screen, setScreen] = useState(1);
  const [photo, setPhoto] = useState(null);

  useEffect(() => {
    dispatch(findOrders());
  }, [findOrders]);

  handleChoosePhoto = () => {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, response => {
      if (response.uri) {
        setPhoto(response.uri);
      }
    });
  };

  const data = {
    labels: ['January', 'February'],
    datasets: [
      {
        data: [70, 50],
      },
    ],
  };

  const chartConfig = {
    backgroundColor: 'white',
    backgroundGradientFrom: 'white',
    backgroundGradientTo: 'white',
    color: (opacity = 1) => `rgba(33, 119, 178, ${opacity})`,
    labelColor: () => 'rgba(33, 119, 178)',
    barPercentage: 2,
    showLegend: false,
    withVerticalLabels: false,
    withHorizontalLabels: false,
    showBarTops: false,
    useShadowColorFromDataset: false, // optional
  };

  const renderDash = () => {
    return (
      <ContainerBody>
        <HeaderDash
          title="Dashboard"
          back={() => props.navigation.goBack()}
          activeDash={theme.primaryColor}
          activeColorDash={theme.fourthColor}
          routePress={() => setScreen(1)}
          routePress2={() => setScreen(2)}
          routePress3={() => setScreen(3)}
          routePress4={() => props.navigation.navigate('Transmit')}
        />
        <SepareContainer>
          <ContainerList activeOpacity={0.8}>
            <Boxs>
              <Subtitle
                weight="normal"
                margin="0px"
                color={theme.tenthColor}
                size={`${wp('3.4%')}`}>
                Total de Ingressos
              </Subtitle>
              <Subtitle
                weight="bold"
                margin="0px"
                color={theme.sixthColor}
                size={`${wp('7%')}`}>
                245
              </Subtitle>
            </Boxs>
          </ContainerList>
          <ContainerList>
            <Boxs>
              <Subtitle
                weight="normal"
                margin="0px"
                color={theme.tenthColor}
                size={`${wp('3.4%')}`}>
                Faturamento Líquido
              </Subtitle>
              <Subtitle
                weight="bold"
                margin="0px"
                color={theme.sixthColor}
                size={`${wp('7%')}`}>
                R$300,00
              </Subtitle>
            </Boxs>
          </ContainerList>
        </SepareContainer>
        <Status>
          <Subtitle
            weight="normal"
            margin="0px"
            color={theme.sixthColor}
            size={`${wp('3.5%')}`}>
            Repasses sempre são realizados até 24 horas úteis após a realização
            da Live na conta bancária informar a em seu cadastro
          </Subtitle>
        </Status>
        <Comprovante>
          <Subtitle
            weight="normal"
            margin="0px"
            color={theme.sixthColor}
            size={`${wp('4%')}`}>
            Pagamento realizado:
          </Subtitle>
          <TouchableOpacity>
            <Subtitle
              weight="normal"
              margin="0px 3px"
              color={theme.primaryColor}
              size={`${wp('4%')}`}>
              ver comprovante
            </Subtitle>
          </TouchableOpacity>
        </Comprovante>
        <ContainerList>
          <Borde>
            <Subtitle
              weight="bold"
              margin="0px"
              color={theme.tenthColor}
              align="left"
              size={`${wp('4%')}`}>
              Acessos na página
            </Subtitle>
            <Subtitle
              weight="normal"
              margin="3px 0px"
              color={theme.sixthColor}
              align="left"
              size={`${wp('3.5%')}`}>
              Total de pessoas que entraram na página de compra da Live
            </Subtitle>
          </Borde>
          <Redes>
            <RedesFlay>
              <Image style={{ marginRight: 5 }} source={images.instagram} />
              <SectionList>
                <Subtitle
                  weight="normal"
                  margin="-3px 0px 0px 0px"
                  color={theme.sixthColor}
                  align="left"
                  size={`${wp('3%')}`}>
                  INSTAGRAM
                </Subtitle>
                <RedesFlay>
                  <Subtitle
                    weight="bold"
                    margin="0px 5px 0px 0px"
                    color={theme.sixthColor}
                    align="left"
                    size={`${wp('4%')}`}>
                    300.590
                  </Subtitle>
                  <Subtitle
                    weight="bold"
                    margin="0px"
                    color={theme.fourteenthColor}
                    align="left"
                    size={`${wp('4%')}`}>
                    83%
                  </Subtitle>
                </RedesFlay>
              </SectionList>
            </RedesFlay>
            <RedesFlay>
              <Image style={{ marginRight: 5 }} source={images.facebook} />
              <SectionList>
                <Subtitle
                  weight="normal"
                  margin="-3px 0px 0px 0px"
                  color={theme.sixthColor}
                  align="left"
                  size={`${wp('3%')}`}>
                  FACEBOOK
                </Subtitle>
                <RedesFlay>
                  <Subtitle
                    weight="bold"
                    margin="0px 5px 0px 0px"
                    color={theme.sixthColor}
                    align="left"
                    size={`${wp('4%')}`}>
                    300.590
                  </Subtitle>
                  <Subtitle
                    weight="bold"
                    margin="0px"
                    color={theme.fourteenthColor}
                    align="left"
                    size={`${wp('4%')}`}>
                    83%
                  </Subtitle>
                </RedesFlay>
              </SectionList>
            </RedesFlay>
          </Redes>
          <Redes>
            <RedesFlay>
              <Image style={{ marginRight: 5 }} source={images.google} />
              <SectionList>
                <Subtitle
                  weight="normal"
                  margin="-3px 0px 0px 0px"
                  color={theme.sixthColor}
                  align="left"
                  size={`${wp('3%')}`}>
                  GOOGLE
                </Subtitle>
                <RedesFlay>
                  <Subtitle
                    weight="bold"
                    margin="0px 5px 0px 0px"
                    color={theme.sixthColor}
                    align="left"
                    size={`${wp('4%')}`}>
                    300.590
                  </Subtitle>
                  <Subtitle
                    weight="bold"
                    margin="0px"
                    color={theme.fourteenthColor}
                    align="left"
                    size={`${wp('4%')}`}>
                    83%
                  </Subtitle>
                </RedesFlay>
              </SectionList>
            </RedesFlay>
            <RedesFlay>
              <Image style={{ marginRight: 5 }} source={images.vazio} />
              <SectionList>
                <Subtitle
                  weight="normal"
                  margin="-3px 0px 0px 0px"
                  color={theme.sixthColor}
                  align="left"
                  size={`${wp('3%')}`}>
                  OUTROS
                </Subtitle>
                <RedesFlay>
                  <Subtitle
                    weight="bold"
                    margin="0px 5px 0px 0px"
                    color={theme.sixthColor}
                    align="left"
                    size={`${wp('4%')}`}>
                    300.590
                  </Subtitle>
                  <Subtitle
                    weight="bold"
                    margin="0px"
                    color={theme.fourteenthColor}
                    align="left"
                    size={`${wp('4%')}`}>
                    83%
                  </Subtitle>
                </RedesFlay>
              </SectionList>
            </RedesFlay>
          </Redes>
          <Borde />
          <Graphi>
            <SectionList>
              <Subtitle
                weight="normal"
                margin="0px"
                color={theme.tenthColor}
                size={`${wp('3%')}`}>
                Total de acessos a página
              </Subtitle>
              <Subtitle
                weight="bold"
                margin="0px"
                color={theme.sixthColor}
                size={`${wp('7%')}`}>
                245
              </Subtitle>
            </SectionList>
            <SectionList>
              <Subtitle
                weight="normal"
                margin="0px"
                color={theme.tenthColor}
                size={`${wp('3%')}`}>
                Média de Visitas por dia
              </Subtitle>
              <Subtitle
                weight="bold"
                margin="0px"
                color={theme.sixthColor}
                size={`${wp('7%')}`}>
                245
              </Subtitle>
            </SectionList>
          </Graphi>
        </ContainerList>
        <ContainerList>
          <Graphi>
            <SectionList>
              <Subtitle
                weight="normal"
                margin="0px"
                color={theme.tenthColor}
                size={`${wp('3%')}`}>
                Total de vendas realizadas
              </Subtitle>
              <Subtitle
                weight="bold"
                margin="0px"
                color={theme.sixthColor}
                size={`${wp('7%')}`}>
                R$ 23.990
              </Subtitle>
            </SectionList>
            <SectionList>
              <Subtitle
                weight="normal"
                margin="0px"
                color={theme.tenthColor}
                size={`${wp('3%')}`}>
                Média de Vendas por dia
              </Subtitle>
              <Subtitle
                weight="bold"
                margin="0px"
                color={theme.sixthColor}
                size={`${wp('7%')}`}>
                R$ 346,00
              </Subtitle>
            </SectionList>
          </Graphi>
        </ContainerList>
      </ContainerBody>
    );
  };

  const renderParticipant = () => {
    return (
      <>
        <HeaderDash
          title="Participantes"
          back={() => props.navigation.goBack()}
          activePart={theme.primaryColor}
          activeColorPart={theme.fourthColor}
          routePress={() => setScreen(1)}
          routePress2={() => setScreen(2)}
          routePress3={() => setScreen(3)}
          routePress4={() => props.navigation.navigate('Live')}
        />
        <ContainerList>
          <BarChart
            style={{}}
            data={data}
            width={Dimensions.get('window').width - 80}
            height={220}
            withHorizontalLabels={false}
            showBarTops={false}
            chartConfig={chartConfig}
          />
          <RedesFlay
            style={{
              justifyContent: 'center',
              paddingTop: 15,
              paddingBottom: 10,
            }}>
            <Button
              onPress={props.routePress4}
              height="40px"
              padding="1px 7px"
              sizeText={`${wp('4.5%')}px`}
              title="Homens"
              backgroundActive={theme.primaryColor}
              color={theme.fourthColor}
              borderColor={theme.primaryColor}
              type="borded"
              fontWeight="bold"
            />
            <SectionList style={{ marginLeft: 10 }} />
            <Button
              onPress={props.routePress4}
              height="40px"
              padding="1px 7px"
              sizeText={`${wp('4.5%')}px`}
              title="Mulheres"
              backgroundActive={theme.twelfthColor}
              color={theme.fourthColor}
              borderColor={theme.twelfthColor}
              type="borded"
              fontWeight="bold"
            />
          </RedesFlay>
        </ContainerList>
        <SepareContainer>
          <ContainerList>
            <Boxs>
              <Pain>
                <Image source={images.men} style={{ marginRight: 6 }} />
                <SectionList>
                  <Subtitle
                    weight="normal"
                    margin="0px"
                    align="center"
                    color={theme.tenthColor}
                    size={`${wp('3.8%')}`}>
                    Men
                  </Subtitle>
                  <Subtitle
                    weight="bold"
                    margin="0px 0px 0px 5px"
                    color={theme.sixthColor}
                    size={`${wp('6%')}`}>
                    44%
                  </Subtitle>
                  <Subtitle
                    weight="normal"
                    margin="0px"
                    color={theme.tenthColor}
                    size={`${wp('3.8%')}`}>
                    1.900
                  </Subtitle>
                </SectionList>
              </Pain>
            </Boxs>
          </ContainerList>
          <ContainerList>
            <Boxs>
              <Pain>
                <Image source={images.woman} style={{ marginRight: 6 }} />
                <SectionList>
                  <Subtitle
                    weight="normal"
                    margin="0px"
                    color={theme.tenthColor}
                    size={`${wp('3.8%')}`}>
                    Woman
                  </Subtitle>
                  <Subtitle
                    weight="bold"
                    margin="0px 0px 0px 7px"
                    color={theme.sixthColor}
                    size={`${wp('6%')}`}>
                    44%
                  </Subtitle>
                  <Subtitle
                    weight="normal"
                    margin="0px"
                    color={theme.tenthColor}
                    size={`${wp('3.8%')}`}>
                    1.900
                  </Subtitle>
                </SectionList>
              </Pain>
            </Boxs>
          </ContainerList>
        </SepareContainer>
        <ContainerList>
          <Boxs>
            <RedesFlay
              style={{
                justifyContent: 'center',
                borderBottomWidth: 1,
                paddingBottom: 20,
                borderColor: '#BDBEBF',
              }}>
              <Image source={images.interna} style={{ marginRight: 15 }} />
              <SectionList>
                <Subtitle
                  weight="normal"
                  margin="0px"
                  color={theme.tenthColor}
                  size={`${wp('3.8%')}`}>
                  Orlando
                </Subtitle>
                <Subtitle
                  weight="bold"
                  margin="0px"
                  color={theme.sixthColor}
                  size={`${wp('6%')}`}>
                  56%
                </Subtitle>
                <Subtitle
                  weight="normal"
                  margin="0px"
                  color={theme.tenthColor}
                  size={`${wp('3.8%')}`}>
                  3.500
                </Subtitle>
              </SectionList>
            </RedesFlay>
            <Pain style={{ paddingTop: 10 }}>
              <Subtitle
                weight="normal"
                margin="0px"
                color={theme.tenthColor}
                size={`${wp('4%')}`}>
                Orlando
              </Subtitle>
              <Subtitle
                weight="bold"
                margin="0px"
                color={theme.tenthColor}
                size={`${wp('4%')}`}>
                3.500
              </Subtitle>
            </Pain>
            <Pain style={{ paddingTop: 10 }}>
              <Subtitle
                weight="normal"
                margin="0px"
                color={theme.tenthColor}
                size={`${wp('4%')}`}>
                New York
              </Subtitle>
              <Subtitle
                weight="bold"
                margin="0px"
                color={theme.tenthColor}
                size={`${wp('4%')}`}>
                3.500
              </Subtitle>
            </Pain>
          </Boxs>
        </ContainerList>
        <ContainerList>
          <Borde>
            <Subtitle
              weight="bold"
              margin="0px"
              color={theme.tenthColor}
              align="left"
              size={`${wp('4%')}`}>
              Lista dos Participantes
            </Subtitle>
            <Subtitle
              weight="normal"
              margin="3px 0px"
              color={theme.sixthColor}
              align="left"
              size={`${wp('3.5%')}`}>
              Avalie com atenção o perfil do seu público e direcione sua Live de
              forma a obter maior resultado
            </Subtitle>
          </Borde>
          <RedesFlay
            style={{
              justifyContent: 'flex-end',
              paddingTop: 15,
              paddingBottom: 25,
            }}>
            <Button
              onPress={props.routePress4}
              height="45px"
              padding="5px 10px"
              sizeText={`${wp('4.5%')}px`}
              title="Convidar Participante"
              type="borded"
              fontWeight="bold"
            />
            <Image
              source={images.dowloand}
              style={{
                marginLeft: 15,
                width: '16%',
                height: '100%',
              }}
            />
          </RedesFlay>
          <Borde>
            <Pain>
              <Subtitle
                weight="bold"
                margin="0px"
                color={theme.tenthColor}
                align="left"
                size={`${wp('4%')}`}>
                Nome
              </Subtitle>
              <Subtitle
                weight="bold"
                margin="0px"
                color={theme.tenthColor}
                align="left"
                size={`${wp('4%')}`}>
                Email
              </Subtitle>
              <Subtitle
                weight="bold"
                margin="0px"
                color={theme.tenthColor}
                align="left"
                size={`${wp('4%')}`}>
                Convidado
              </Subtitle>
            </Pain>
          </Borde>
          <Tables>
            <Subtitle
              style={{ width: '30%' }}
              weight="normal"
              margin="0px"
              color={theme.tenthColor}
              align="left"
              size={`${wp('3%')}`}>
              Antonio Roberto das Virgens
            </Subtitle>
            <Subtitle
              style={{ width: '41%' }}
              weight="normal"
              margin="0px"
              color={theme.tenthColor}
              align="left"
              size={`${wp('3%')}`}>
              antonio@gmail.com
            </Subtitle>
            <Button
              onPress={props.routePress4}
              height="26px"
              padding="1px 7px"
              sizeText={`${wp('3%')}px`}
              title="Sim"
              backgroundActive={theme.fourthColor}
              color={theme.sixteenthColor}
              borderColor={theme.sixteenthColor}
              type="borded"
              fontWeight="bold"
            />
          </Tables>
          <Tables>
            <Subtitle
              style={{ width: '30%' }}
              weight="normal"
              margin="0px"
              color={theme.tenthColor}
              align="left"
              size={`${wp('3%')}`}>
              Antonio Roberto das Virgens
            </Subtitle>
            <Subtitle
              style={{ width: '41%' }}
              weight="normal"
              margin="0px"
              color={theme.tenthColor}
              align="left"
              size={`${wp('3%')}`}>
              antonio@gmail.com
            </Subtitle>
            <Button
              onPress={props.routePress4}
              height="26px"
              padding="1px 7px"
              sizeText={`${wp('3%')}px`}
              title="Sim"
              backgroundActive={theme.fourthColor}
              color={theme.sixteenthColor}
              borderColor={theme.sixteenthColor}
              type="borded"
              fontWeight="bold"
            />
          </Tables>
        </ContainerList>
      </>
    );
  };

  const renderEdit = () => {
    return (
      <>
        <HeaderDash
          title="Editar Dados"
          back={() => props.navigation.goBack()}
          activeEdit={theme.primaryColor}
          activeColorEdit={theme.fourthColor}
          routePress={() => setScreen(1)}
          routePress2={() => setScreen(2)}
          routePress3={() => setScreen(3)}
          routePress4={() => props.navigation.navigate('Live')}
        />
        <ContainerList>
          <SectionList>
            <Subtitle
              weight="normal"
              margin="0px 0px -20px 0px"
              color={theme.sixthColor}
              align="left"
              fontStyle="italic"
              size={`${wp('4%')}`}>
              Nome da Live
            </Subtitle>
            <Input
              editable={theme.sixthColor}
              placeholder="Nome da Live"
              fontSize={wp('5%')}
              value="A MELHOR SEGUNDA FEIRA DO MUNDO"
              keyboardType="default"
              autoCapitalize="none"
            />
            <Subtitle
              weight="normal"
              margin="0px 0px -20px 0px"
              color={theme.sixthColor}
              align="left"
              fontStyle="italic"
              size={`${wp('4%')}`}>
              Data e Hora
            </Subtitle>
            <Input
              editable={theme.sixthColor}
              placeholder="Data e Hora"
              fontSize={wp('5%')}
              value="22 / 03 / 2020 às 22h40m"
              keyboardType="default"
              autoCapitalize="none"
            />
            <Subtitle
              weight="normal"
              margin="0px 0px -20px 0px"
              color={theme.sixthColor}
              align="left"
              fontStyle="italic"
              size={`${wp('4%')}`}>
              Valor
            </Subtitle>
            <Input
              editable={theme.sixthColor}
              placeholder="Valor"
              fontSize={wp('5%')}
              value="$ 9,90"
              keyboardType="default"
              autoCapitalize="none"
            />
            <Subtitle
              weight="normal"
              margin="0px 0px 3px 0px"
              color={theme.sixthColor}
              align="left"
              fontStyle="italic"
              size={`${wp('4%')}`}>
              Categoria
            </Subtitle>
            <RNPickerSelect
              placeholder={{}}
              style={{
                inputIOS: {
                  fontSize: 19,
                  color: 'black',
                  borderBottomWidth: 1,
                  paddingBottom: 10,
                  borderColor: '#707070',
                },
                inputAndroid: {
                  fontSize: 19,
                  color: '#707070',
                  borderBottomWidth: 1,
                  paddingBottom: 10,
                  borderColor: '#707070',
                },
              }}
              value="Axé"
              onValueChange={value => console.log(value)}
              items={[
                { label: 'AXE MUSIC', value: 'axe' },
                { label: 'SERTANEJO', value: 'Sertanejo' },
              ]}
            />
          </SectionList>
          <SectionChangeImage>
            {photo ? (
              <Avatar size={wp('20%')} source={{ uri: photo }} />
            ) : (
                <Avatar size={wp('20%')} />
              )
            }
            <Button
              sizeText={`${wp('4%')}px`}
              backgroundActive="transparent"
              title={texts.trocarImgs}
              type="transparent"
              onPress={() => handleChoosePhoto()}
              fontWeight="bold"
            />
          </SectionChangeImage>
          <SectionList style={{ marginTop: 30, marginBottom: 20 }}>
            <Button
              onPress={props.routePress4}
              height="45px"
              padding="5px 10px"
              sizeText={`${wp('4.5%')}px`}
              title="Salvar Alterações"
              type="borded"
              fontWeight="bold"
            />
          </SectionList>
        </ContainerList>
      </>
    );
  };

  return (
    <Container>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Content>
          <SectionList>
            {screen === 1 ? renderDash() : undefined}
            {screen === 2 ? renderParticipant() : undefined}
            {screen === 3 ? renderEdit() : undefined}
          </SectionList>
        </Content>
      </ScrollView>
    </Container>
  );
};

export default Dashboard;
