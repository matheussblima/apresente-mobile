import React, { useState } from 'react';
import { ScrollView, Modal, Image } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import RNPickerSelect from 'react-native-picker-select';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
  Container,
  Content,
  Title,
  HeaderBack,
  Card,
  TextInput,
  Avatar,
  Button,
  HeaderClose,
  Division,
} from '../../components';
import { texts } from '../../config';
import {
  Header,
  Row,
  Section,
  SectionMore,
  ItemModal,
  SectionTextsModal,
  SectionChangeImage,
  HeaderModal,
  IconModal,
} from './styles';
import { theme, images } from '../../resources';

export default ({ navigation }) => {
  const [network, setNetwork] = useState();
  const [knowMoreModal, setKnowMoreModal] = useState(false);
  const [photo, setPhoto] = useState(null);

  handleChoosePhoto = () => {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, response => {
      if (response.uri) {
        setPhoto(response.uri);
      }
    });
  };

  const renderModal = () => {
    return (
      <Modal visible={knowMoreModal}>
        <Container>
          <HeaderClose onPress={() => setKnowMoreModal(false)} />
          <ScrollView>
            <Content>
              <HeaderModal>
                <IconModal source={images.costs} resizeMode="contain" />
                <SectionTextsModal>
                  <Title
                    size={wp('6%')}
                    align="left"
                    weight="normal"
                    color={theme.tenthColor}
                    margin="0px">
                    {texts.undestendCosts}
                  </Title>
                  <Title
                    size={wp('3%')}
                    align="left"
                    weight="normal"
                    color={theme.tenthColor}
                    margin="8px 0px 16px 0px">
                    {texts.ourMission}
                  </Title>
                  <Title
                    size={wp('3%')}
                    align="left"
                    weight="normal"
                    color={theme.tenthColor}
                    margin="0px 0px 16px 0px">
                    {texts.forOurPartners}
                  </Title>
                </SectionTextsModal>
              </HeaderModal>
              <Section>
                <Card>
                  <Title
                    size={wp('6%')}
                    align="left"
                    color={theme.tenthColor}
                    margin="0px">
                    {texts.economicHour}
                  </Title>
                  <Row style={{ marginTop: 16, marginBottom: 16 }}>
                    <ItemModal>
                      <Title
                        size={wp('3%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        {texts.valueTicket.toUpperCase()}
                      </Title>
                      <Title
                        size={wp('6%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        $9,99
                      </Title>
                    </ItemModal>
                    <ItemModal>
                      <Title
                        size={wp('3%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        {texts.youReciver.toUpperCase()}
                      </Title>
                      <Title
                        size={wp('6%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        $5,58
                      </Title>
                    </ItemModal>
                  </Row>
                  <Division />
                  <Title
                    size={wp('6%')}
                    align="left"
                    color={theme.tenthColor}
                    margin="0px">
                    {texts.economicTwoHour}
                  </Title>
                  <Row style={{ marginTop: 16, marginBottom: 16 }}>
                    <ItemModal>
                      <Title
                        size={wp('3%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        {texts.valueTicket.toUpperCase()}
                      </Title>
                      <Title
                        size={wp('6%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        $19,99
                      </Title>
                    </ItemModal>
                    <ItemModal>
                      <Title
                        size={wp('3%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        {texts.youReciver.toUpperCase()}
                      </Title>
                      <Title
                        size={wp('6%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        $11,49
                      </Title>
                    </ItemModal>
                  </Row>
                </Card>
              </Section>
              <Section>
                <Card>
                  <Title
                    size={wp('6%')}
                    align="left"
                    color={theme.tenthColor}
                    margin="0px">
                    {texts.basicHour}
                  </Title>
                  <Row style={{ marginTop: 16, marginBottom: 16 }}>
                    <ItemModal>
                      <Title
                        size={wp('3%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        {texts.valueTicket.toUpperCase()}
                      </Title>
                      <Title
                        size={wp('6%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        $19,99
                      </Title>
                    </ItemModal>
                    <ItemModal>
                      <Title
                        size={wp('3%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        {texts.youReciver.toUpperCase()}
                      </Title>
                      <Title
                        size={wp('6%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        $14,68
                      </Title>
                    </ItemModal>
                  </Row>
                  <Division />
                  <Title
                    size={wp('6%')}
                    align="left"
                    color={theme.tenthColor}
                    margin="0px">
                    {texts.basicTwoHour}
                  </Title>
                  <Row style={{ marginTop: 16, marginBottom: 16 }}>
                    <ItemModal>
                      <Title
                        size={wp('3%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        {texts.valueTicket.toUpperCase()}
                      </Title>
                      <Title
                        size={wp('6%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        $39,98
                      </Title>
                    </ItemModal>
                    <ItemModal>
                      <Title
                        size={wp('3%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        {texts.youReciver.toUpperCase()}
                      </Title>
                      <Title
                        size={wp('6%')}
                        align="left"
                        weight="normal"
                        color={theme.tenthColor}
                        margin="0px">
                        $29,48
                      </Title>
                    </ItemModal>
                  </Row>
                </Card>
              </Section>
            </Content>
          </ScrollView>
        </Container>
      </Modal>
    );
  };

  return (
    <Container>
      {renderModal()}
      <Content>
        <Header>
          <HeaderBack isLogo={false} onPress={() => navigation.goBack()}>
            <Title
              size={wp('9%')}
              color={theme.sixthColor}
              align="left"
              margin="0px">
              {`${texts.singUp}\n${texts.NewLive}`}
            </Title>
          </HeaderBack>
        </Header>
      </Content>
      <ScrollView>
        <Content>
          <Section>
            <Title
              size={wp('9%')}
              align="left"
              color={theme.primaryColor}
              margin="0px 0px 16px 0px">
              {texts.dataLive}
            </Title>
            <Card>
              <KeyboardAwareScrollView>
                <TextInput
                  label={texts.nameLive}
                  placeholder={texts.giveNiceNameLive}
                  value={network}
                  keyboardType="default"
                  onChangeText={value => setNetwork(value)}
                />
                <Row>
                  <TextInput
                    label={texts.dateLive}
                    placeholder="dd/mm/yyyy"
                    value={network}
                    keyboardType="default"
                    onChangeText={value => setNetwork(value)}
                  />
                  <TextInput
                    label={texts.timeLive}
                    placeholder="00:00"
                    value={network}
                    keyboardType="default"
                    onChangeText={value => setNetwork(value)}
                  />
                </Row>
              </KeyboardAwareScrollView>
              <Title
                size={wp('3.4%')}
                align="left"
                color={theme.seventhColors}
                margin="4px 0px 8px 0px">
                {texts.category}
              </Title>
              <RNPickerSelect
                placeholder={{}}
                style={{
                  inputIOS: {
                    fontSize: 19,
                    color: theme.primaryColor,
                    borderBottomWidth: 1,
                    paddingBottom: 10,
                    borderColor: theme.primaryColor,
                  },
                  inputAndroid: {
                    fontSize: 19,
                    color: theme.primaryColor,
                    borderBottomWidth: 1,
                    paddingBottom: 10,
                    borderColor: theme.primaryColor,
                  },
                }}
                value="Axé"
                onValueChange={value => console.log(value)}
                items={[
                  { label: 'Show', value: 'axe' },
                  { label: 'Evento', value: 'Sertanejo' },
                ]}
              />
            </Card>
          </Section>
          <Section>
            <Card>
              <Title
                size={wp('3.4%')}
                align="left"
                color={theme.seventhColors}
                margin="4px 0px 8px 0px">
                {texts.dateAndValue}
              </Title>
              <RNPickerSelect
                placeholder={{}}
                style={{
                  inputIOS: {
                    fontSize: 19,
                    color: theme.primaryColor,
                    borderBottomWidth: 1,
                    paddingBottom: 10,
                    borderColor: theme.primaryColor,
                  },
                  inputAndroid: {
                    fontSize: 19,
                    color: theme.primaryColor,
                    borderBottomWidth: 1,
                    paddingBottom: 10,
                    borderColor: theme.primaryColor,
                  },
                }}
                value="Axé"
                onValueChange={value => console.log(value)}
                items={[
                  { label: '20R$ - 30 min', value: 'axe' },
                  { label: '20R$ - 30 min', value: 'axe' },
                ]}
              />
              <SectionMore onPress={() => setKnowMoreModal(true)}>
                <Title
                  size={wp('4%')}
                  color={theme.seventhColors}
                  margin="4px 0px 8px 0px">
                  {texts.moreAboutValue}
                </Title>
              </SectionMore>
            </Card>
          </Section>
          <Section>
            <Card>
              <KeyboardAwareScrollView>
                <SectionChangeImage>
                  {photo ? (
                    <Avatar size={wp('20%')} source={{ uri: photo }} />
                  ) : (
                      <Avatar size={wp('20%')} />
                    )
                  }
                  <Button
                    sizeText={`${wp('4%')}px`}
                    backgroundActive="transparent"
                    title={texts.selectImage}
                    type="transparent"
                    onPress={() => handleChoosePhoto()}
                    fontWeight="bold"
                  />
                </SectionChangeImage>
                <TextInput
                  label={texts.description}
                  value={network}
                  keyboardType="default"
                  onChangeText={value => setNetwork(value)}
                />
              </KeyboardAwareScrollView>
            </Card>
          </Section>
          <Section>
            <Button
              backgroundActive="transparent"
              title={texts.createLives}
              type="borded"
              fontWeight="bold"
            />
          </Section>
        </Content>
      </ScrollView>
    </Container>
  );
};
