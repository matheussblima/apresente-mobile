import styled from 'styled-components/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const Header = styled.View``;

export const SectionText = styled.View``;

export const Section = styled.View`
  margin-top: 32px;
`;

export const Row = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const SectionChangeImage = styled.View`
  display: flex;
  flex-direction: row;
  margin-bottom: 16px;
`;

export const SectionMore = styled.TouchableOpacity`
  margin-top: 16px;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.theme.eightteenthColor};
  padding: 16px 8px;
  border-radius: 8px;
`;

export const IconModal = styled.Image`
  height: ${wp('20%')}px;
  width: ${wp('20%')}px;
  margin-right: 16px;
`;

export const HeaderModal = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 20px;
`;

export const SectionTextsModal = styled.View`
  flex: 1;
`;

export const ItemModal = styled.View``;
