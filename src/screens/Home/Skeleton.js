import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export default () => {
  return (
    <SkeletonPlaceholder>
      <View style={{width: '100%', height: 100}} />

      <View style={{marginTop: 20, paddingHorizontal: 16}}>
        <View style={{width: 200, height: 20}} />
        <View style={{width: 120, height: 20, marginTop: 8}} />
      </View>
      <View
        style={{marginTop: 20, paddingHorizontal: 16, flexDirection: 'row'}}>
        <View style={{width: 150, height: 150, marginRight: 20}} />
        <View style={{width: 150, height: 150, marginRight: 20}} />
        <View style={{width: 150, height: 150, marginRight: 20}} />
      </View>

      <View style={{marginTop: 20, paddingHorizontal: 16}}>
        <View style={{width: 200, height: 20}} />
        <View style={{width: 120, height: 20, marginTop: 8}} />
      </View>
      <View
        style={{marginTop: 20, paddingHorizontal: 16, flexDirection: 'row'}}>
        <View style={{width: 150, height: 150, marginRight: 20}} />
        <View style={{width: 150, height: 150, marginRight: 20}} />
        <View style={{width: 150, height: 150, marginRight: 20}} />
      </View>

      <View style={{marginTop: 20, paddingHorizontal: 16}}>
        <View style={{width: 200, height: 20}} />
        <View style={{width: 120, height: 20, marginTop: 8}} />
      </View>
      <View
        style={{
          marginTop: 20,
          paddingHorizontal: 16,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'space-between',
        }}>
        <View style={{width: 110, height: 110, marginBottom: 20}} />
        <View style={{width: 110, height: 110, marginBottom: 20}} />
        <View style={{width: 110, height: 110, marginBottom: 20}} />
      </View>
    </SkeletonPlaceholder>
  );
};
