import styled from 'styled-components/native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export const HeaderInfo = styled.View`
  align-items: center;
  margin-top: ${hp('2%')}px;
  padding: ${hp('2%')}px;
  background-color: ${props => props.theme.fifthColor};
`;

export const SectionEvents = styled.View`
  margin-top: ${hp('3%')}px;
`;

export const MoreSearchButton = styled.TouchableOpacity`
  min-width: ${wp('24%')}px;
  margin-bottom: ${wp('3%')}px;
  margin-right: 8px;
  background-color: ${props => props.theme.fourthColor};
  border-radius: ${wp('8%')}px;
  padding: ${wp('2%')}px;
  border-color: ${props => props.theme.sixthColor};
  border-width: 1px;
  align-items: center;
  justify-content: center;
`;

export const ListArtist = styled.View`
  display: flex;
  flex-direction: row;
  margin-bottom: 16px;
  margin-top: 16px;
`;

export const ListArtistInfo = styled.View`
  display: flex;
  flex: 1;
`;
