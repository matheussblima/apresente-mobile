import React, {useEffect} from 'react';
import {ScrollView, RefreshControl, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import Skeleton from './Skeleton';
import {
  Container,
  Content,
  Title,
  Subtitle,
  ListHorizontalImages,
  ListSideBySide,
  Avatar,
  Division,
} from '../../components';
import {theme} from '../../resources';
import {texts} from '../../config';
import {
  SectionEvents,
  MoreSearchButton,
  ListArtist,
  ListArtistInfo,
} from './styles';
import {findEventByNearby, setLastSeenEvents} from '../../redux/events';
import {setFilter} from '../../redux/filter';
import {findCitys} from '../../redux/citys';
import {findCategorys} from '../../redux/category';
import {getLocation} from '../../redux/geolocation';

const Home = props => {
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    props.getLocation();
  }, [refreshing]);

  useEffect(() => {
    props.getLocation();
    props.findCategorys();
  }, []);

  useEffect(() => {
    if (props.geolocation.isSuccess) {
      props
        .findCitys({
          latitude: props.geolocation.location.latitude,
          longitude: props.geolocation.location.longitude,
        })
        .then(responseCity => {
          if (responseCity.isSuccess) {
            props
              .findEventByNearby({
                nearby: responseCity.payload[0].codCidade,
              })
              .then(() => {
                setRefreshing(false);
              });
          } else {
            props.findEventByNearby({}).then(() => {
              setRefreshing(false);
            });
          }
        });
    } else {
      props.findCitys({});
      props.findEventByNearby({}).then(() => {
        setRefreshing(false);
      });
      setRefreshing(false);
    }
  }, [props.geolocation.location]);

  return (
    <Container>
      {props.events.isSuccess && props.geolocation.isSuccess ? (
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          <Content>
            <SectionEvents>
              <Title
                size={wp('9%')}
                color={theme.primaryColor}
                align="left"
                margin="0px">
                {texts.nextsExperiences}
              </Title>
              <ListHorizontalImages
                onPress={item => {
                  props.setLastSeenEvents(item);
                  props.navigation.navigate('Event', {event: item});
                }}
                data={props.events.events
                  .map(event => ({
                    ...event,
                    id: event.codEvento,
                    image: {uri: event.linkImage},
                  }))
                  .sort(
                    (a, b) =>
                      new Date(a.data.split(' ')[0]) -
                      new Date(b.data.split(' ')[0]),
                  )}
              />
            </SectionEvents>

            {props.category.isSuccess &&
            props.category.categories.length > 0 ? (
              <>
                <SectionEvents>
                  <Title
                    size={wp('9%')}
                    color={theme.primaryColor}
                    align="left"
                    margin="0px">
                    {texts.moreSearch}
                  </Title>
                  <ListSideBySide
                    onPress={() => {}}
                    data={props.category.categories.slice(0, 10)}
                    renderItem={(item, index) => {
                      return (
                        <MoreSearchButton
                          key={index}
                          activeOpacity={0.8}
                          onPress={() => {
                            props.setFilter({category: item});
                            props.navigation.navigate('Search');
                          }}>
                          <Subtitle
                            size={`${wp('3.2%')}`}
                            numberOfLines={1}
                            color={theme.sixthColor}
                            margin="0px">
                            {item.nome}
                          </Subtitle>
                        </MoreSearchButton>
                      );
                    }}
                  />
                </SectionEvents>
              </>
            ) : (
              undefined
            )}

            <SectionEvents>
              <Title
                size={wp('9%')}
                color={theme.primaryColor}
                align="left"
                margin="0px">
                {texts.topArtists}
              </Title>
              <FlatList
                showsVerticalScrollIndicator={false}
                data={[
                  {
                    id: '1',
                    name: 'Murilo Coutos',
                    lives: '26 Lives publicadas - 2.345 convidados',
                    avatar:
                      'https://noverbal.es/uploads/blog/rostro-de-un-criminal.jpg',
                  },
                  {
                    id: '12',
                    name: 'Murilo Coutos',
                    lives: '26 Lives publicadas - 2.345 convidados',
                    avatar:
                      'https://noverbal.es/uploads/blog/rostro-de-un-criminal.jpg',
                  },
                  {
                    id: '13',
                    name: 'Murilo Coutos',
                    lives: '26 Lives publicadas - 2.345 convidados',
                    avatar:
                      'https://noverbal.es/uploads/blog/rostro-de-un-criminal.jpg',
                  },
                  {
                    id: '14',
                    name: 'Murilo Coutos',
                    lives: '26 Lives publicadas - 2.345 convidados',
                    avatar:
                      'https://noverbal.es/uploads/blog/rostro-de-un-criminal.jpg',
                  },
                ]}
                renderItem={({item}) => {
                  return (
                    <>
                      <ListArtist>
                        <Avatar size={wp('16%')} source={{uri: item.avatar}} />
                        <ListArtistInfo>
                          <Title
                            size={wp('9%')}
                            color={theme.sixthColor}
                            align="left"
                            margin="0px">
                            {item.name}
                          </Title>
                          <Subtitle
                            margin="0px"
                            align="left"
                            color={theme.sixthColor}>
                            {item.lives}
                          </Subtitle>
                        </ListArtistInfo>
                      </ListArtist>
                      <Division />
                    </>
                  );
                }}
                keyExtractor={item => item.id.toString()}
              />
            </SectionEvents>
          </Content>
        </ScrollView>
      ) : (
        <Skeleton />
      )}
    </Container>
  );
};

const mapStateToProps = state => ({
  ...state,
});
const mapDispatchToProps = dispatch => ({
  findEventByNearby: params => dispatch(findEventByNearby(params)),
  findCitys: coords => dispatch(findCitys(coords)),
  findCategorys: () => dispatch(findCategorys()),
  getLocation: () => dispatch(getLocation()),
  setFilter: filter => dispatch(setFilter(filter)),
  setLastSeenEvents: event => dispatch(setLastSeenEvents(event)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
