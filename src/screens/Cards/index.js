import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {FlatList, TouchableOpacity} from 'react-native';
import {
  Container,
  HeaderBack,
  Title,
  Subtitle,
  Content,
} from '../../components';
import {
  Bottom,
  Header,
  SectionList,
  ContainerList,
  SectionIcon,
  Touch,
  Icon,
  SectionBottom,
  SectionListLeft,
  IconButtom,
} from './styles';
import {theme, images} from '../../resources';
import {texts} from '../../config';
import {deleteCard} from '../../redux/card';

const Cards = props => {
  const dispatch = useDispatch();
  const cardSelector = useSelector(state => state.card);
  const auth = useSelector(state => state.auth);
  const card = cardSelector.card.filter(
    value => value.userToken === auth.auth.data.keyapp,
  );

  const listItems = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('CardEdit', {cardSelected: item});
        }}>
        <ContainerList>
          <SectionListLeft>
            <Title margin="0" align="left" color={theme.sixthColor}>
              {item.numberCard.slice(-4)}
            </Title>
            <Subtitle margin="0" align="left" color={theme.sixthColor}>
              {texts.validate}: {item.month}/{item.year}
            </Subtitle>
            {item.isDefault ? (
              <Subtitle margin="0" align="left">
                {texts.default}
              </Subtitle>
            ) : (
              undefined
            )}
          </SectionListLeft>
          <SectionIcon>
            <Touch
              activeOpacity={0.5}
              onPress={() => {
                dispatch(deleteCard(item.numberCard));
              }}>
              <Icon source={images.trash} resizeMode="contain" />
            </Touch>
          </SectionIcon>
        </ContainerList>
      </TouchableOpacity>
    );
  };

  const renderKeys = item => item.numberCard.toString();

  return (
    <Container>
      <HeaderBack
        onPress={() => {
          props.navigation.goBack();
        }}
      />
      <Content style={{flex: 1}}>
        <Header>
          <Title margin="8px 0" align="left" color={theme.sixthColor}>
            {texts.myCards}
          </Title>
        </Header>
        <SectionList>
          {card.length > 0 ? (
            <FlatList
              renderItem={item => listItems(item, props)}
              data={card}
              keyExtractor={renderKeys}
            />
          ) : (
            <Subtitle align="center" color={theme.sixthColor}>
              {texts.cardEmpty}
            </Subtitle>
          )}
        </SectionList>
        <SectionBottom>
          <Bottom>
            <Subtitle align="right">{texts.createCreditCard}</Subtitle>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() =>
                props.navigation.navigate('CardData', {
                  previousScreen: 'Cards',
                })
              }>
              <IconButtom source={images.plus} resizeMode="contain" />
            </TouchableOpacity>
          </Bottom>
        </SectionBottom>
      </Content>
    </Container>
  );
};

export default Cards;
