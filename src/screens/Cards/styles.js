import styled from 'styled-components/native';

export const Header = styled.View``;

export const SectionList = styled.View`
  flex: 1;
`;

export const SectionBottom = styled.View`
  align-items: flex-end;
  justify-content: flex-end;
  margin-bottom: 8px;
`;

export const Bottom = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const SectionListLeft = styled.View``;

export const ContainerList = styled.View`
  margin: 8px 4px;
  padding: 8px 16px;
  flex-direction: row;
  background-color: #fff;
  shadow-color: ${props => props.theme.sixthColor};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.22;
  shadow-radius: 2.22px;
  elevation: 3;
  border-radius: 8px;
`;

export const SectionIcon = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  padding: 4px;
`;

export const Touch = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
`;

export const Icon = styled.Image`
  width: 30px;
  height: 30px;
`;

export const IconButtom = styled.Image`
  width: 50px;
  height: 50px;
`;
