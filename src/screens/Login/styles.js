import styled from 'styled-components/native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const Header = styled.View`
  align-items: center;
  margin-top: ${hp('5%')}px;
`;

export const Section = styled.View`
  margin-top: ${hp('8%')}px;
`;

export const SectionButton = styled.View`
  margin-top: ${hp('2%')}px;
`;

export const SectionHeader = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
