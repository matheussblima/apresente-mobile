import React, {useState, useRef, useEffect} from 'react';
import {ScrollView} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import DropdownAlert from 'react-native-dropdownalert';
import {
  Container,
  Logo,
  TextInput,
  Content,
  Button,
  Card,
  Title,
} from '../../components';
import {login} from '../../redux/auth';
import {Header, Section, SectionButton, SectionHeader} from './styles';
import {texts} from '../../config';
import {theme} from '../../resources';

const Login = props => {
  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);
  const [isFetchLogin, setIsFetchLogin] = useState(false);
  const [email, setEmail] = useState('');
  const dropDownAlertRef = useRef();
  const [password, setPassword] = useState('');

  useEffect(() => {
    if (isFetchLogin) {
      if (auth.isSuccessAuth) {
        props.navigation.navigate('Home');
        setIsFetchLogin(false);
      } else if (!auth.isSuccessAuth && !auth.isFetchAuth) {
        setIsFetchLogin(false);
        dropDownAlertRef.current.alertWithType(
          'error',
          texts.errorAuth,
          auth.message,
        );
      }
    }
  }, [auth]);

  const authing = loginValue => {
    dispatch(login(loginValue.email, loginValue.password));
    setIsFetchLogin(true);
  };

  return (
    <>
      <Container>
        <ScrollView>
          <Content>
            <Header>
              <Logo />
            </Header>
            <Section>
              <Card>
                <SectionHeader>
                  <Title color={theme.sixthColor} align="left" margin="0px">
                    {texts.login}
                  </Title>
                  <Button
                    title={texts.createAccountCompany}
                    type="transparent"
                    onPress={() => props.navigation.navigate('SingUp')}
                  />
                </SectionHeader>
                <TextInput
                  placeholder={texts.yourEmail}
                  value={email}
                  keyboardType="email-address"
                  autoCapitalize="none"
                  onChangeText={value => setEmail(value)}
                />
                <TextInput
                  placeholder={texts.yourPassword}
                  value={password}
                  secureTextEntry
                  keyboardType="default"
                  autoCapitalize="none"
                  onChangeText={value => setPassword(value)}
                />

                <SectionButton>
                  <Button
                    loading={auth.isFetchAuth}
                    title={texts.login}
                    type="borded"
                    onPress={() => authing({email, password})}
                  />
                  <Button
                    title={texts.forgotMyPassword}
                    type="transparent"
                    onPress={() => props.navigation.navigate('Recovery')}
                  />
                </SectionButton>
              </Card>
            </Section>
          </Content>
        </ScrollView>
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};

export default Login;
