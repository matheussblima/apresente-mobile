import React, {useState} from 'react';
import Video from 'react-native-video';
import emoji from 'node-emoji';
import {
  Container,
  Content,
  Division,
  Title,
  Subtitle,
  Button,
  AnimatedEmoji,
} from '../../components';
import {
  SectionVideo,
  Header,
  SectionInfo,
  SectionCommenter,
  SectionButton,
  SectionEmojis,
  Emoji,
  TouchEmoji,
} from './styles';
import {theme} from '../../resources';

const Live = props => {
  const [selectedEmoji, setSelectedEmoji] = useState([]);

  const actionEmoji = {
    remove: index => {
      console.log('=====>', index);
      const auxArrayEmojis = selectedEmoji.slice(0);
      const item = auxArrayEmojis.findIndex(value => value.index === index);
      if (item !== -1) {
        setSelectedEmoji(auxArrayEmojis.splice(index, 1));
        console.log('======>', auxArrayEmojis.splice(index, 1));
      }
    },
    add: emojiValue => {
      const auxArrayEmojis = selectedEmoji.slice(0);
      auxArrayEmojis.push({...emojiValue});
      setSelectedEmoji(auxArrayEmojis);
    },
  };

  const id = () => {
    return `_${Math.random()
      .toString(36)
      .substr(2, 9)}`;
  };

  console.log(selectedEmoji);

  const renderEmoji = () => {
    return (
      <>
        {selectedEmoji.map(value => {
          return (
            <AnimatedEmoji
              index={value.index}
              style={{bottom: 240, right: 100}}
              name={value.name}
              size={30}
              duration={3000}
              onAnimationCompleted={indexCompleted => {
                actionEmoji.remove(indexCompleted);
              }}
            />
          );
        })}
      </>
    );
  };

  return (
    <Container>
      <SectionVideo>
        {/* <Video
          style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0}}
          source={{
            uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
          }} // Can be a URL or a local file.
          onBuffer={() => {}}
          onError={() => {}}
        /> */}
        {renderEmoji()}
      </SectionVideo>
      <SectionInfo>
        <Content>
          <Header>
            <Title margin="0px" align="left" color={theme.fourthColor}>
              Lavagem do Senhor do Bomfim 2021
            </Title>
            <Subtitle margin="0px" align="left" color={theme.fourthColor}>
              Termina em 25m 43s
            </Subtitle>
            <Division color={theme.thirdColor} />
          </Header>
          <SectionButton>
            <Button
              margin="0px 4px"
              backgroundColor={theme.seventhColors}
              title="342"
              type="rounded"
            />
            <Button
              margin="0px 4px"
              backgroundColor={theme.seventhColors}
              title="342"
              type="rounded"
            />
            <Button margin="0px 4px" title="Info" type="rounded" />
            <Button
              onPress={() => props.navigation.goBack()}
              margin="0px 0px 0px 4px"
              backgroundColor={theme.twelfthColor}
              title="Sair"
              type="rounded"
            />
          </SectionButton>
          <SectionCommenter>
            <SectionEmojis>
              <TouchEmoji
                onPress={() =>
                  actionEmoji.add({name: 'thumbsup', index: id()})
                }>
                <Emoji>{emoji.get('thumbsup')}</Emoji>
              </TouchEmoji>
              <TouchEmoji
                onPress={() => actionEmoji.add({name: 'heart', index: id()})}>
                <Emoji>{emoji.get('heart')}</Emoji>
              </TouchEmoji>
              <TouchEmoji
                onPress={() =>
                  actionEmoji.add({
                    name: 'stuck_out_tongue_closed_eyes',
                    index: id(),
                  })
                }>
                <Emoji>{emoji.get('stuck_out_tongue_closed_eyes')}</Emoji>
              </TouchEmoji>
              <TouchEmoji
                onPress={() => actionEmoji.add({name: 'hushed', index: id()})}>
                <Emoji>{emoji.get('hushed')}</Emoji>
              </TouchEmoji>
              <TouchEmoji
                onPress={() =>
                  actionEmoji.add({name: 'disappointed_relieved', index: id()})
                }>
                <Emoji>{emoji.get('disappointed_relieved')}</Emoji>
              </TouchEmoji>
              <TouchEmoji
                onPress={() => actionEmoji.add({name: 'rage', index: id()})}>
                <Emoji>{emoji.get('rage')}</Emoji>
              </TouchEmoji>
            </SectionEmojis>
          </SectionCommenter>
        </Content>
      </SectionInfo>
    </Container>
  );
};

export default Live;
