import styled from 'styled-components/native';

export const SectionVideo = styled.View`
  background-color: ${props => props.theme.tenthColor};
  height: 405px;
  width: 100%;
`;

export const SectionInfo = styled.View`
  background-color: ${props => props.theme.sixthColor};
  height: 100%;
`;

export const SectionCommenter = styled.View`
  margin-top: 16px;
  background-color: ${props => props.theme.fourthColor};
`;

export const Header = styled.View`
  margin-top: 16px;
`;

export const SectionButton = styled.View`
  margin-top: 16px;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`;

export const SectionEmojis = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const Emoji = styled.Text`
  font-size: 40px;
`;

export const TouchEmoji = styled.TouchableOpacity``;
