import styled from 'styled-components/native';

export const SectionInfoEnvent = styled.View`
  padding: 16px 0px;
`;

export const SectionPayment = styled.View`
  margin: 16px 0px;
`;

export const SectionInfo = styled.View`
  flex: 1;
  z-index: 10;
  position: relative;
  padding-bottom: 100px;
  background-color: ${props => props.theme.fourthColor};
`;

export const SectionTabs = styled.View`
  margin: 16px 0px;
`;

export const SecTionTotal = styled.View`
  margin: 16px 0px;
`;

export const ContentTab = styled.View`
  margin-top: 32px;
  padding: 0px 8%;
`;

export const PicketSection = styled.View`
  margin-top: 32px;
  border: 1px solid ${props => props.theme.thirdColor};
  padding: 8px;
  border-radius: 4px;
`;

export const CardInfo = styled.View`
  border-radius: 20px;
  background-color: ${props => props.theme.fifthColor};
`;
