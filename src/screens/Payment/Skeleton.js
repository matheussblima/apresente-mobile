import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export default () => {
  return (
    <SkeletonPlaceholder>
      <View
        style={{
          width: '50%',
          height: 30,
          marginBottom: 16,
          marginTop: 20,
          alignSelf: 'center',
        }}
      />
      <View style={{flexDirection: 'row', justifyContent: 'center'}}>
        <View
          style={{width: 150, height: 120, marginBottom: 16, marginRight: 20}}
        />
        <View style={{width: 150, height: 120, marginBottom: 16}} />
      </View>
      <View
        style={{
          width: '100%',
          height: 30,
          marginBottom: 16,
          alignSelf: 'center',
        }}
      />
      <View
        style={{
          width: '100%',
          height: 30,
          marginBottom: 16,
          alignSelf: 'center',
        }}
      />
    </SkeletonPlaceholder>
  );
};
