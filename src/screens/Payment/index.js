import React, {useState, useEffect, useRef} from 'react';
import {connect, useDispatch, useSelector} from 'react-redux';

import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import RNPickerSelect from 'react-native-picker-select';
import DropdownAlert from 'react-native-dropdownalert';
import moment from 'moment';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {
  Container,
  HeaderClose,
  Title,
  EventInfo,
  Content,
  ListDeleteItem,
  Division,
  Tabs,
  Subtitle,
  Button,
} from '../../components';
import {theme, images} from '../../resources';
import {
  SectionInfoEnvent,
  SectionInfo,
  SectionPayment,
  SectionTabs,
  CardInfo,
  ContentTab,
  PicketSection,
  SecTionTotal,
} from './styles';
import {texts} from '../../config';
import currencyFormat from '../../utility/currencyFormat';
import {deleteItemCart} from '../../redux/cart';
import {createOrder} from '../../redux/order';
import {
  createPayment,
  getPaymentsInfo,
  validateCart,
} from '../../redux/payment';
import {getProfile} from '../../redux/auth';

import Skeleton from './Skeleton';

const Payment = props => {
  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);
  const {card, payment, order, cart} = props;
  const dropDownAlertRef = useRef();
  const [tab, setTab] = useState(1);
  const [selectedParcel, setSelectedParcel] = useState(1);

  const defaultCard =
    card.card && card.card.find(value => value.isDefault === true);
  const cardInfo =
    defaultCard &&
    `${defaultCard.numberCard.slice(-4).padStart(8, '*')} | ${
      defaultCard.brand
    }`;

  const tax = payment.paymentsInfo.taxaParcelamento;
  const taxBoleto = payment.paymentsInfo.txBoleto;
  const totalCart = cart.total.totalTicket;
  const discount = cart.total.discounts;

  useEffect(() => {
    dispatch(getProfile());
    props.getPaymentsInfo(cart.event.codEvento);
  }, []);

  const onPressPayment = () => {
    props
      .validateCart({
        codEvent: props.cart.event.codEvento,
        type: tab === 1 ? 'cartao' : 'boleto',
        parcel: selectedParcel || 1,
        ticket: props.cart.items.map(value => ({
          lote: +value.codLote,
          qtd: +value.qtd,
        })),
        discount: null,
      })
      .then(responseValidade => {
        if (responseValidade.isSuccessValidate) {
          props
            .createOrder({
              codEvent: props.cart.event.codEvento,
              type: tab === 1 ? 'cartao' : 'boleto',
              parcel: selectedParcel || 1,
              ticket: props.cart.items.map(value => ({
                lote: +value.codLote,
                qtd: +value.qtd,
              })),
              discount: null,
            })
            .then(responseOrder => {
              if (responseOrder.isSuccessOrderCreate) {
                props
                  .createPayment({
                    card: defaultCard,
                    typePayment: tab === 1 ? 'cartao' : 'boleto',
                    gateway: payment.paymentsInfo.gateway,
                    keyOrder: responseOrder.payload.key,
                  })
                  .then(responseCreatePayment => {
                    if (responseCreatePayment.isSuccessCreate) {
                      props.navigation.navigate('Invoice', {
                        orderId: responseOrder.payload.key,
                      });
                    } else {
                      dropDownAlertRef.current.alertWithType(
                        'error',
                        texts.errorPay,
                        responseCreatePayment.message,
                      );
                    }
                  });
              } else {
                dropDownAlertRef.current.alertWithType(
                  'error',
                  texts.errorPay,
                  responseOrder.message,
                );
              }
            });
        } else {
          dropDownAlertRef.current.alertWithType(
            'error',
            texts.errorPay,
            responseValidade.message,
          );
        }
      });
  };

  useEffect(() => {
    if (cart.items) {
      if (cart.items.length === 0) {
        props.navigation.navigate('Event');
      }
    }
  }, [cart]);

  return (
    <>
      <Container>
        <ParallaxScroll
          parallaxHeight={hp('34%')}
          parallaxBackgroundScrollSpeed={5}
          parallaxForegroundScrollSpeed={2.5}
          isHeaderFixed
          headerHeight={50}
          isBackgroundScalable={false}
          headerFixedBackgroundColor={theme.fourthColor}
          renderHeader={() => (
            <HeaderClose onPress={() => props.navigation.navigate('Event')} />
          )}
          renderParallaxForeground={() => (
            <HeaderClose onPress={() => props.navigation.navigate('Event')}>
              <Title color={theme.sixthColor}>{texts.orderSummary}</Title>
              <Content>
                <SectionInfoEnvent>
                  <EventInfo
                    image={{uri: cart.event.linkImage}}
                    title={cart.event.nome}
                    overTitle={moment(cart.event.data).format(
                      'ddd, DD MMM YYYY HH:mm',
                    )}
                    subTitle={cart.event.local}
                  />
                  <Division />
                </SectionInfoEnvent>
              </Content>
            </HeaderClose>
          )}>
          <SectionInfo>
            <Content>
              <ListDeleteItem
                data={cart.items}
                onPressDelete={item => {
                  props.deleteItemCart(item);
                }}
              />
              {payment.isSuccess ? (
                <>
                  <SectionPayment>
                    <Title color={theme.sixthColor}>{texts.payment}</Title>
                    <SectionTabs>
                      <Tabs
                        onPressTab={item => {
                          setTab(item);
                        }}
                        selected={tab}
                        tabOneImage={images.creditCard}
                        tabOneTitle={texts.creditCard}
                        disabled={
                          payment.paymentsInfo.permiteBoleto ? undefined : 2
                        }
                        tabTwoImage={images.barcode}
                        tabTwoTitle={texts.boletoBank}>
                        <ContentTab>
                          {tab === 1 ? (
                            <>
                              {cardInfo ? (
                                <>
                                  <CardInfo>
                                    <Subtitle color={theme.sixthColor}>
                                      {cardInfo}
                                    </Subtitle>
                                  </CardInfo>
                                  <Button
                                    onPress={() =>
                                      props.navigation.navigate('Cards')
                                    }
                                    title={texts.changeCard}
                                    type="transparent"
                                  />
                                </>
                              ) : (
                                <Button
                                  onPress={() =>
                                    props.navigation.navigate('CardData', {
                                      previousScreen: 'Payment',
                                    })
                                  }
                                  title={texts.addCard}
                                  type="transparent"
                                />
                              )}
                            </>
                          ) : (
                            undefined
                          )}
                        </ContentTab>
                      </Tabs>
                      <Subtitle margin="0" color={theme.sixthColor}>
                        {texts.toStartProcessPayment}
                      </Subtitle>
                      {tab === 1 ? (
                        <PicketSection>
                          <RNPickerSelect
                            value={selectedParcel}
                            placeholder={{
                              label: texts.parcel,
                              value: -1,
                            }}
                            textInputProps={{
                              fontSize: wp('5%'),
                              fontWeight: 'bold',
                              color: theme.sixthColor,
                            }}
                            onValueChange={value => {
                              if (value !== -1) {
                                setSelectedParcel(value);
                              }
                            }}
                            items={Object.keys(
                              payment.paymentsInfo.taxaParcelamento,
                            )
                              .map((parcel, index) => {
                                const taxs = tax[parcel].vlTaxa;

                                const parcelValue =
                                  (totalCart + (totalCart / 100) * taxs) /
                                  parcel;

                                if (
                                  parcelValue >=
                                  payment.paymentsInfo.vl_min_parcela
                                ) {
                                  return {
                                    label: `${parcel} x ${currencyFormat(
                                      parseFloat(parcelValue),
                                      true,
                                    )}`,
                                    value: index + 1,
                                  };
                                }
                                return undefined;
                              })
                              .filter(value => value !== undefined)}
                          />
                        </PicketSection>
                      ) : (
                        undefined
                      )}
                    </SectionTabs>
                  </SectionPayment>
                  <Division />
                  <SecTionTotal>
                    {/* TOTAL CART */}
                    <Subtitle margin="0" align="right" color={theme.sixthColor}>
                      {texts.totalTicket}:{' '}
                      {currencyFormat(parseFloat(totalCart), true)}
                    </Subtitle>
                    {/* TAXS */}
                    <Subtitle margin="0" align="right" color={theme.sixthColor}>
                      {texts.totaltax}:{' '}
                      {tab === 1
                        ? currencyFormat(
                            (totalCart / 100) * tax[selectedParcel].vlTaxa,
                            true,
                          )
                        : currencyFormat(taxBoleto, true)}
                    </Subtitle>
                    {/* DISCOUNT */}
                    <Subtitle margin="0" align="right" color={theme.sixthColor}>
                      {texts.discounts}:{' '}
                      {currencyFormat(parseFloat(discount), true)}
                    </Subtitle>
                    {/* TOTAL */}
                    <Title margin="16px 0" align="right">
                      {texts.total}:{' '}
                      {tab === 1
                        ? currencyFormat(
                            (totalCart / 100) * tax[selectedParcel].vlTaxa +
                              totalCart,
                            true,
                          )
                        : currencyFormat(taxBoleto + totalCart, true)}
                    </Title>
                  </SecTionTotal>
                  {auth.isSuccessGetProfile ? (
                    <>
                      {auth.profile.emptyFields.length === 0 ? (
                        <Button
                          loading={
                            payment.isFetchValidate ||
                            order.isFetchOrderCreate ||
                            payment.isFetchCreate
                          }
                          title={texts.checkout}
                          type="normal"
                          onPress={() => {
                            onPressPayment();
                          }}
                        />
                      ) : (
                        <>
                          <Subtitle color={theme.sixthColor}>
                            {texts.editYourProfile}{' '}
                            {auth.profile.emptyFields.join(', ')}
                          </Subtitle>
                          <Button
                            loading={
                              payment.isFetchValidate ||
                              order.isFetchOrderCreate ||
                              payment.isFetchCreate
                            }
                            title={texts.editProfile}
                            type="normal"
                            onPress={() => {
                              props.navigation.navigate('EditPerfil');
                            }}
                          />
                        </>
                      )}
                    </>
                  ) : (
                    undefined
                  )}
                </>
              ) : (
                <Skeleton />
              )}
            </Content>
          </SectionInfo>
        </ParallaxScroll>
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};

const mapStateToProps = state => ({...state});
const mapDispatchToProps = {
  validateCart,
  createOrder,
  createPayment,
  getPaymentsInfo,
  deleteItemCart,
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);
