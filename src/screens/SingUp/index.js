import React, {useState, useRef, useEffect} from 'react';
import {ScrollView} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import VMasker from 'vanilla-masker';
import DropdownAlert from 'react-native-dropdownalert';
import {
  Container,
  Logo,
  TextInput,
  Content,
  Button,
  Title,
  Card,
  Switch,
} from '../../components';
import {createUser} from '../../redux/auth';
import {Header, Section, SectionButton, SectionHeader} from './styles';
import {texts} from '../../config';
import {theme} from '../../resources';

const SingUp = props => {
  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [cell, setCell] = useState('');
  const [acceptTerms, setAcceptTerms] = useState(false);
  const [dateBorn, setDateBorn] = useState('');
  const [name, setName] = useState('');
  const [isFetchCreateAccount, setIsFetchCreateAccount] = useState(false);
  const dropDownAlertRef = useRef();

  useEffect(() => {
    if (isFetchCreateAccount) {
      if (auth.isSuccessCreateUser) {
        props.navigation.navigate('Login');
        setIsFetchCreateAccount(false);
      } else if (!auth.isSuccessCreateUser && !auth.isFetchCreateUser) {
        setIsFetchCreateAccount(false);
        dropDownAlertRef.current.alertWithType(
          'error',
          texts.errorAuth,
          auth.message,
        );
      }
    }
  }, [auth]);

  const createAccount = user => {
    dispatch(createUser(user));
    setIsFetchCreateAccount(true);
  };

  return (
    <>
      <Container>
        <ScrollView>
          <Content>
            <Header>
              <Logo />
            </Header>
            <Section>
              <Card>
                <SectionHeader>
                  <Title color={theme.sixthColor} align="left" margin="0px">
                    {texts.createAccount}
                  </Title>
                  <Button
                    title={texts.doLogin}
                    type="transparent"
                    onPress={() => props.navigation.goBack()}
                  />
                </SectionHeader>
                <KeyboardAwareScrollView>
                  <TextInput
                    placeholder={texts.yourName}
                    value={name}
                    keyboardType="default"
                    onChangeText={value => setName(value)}
                  />
                  <TextInput
                    placeholder={texts.yourEmail}
                    value={email}
                    keyboardType="email-address"
                    autoCapitalize="none"
                    onChangeText={value => setEmail(value)}
                  />
                  <TextInput
                    placeholder={texts.yourCell}
                    value={cell}
                    keyboardType="phone-pad"
                    onChangeText={value => {
                      const mask = VMasker.toPattern(value, '(99) 99999-9999');
                      setCell(mask);
                    }}
                  />
                  <TextInput
                    placeholder={texts.yourPassword}
                    value={password}
                    secureTextEntry
                    keyboardType="default"
                    autoCapitalize="none"
                    onChangeText={value => setPassword(value)}
                  />
                  <TextInput
                    placeholder={texts.dateBorn}
                    value={dateBorn}
                    keyboardType="numeric"
                    onChangeText={value => {
                      const mask = VMasker.toPattern(value, '99/99/9999');
                      setDateBorn(mask);
                    }}
                  />
                  <Switch
                    title={texts.youDeclarerReadTerms}
                    value={acceptTerms}
                    onValueChange={value => setAcceptTerms(value)}
                  />
                  <Button
                    title={texts.termsPolicies}
                    type="transparent"
                    fontWeight="bold"
                  />
                </KeyboardAwareScrollView>
                <SectionButton>
                  <Button
                    type="borded"
                    title={texts.singUp}
                    onPress={() => {
                      if (email || password || name || cell || dateBorn) {
                        const arryDateBorn = dateBorn.split('/');
                        createAccount({
                          email,
                          password,
                          cell: cell
                            .replace('(', '')
                            .replace(')', '')
                            .replace(' ', '')
                            .replace('-', ''),
                          name,
                          dateBorn: `${arryDateBorn[2]}-${arryDateBorn[1]}-${arryDateBorn[0]}`,
                        });
                      } else {
                        dropDownAlertRef.current.alertWithType(
                          'error',
                          texts.errorRegister,
                          texts.verifyData,
                        );
                      }
                    }}
                  />
                </SectionButton>
              </Card>
            </Section>
          </Content>
        </ScrollView>
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};

export default SingUp;
