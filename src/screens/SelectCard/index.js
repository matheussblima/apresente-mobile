import React, {useState, useRef, useEffect} from 'react';
import {View} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
} from 'react-native-simple-radio-button';
import {connect, useSelector} from 'react-redux';
import DropdownAlert from 'react-native-dropdownalert';
import moment from 'moment';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import {
  Title,
  Subtitle,
  HeaderClose,
  Division,
  Button,
  TextInput,
} from '../../components';
import {
  SectionInfo,
  ImageEvent,
  OverlayImage,
  Header,
  Container,
  ContainerCard,
  Icon,
  SectionCard,
  Row,
  SectionIcon,
  SectionButton,
} from './styles';
import {theme, images} from '../../resources';
import {texts} from '../../config';
import {createOrder} from '../../redux/order';
import {
  createPayment,
  getPaymentsInfo,
  validateCart,
} from '../../redux/payment';

const Event = props => {
  const {payment, order} = props;
  const cart = useSelector(state => state.cart);
  const {card} = useSelector(state => state.card);
  const dropDownAlertRef = useRef();
  const [selectedCardIndex, setSelectedCardIndex] = useState(0);

  useEffect(() => {
    props.getPaymentsInfo(cart.event.codEvento);
  }, []);

  const onPressPayment = () => {
    props
      .validateCart({
        codEvent: cart.event.codEvento,
        type: 'cartao',
        parcel: 1,
        ticket: cart.items.map(value => ({
          lote: +value.codLote,
          qtd: +value.qtd,
        })),
        discount: null,
      })
      .then(responseValidade => {
        if (responseValidade.isSuccessValidate) {
          props
            .createOrder({
              codEvent: props.cart.event.codEvento,
              type: 'cartao',
              parcel: 1,
              ticket: props.cart.items.map(value => ({
                lote: +value.codLote,
                qtd: +value.qtd,
              })),
              discount: null,
            })
            .then(responseOrder => {
              if (responseOrder.isSuccessOrderCreate) {
                props
                  .createPayment({
                    card: card[selectedCardIndex],
                    typePayment: 'cartao',
                    gateway: payment.paymentsInfo.gateway,
                    keyOrder: responseOrder.payload.key,
                  })
                  .then(responseCreatePayment => {
                    if (responseCreatePayment.isSuccessCreate) {
                      props.navigation.navigate('Invoice', {
                        orderId: responseOrder.payload.key,
                      });
                    } else {
                      props.navigation.navigate('InvoiceError');
                      // dropDownAlertRef.current.alertWithType(
                      //   'error',
                      //   texts.errorPay,
                      //   responseCreatePayment.message,
                      // );
                    }
                  });
              } else {
                props.navigation.navigate('InvoiceError');
                // dropDownAlertRef.current.alertWithType(
                //   'error',
                //   texts.errorPay,
                //   responseOrder.message,
                // );
              }
            });
        } else {
          props.navigation.navigate('InvoiceError');
          // dropDownAlertRef.current.alertWithType(
          //   'error',
          //   texts.errorPay,
          //   responseValidade.message,
          // );
        }
      });
  };

  return (
    <>
      <Container>
        <ParallaxScroll
          isHeaderFixed={false}
          parallaxHeight={hp('20%')}
          parallaxBackgroundScrollSpeed={5}
          parallaxForegroundScrollSpeed={2.5}
          renderHeader={() => (
            <HeaderClose
              onPress={() => props.navigation.goBack()}
              colorIcon={theme.fourthColor}
            />
          )}
          renderParallaxBackground={() => (
            <>
              <OverlayImage />
              <ImageEvent
                source={{
                  uri: cart.event.linkImage,
                }}
              />
            </>
          )}>
          <SectionInfo>
            <Header>
              <Title
                numberOfLines={2}
                align="left"
                margin="0"
                color={theme.sixthColor}>
                {cart.event.nome}
              </Title>
              <Subtitle
                numberOfLines={1}
                color={theme.sixthColor}
                align="left"
                margin="0">
                {moment(cart.event.data).format('dddd, DD MMM YYYY HH:mm')}
              </Subtitle>
            </Header>
            <Division />
            <Title
              align="left"
              color={theme.sixthColor}
              margin="16px 0px 0px 0px">
              {texts.selectCard}
            </Title>
            <ContainerCard>
              <RadioForm animation>
                {card.map((cardValue, i) => (
                  <RadioButton labelHorizontal key={i}>
                    <RadioButtonInput
                      obj={card}
                      index={i}
                      isSelected={selectedCardIndex === i}
                      onPress={() => setSelectedCardIndex(i)}
                      borderWidth={1}
                      buttonInnerColor={theme.sixthColor}
                      buttonOuterColor={theme.thirdColor}
                      buttonSize={10}
                      buttonOuterSize={20}
                      buttonWrapStyle={{marginLeft: 10}}
                    />
                    <Row>
                      <SectionCard>
                        <SectionIcon>
                          <Icon
                            source={
                              images[cardValue.brand.toLowerCase()] ||
                              images.cardDefault
                            }
                            resizeMode="contain"
                          />
                          <Subtitle
                            numberOfLines={1}
                            color={theme.sixthColor}
                            align="left"
                            margin="0px 8px">
                            {cardValue.brand}
                          </Subtitle>
                        </SectionIcon>
                        <Subtitle
                          numberOfLines={1}
                          color={theme.sixthColor}
                          align="left"
                          margin="0px 16px">
                          {cardValue.numberCard.slice(-4).padStart(8, '*')}
                        </Subtitle>
                      </SectionCard>
                      {selectedCardIndex === i ? (
                        <View>
                          <TextInput
                            label="CVV"
                            placeholder={texts.CVV}
                            keyboardType="numeric"
                            value={cardValue.cvv}
                            editable={false}
                          />
                        </View>
                      ) : (
                        undefined
                      )}
                    </Row>
                  </RadioButton>
                ))}
              </RadioForm>
            </ContainerCard>
            <Division />
            <SectionButton>
              <Button
                loading={
                  payment.isFetchValidate ||
                  order.isFetchOrderCreate ||
                  payment.isFetchCreate
                }
                title={texts.finalize}
                type="borded"
                onPress={onPressPayment}
              />
              <Button
                title={texts.newCardInsert}
                type="transparent"
                onPress={() => props.navigation.navigate('CardData')}
              />
            </SectionButton>
          </SectionInfo>
        </ParallaxScroll>
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};

const mapStateToProps = state => ({...state});
const mapDispatchToProps = {
  validateCart,
  createOrder,
  createPayment,
  getPaymentsInfo,
};

export default connect(mapStateToProps, mapDispatchToProps)(Event);
