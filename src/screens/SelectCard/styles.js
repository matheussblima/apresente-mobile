import styled from 'styled-components/native';

export const SectionInfo = styled.View`
  flex: 1;
  z-index: 10;
  padding: 24px;
  position: relative;
  padding-bottom: 140px;
  background-color: ${props => props.theme.fourthColor};
`;

export const OverlayImage = styled.View`
  background-color: rgba(0, 0, 0, 0.4);
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 20;
`;

export const ImageEvent = styled.Image`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
`;

export const Header = styled.View`
  margin-bottom: 16px;
`;

export const SectionButton = styled.View`
  margin-top: 24px;
  margin-bottom: 24px;
`;

export const SectionInfoEvent = styled.View`
  margin-top: 16px;
`;

export const ContentArtistInfo = styled.View`
  flex: 1;
  overflow: hidden;
  flex-direction: row;
  display: flex;
  margin-bottom: 24px;
`;

export const SectionInfoArtist = styled.View`
  flex: 1;
  display: flex;
  margin-top: 8px;
  justify-content: center;
`;

export const Container = styled.View``;

export const ContainerCard = styled.View`
  margin-top: 24px;
  margin-bottom: 24px;
`;

export const SectionCard = styled.View``;

export const Row = styled.View`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const SectionIcon = styled.View`
  display: flex;
  flex-direction: row;
  margin-left: 10px;
  align-items: center;
`;

export const Icon = styled.Image`
  width: 30px;
  height: 30px;
`;
