import React, {useState, useRef, useEffect} from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {ScrollView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import VMasker from 'vanilla-masker';
import DropdownAlert from 'react-native-dropdownalert';
import {editCard} from '../../redux/card';
import {
  Container,
  Logo,
  TextInput,
  Content,
  Button,
  HeaderClose,
  Title,
  Switch,
} from '../../components';
import {Header, SectionInput, SectionButton} from './styles';
import {texts} from '../../config';

const CardEdit = props => {
  const card = useSelector(state => state.card);
  const {cardSelected} = props.route.params || {};
  const dispatch = useDispatch();
  const [isFetchCard, setIsFetchCard] = useState(false);
  const [numberCard, setNumberCard] = useState('');
  const [nameCard, setNameCard] = useState('');
  const [validateCard, setValidateCard] = useState('');
  const [cvv, setCvv] = useState('');
  const [isDefault, setIsDefault] = useState(true);
  const dropDownAlertRef = useRef();
  const [cardInfo, setCardInfo] = useState(true);

  if (cardInfo) {
    setCvv(cardSelected.cvv);
    setValidateCard(`${cardSelected.month}/${cardSelected.year}`);
    setNameCard(`${cardSelected.nameCard}`);
    setNumberCard(
      VMasker.toPattern(cardSelected.numberCard, '9999 9999 9999 9999'),
    );
    setCardInfo(false);
    setIsDefault(cardSelected.isDefault);
  }

  useEffect(() => {
    if (isFetchCard) {
      if (card.isSuccessEdit) {
        props.navigation.goBack();
        setIsFetchCard(false);
      } else if (!card.isSuccessEdit && !card.isFetchEdit) {
        setIsFetchCard(false);
        dropDownAlertRef.current.alertWithType(
          'error',
          texts.errorAuth,
          card.message,
        );
      }
    }
  }, [card]);

  const onPressEditCard = item => {
    dispatch(
      editCard({
        numberCard: item.numberCard.split(' ').join(''),
        nameCard: item.nameCard,
        month: item.validateCard.split('/')[0],
        year: item.validateCard.split('/')[1],
        cvv: item.cvv,
        isDefault: item.isDefault,
      }),
    );
    setIsFetchCard(true);
  };

  return (
    <>
      <Container>
        <HeaderClose
          onPress={() => {
            props.navigation.goBack();
          }}
        />
        <ScrollView>
          <Content>
            <Header>
              <Logo />
              <Title margin="16px 0px 0px 0px">{texts.editCard}</Title>
            </Header>
            <KeyboardAwareScrollView>
              <SectionInput>
                <TextInput
                  placeholder={texts.numberCard}
                  value={numberCard}
                  editable={false}
                  keyboardType="numeric"
                  onChangeText={value => {
                    const mask = VMasker.toPattern(
                      value,
                      '9999 9999 9999 9999',
                    );
                    setNumberCard(mask);
                  }}
                />
                <TextInput
                  placeholder={texts.nameCard}
                  keyboardType="default"
                  editable={false}
                  value={nameCard}
                  onChangeText={value => setNameCard(value)}
                />
                <TextInput
                  placeholder={texts.validateCard}
                  keyboardType="numeric"
                  editable={false}
                  value={validateCard}
                  onChangeText={value => {
                    const mask = VMasker.toPattern(value, '99/99');
                    setValidateCard(mask);
                  }}
                />
                <TextInput
                  placeholder={texts.CVV}
                  keyboardType="numeric"
                  editable={false}
                  value={cvv}
                  onChangeText={value => setCvv(value)}
                />
                <Switch
                  title={texts.turnDefault}
                  value={isDefault}
                  onValueChange={value => setIsDefault(value)}
                />
              </SectionInput>
            </KeyboardAwareScrollView>
            <SectionButton>
              <Button
                loading={card.isFetch}
                title={texts.writeData}
                type="normal"
                onPress={() => {
                  onPressEditCard({
                    numberCard,
                    validateCard,
                    cvv,
                    nameCard,
                    isDefault,
                  });
                }}
              />
            </SectionButton>
          </Content>
        </ScrollView>
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};

export default CardEdit;
