import styled from 'styled-components/native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const Header = styled.View`
  align-items: center;
  margin-top: ${hp('3%')}px;
`;

export const SectionInput = styled.View`
  margin-top: ${hp('5%')}px;
`;

export const SectionButton = styled.View`
  margin-top: ${hp('5%')}px;
`;
