import React from 'react';
import {Modal, FlatList} from 'react-native';
import {
  Title,
  Division,
  HeaderBack,
  Container,
  Content,
  Badge,
} from '../../components';
import {theme} from '../../resources';
import {ContainerList, Touch, SectionListModal} from './styles';

export default props => {
  const renderItem = ({item}) => {
    return (
      <Touch activeOpacity={0.4} onPress={() => props.onPress(item)}>
        <ContainerList>
          <Title
            style={{textTransform: 'capitalize'}}
            align="left"
            weight="normal"
            color={theme.sixthColor}>
            {item.name}
          </Title>
          {item.selected ? <Badge /> : undefined}
        </ContainerList>
        <Division />
      </Touch>
    );
  };

  return (
    <Modal {...props}>
      <Container>
        <HeaderBack onPress={props.onPressClose} />
        <Content>
          <SectionListModal>
            <FlatList
              scrollEnabled={false}
              showsVerticalScrollIndicator={false}
              data={props.data}
              renderItem={items => renderItem(items)}
              keyExtractor={item => item.id.toString()}
            />
          </SectionListModal>
        </Content>
      </Container>
    </Modal>
  );
};
