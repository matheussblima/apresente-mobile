import styled from 'styled-components/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const SectionList = styled.View`
  margin-bottom: 24px;
`;

export const SectionListInfos = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const SectionListModal = styled.View`
  margin-top: 24px;
`;

export const IconList = styled.Image`
  width: ${wp('6%')}px;
  height: ${wp('6%')}px;
  margin-left: 8px;
`;

export const ContainerList = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 4px 0px;
`;

export const Touch = styled.TouchableOpacity``;
