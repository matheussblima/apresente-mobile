import React, {useState, useEffect} from 'react';
import {FlatList} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {
  Container,
  HeaderBack,
  Title,
  Content,
  Division,
  Badge,
  Button,
} from '../../components';
import {theme, images} from '../../resources';
import {findCategorys} from '../../redux/category';
import {setFilter} from '../../redux/filter';
import {findEventFilter, clearEventsFilter} from '../../redux/events';
import {
  SectionList,
  ContainerList,
  IconList,
  Touch,
  SectionListInfos,
} from './styles';
import Modal from './Modal';
import {texts} from '../../config';

const dataMenu = [
  {id: 1, name: 'Cidade'},
  {id: 2, name: 'Tipo de evento'},
  {id: 3, name: 'Período'},
];

const dataPeriod = [
  {id: 1, name: 'Hoje'},
  {id: 2, name: 'Nesta semana'},
  {id: 3, name: 'Neste ano'},
  {id: 4, name: 'Neste mês'},
];

const Filter = props => {
  const dispatch = useDispatch();
  const [showModalCategory, setModalCategory] = useState(false);
  const [showModalPeriod, setShowModalPeriod] = useState(false);
  const [showModalCity, setShowModalCity] = useState(false);
  const filter = useSelector(state => state.filter);
  const [city, setCity] = useState({});
  const [category, setCaregory] = useState({});
  const [period, setPeriod] = useState({});
  const citys = useSelector(state => state.citys);
  const categories = useSelector(state => state.category);

  useEffect(() => {
    dispatch(findCategorys());
  }, []);

  useEffect(() => {
    if (filter.filter) {
      setCity(filter.filter.city || {});
      setCaregory(filter.filter.category || {});
      setPeriod(filter.filter.period || {});
    }
  }, [filter]);

  const onPressListItem = item => {
    switch (item.id) {
      case 1:
        setShowModalCity(true);
        break;
      case 2:
        setModalCategory(true);
        break;
      case 3:
        setShowModalPeriod(true);
        break;
      default:
        break;
    }
  };

  const renderItem = ({item}) => {
    return (
      <Touch activeOpacity={0.4} onPress={() => onPressListItem(item)}>
        <ContainerList>
          <Title align="left" weight="normal" color={theme.sixthColor}>
            {item.name}
          </Title>
          <SectionListInfos>
            {Object.keys(city).length > 0 && item.id === 1 ? (
              <Badge />
            ) : (
              undefined
            )}

            {Object.keys(category).length > 0 && item.id === 2 ? (
              <Badge />
            ) : (
              undefined
            )}

            {Object.keys(period).length > 0 && item.id === 3 ? (
              <Badge />
            ) : (
              undefined
            )}
            <IconList source={images.arrowRight} />
          </SectionListInfos>
        </ContainerList>
        <Division />
      </Touch>
    );
  };

  return (
    <Container>
      <HeaderBack
        onPress={() => {
          props.navigation.goBack();
          dispatch(clearEventsFilter());
          dispatch(findEventFilter());
        }}
      />
      <Content>
        <SectionList>
          <Title align="left" color={theme.sixthColor}>
            Filtros para buscar
          </Title>
          <FlatList
            scrollEnabled={false}
            showsVerticalScrollIndicator={false}
            data={dataMenu}
            renderItem={items => renderItem(items)}
            keyExtractor={item => item.id.toString()}
          />
        </SectionList>
        <Button
          title={texts.fiter}
          type="normal"
          onPress={() => {
            dispatch(clearEventsFilter());
            dispatch(findEventFilter());
            props.navigation.navigate('Search');
          }}
        />
      </Content>

      <Modal
        animationType="slide"
        onPress={item => {
          setCity(item);
          setShowModalCity(false);
          dispatch(setFilter({city: {...item}, category, period}));
        }}
        data={
          citys.citys
            ? citys.citys.map(value => ({
                ...value,
                id: value.codCidade,
                name: value.cidade,
                selected: city.codCidade === value.codCidade,
              }))
            : []
        }
        transparent={false}
        visible={showModalCity}
        onPressClose={() => setShowModalCity(false)}
      />

      <Modal
        onPress={item => {
          setCaregory(item);
          setModalCategory(false);
          dispatch(setFilter({city, category: item, period}));
        }}
        selected={category}
        animationType="slide"
        data={
          categories.categories
            ? categories.categories.map(value => ({
                ...value,
                id: value.cod,
                name: value.nome,
                selected: category.cod === value.cod,
              }))
            : []
        }
        transparent={false}
        visible={showModalCategory}
        onPressClose={() => setModalCategory(false)}
      />

      <Modal
        onPress={item => {
          setPeriod(item);
          setShowModalPeriod(false);
          dispatch(setFilter({city, category, period: item}));
        }}
        selected={period}
        animationType="slide"
        data={dataPeriod.map(value => ({
          ...value,
          selected: period.id === value.id,
        }))}
        transparent={false}
        visible={showModalPeriod}
        onPressClose={() => setShowModalPeriod(false)}
      />
    </Container>
  );
};

export default Filter;
