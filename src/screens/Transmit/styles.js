import styled, {css} from 'styled-components/native';

export const ButtonCamera = styled.TouchableOpacity`
  height: 80px;
  width: 80px;
  border-color: ${props => props.theme.fourthColor};
  border-width: 6px;
  border-radius: 40px;
  align-items: center;
  justify-content: center;
`;

export const ButtonInside = styled.View`
  height: 54px;
  width: 54px;
  background-color: ${props => props.theme.ninethColor};
  border-radius: 30px;

  ${props =>
    props.active &&
    css`
      border-radius: 8px;
      height: 40px;
      width: 40px;
    `}
`;

export const Section = styled.View`
  flex: 1;
  align-items: flex-end;
  justify-content: flex-end;
  padding: 22px;
`;
