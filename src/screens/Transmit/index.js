import React, {useRef, useState} from 'react';
import {NodeCameraView} from 'react-native-nodemediaclient';
import {Container} from '../../components';
import {live} from '../../config';
import {Section, ButtonCamera, ButtonInside} from './styles';
import {theme} from '../../resources';

const Live = () => {
  const setCameraRef = useRef();
  const [play, setPlay] = useState();
  return (
    <Container backgroundColor={theme.tenthColor}>
      <NodeCameraView
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          height: '100%',
          width: '100%',
        }}
        ref={setCameraRef}
        outputUrl={live.outputUrl}
        camera={{cameraId: 1, cameraFrontMirror: true}}
        audio={live.audioConfig}
        video={live.videoConfig}
        smoothSkinLevel={3}
        autopreview
      />
      <Section>
        <ButtonCamera onPress={() => setPlay(!play)}>
          <ButtonInside active={play} />
        </ButtonCamera>
      </Section>
    </Container>
  );
};

export default Live;
