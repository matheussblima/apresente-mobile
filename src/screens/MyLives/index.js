import React, {useEffect, useState} from 'react';
import {FlatList, ScrollView, Image} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment';
import Skeleton from './Skeleton';
import {
  Container,
  EventInfo,
  Subtitle,
  Title,
  Content,
  Button,
} from '../../components';
import {findOrders} from '../../redux/order';
import {
  ContainerList,
  Header,
  SectionList,
  SectionBtn,
  SectionCadst,
  ContainerLives,
  PropImgs,
  ImagePc,
} from './styles';
import {theme, images} from '../../resources';
import {texts} from '../../config';

const MyLives = props => {
  const dispatch = useDispatch();
  const order = useSelector(states => states.order);
  const [screen, setScreen] = useState(0);

  useEffect(() => {
    dispatch(findOrders());
  }, [findOrders]);

  const renderItem = ({item}) => {
    return (
      <ContainerList
        activeOpacity={0.8}
        onPress={() => props.navigation.navigate('Dashboard')}>
        <EventInfo
          image={{uri: item.linkImage}}
          title={item.nameEvent}
          valorLive={texts.valorLive}
          valor1="$ 9,90"
          ingresso={texts.ingresso}
          valor2="235"
          faturamento={texts.faturamento}
          valor3="$ 350,00"
          overTitle={moment(item.dateEvent).format('ddd, DD MMM YYYY HH:mm')}
        />
      </ContainerList>
    );
  };

  const renderNewLives = () => {
    return (
      <ContainerList activeOpacity={0.8}>
        <ContainerLives>
          <PropImgs>
            <ImagePc source={images.pc} />
          </PropImgs>
          <Title
            size={wp('8%')}
            color={theme.sixthColor}
            align="center"
            margin="10px 0px 0px 0px">
            {'Cadastro de' + '\n' + 'nova Live'}
          </Title>
          <Title
            size={wp('4%')}
            color={theme.tenthColor}
            align="center"
            weight="normal"
            margin="5px 0px">
            Para melhorar sua experiência cadastre novas Lives por um Computador
          </Title>
        </ContainerLives>
      </ContainerList>
    );
  };

  const renderKeys = item => item.idOrder.toString();

  return (
    <Container>
      <Content>
        <Header>
          <SectionCadst>
            <Title
              size={wp('9%')}
              color={theme.primaryColor}
              align="left"
              margin="0px">
              {`${texts.myLive}\n${texts.myLive2}`}
            </Title>
            <Button
              onPress={() => props.navigation.navigate('NewLiveBank')}
              height="30px"
              padding="5px 15px"
              margin="10px 10px 0px 0px"
              sizeText={`${wp('4.2%')}px`}
              title={texts.cadastrarLives}
              type="normal"
              fontWeight="bold"
            />
          </SectionCadst>
          <SectionBtn>
            <Button
              onPress={() => setScreen(0)}
              height="26px"
              padding="1px 12px"
              margin="0px 10px 0px 0px"
              sizeText={`${wp('3.5%')}px`}
              title={texts.todas}
              type="borded"
              fontWeight="bold"
            />
            <Button
              onPress={() => setScreen(0)}
              height="26px"
              padding="1px 12px"
              margin="0px 10px 0px 0px"
              sizeText={`${wp('3.5%')}px`}
              title={texts.proximas}
              type="borded"
              fontWeight="bold"
            />
            <Button
              onPress={() => setScreen(0)}
              height="26px"
              padding="1px 12px"
              sizeText={`${wp('3.5%')}px`}
              title={texts.anteriores}
              type="borded"
              fontWeight="bold"
            />
          </SectionBtn>
        </Header>
      </Content>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Content>
          {screen === 1 ? (
            renderNewLives()
          ) : order.isSuccess ? (
            <SectionList>
              {order.orders.length > 0 ? (
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={order.orders}
                  renderItem={items => renderItem(items)}
                  keyExtractor={renderKeys}
                />
              ) : (
                <Subtitle align="center" color={theme.sixthColor}>
                  {texts.ticketEmpty}
                </Subtitle>
              )}
            </SectionList>
          ) : (
            <Skeleton />
          )}
        </Content>
      </ScrollView>
    </Container>
  );
};

export default MyLives;
