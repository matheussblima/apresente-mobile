import styled from 'styled-components/native';

export const ContainerList = styled.TouchableOpacity`
  margin: 8px 4px;
  padding: 8px 16px;
  background-color: #fff;
  shadow-color: ${props => props.theme.sixthColor};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.22;
  shadow-radius: 2.22px;
  elevation: 3;
  border-radius: 8px;
`;

export const Status = styled.View`
  margin-top: 8px;
  border-radius: 10px;
  background-color: ${props => props.theme.twelfthColor};
`;

export const PropImgs = styled.View`
text-align: center;
display: flex;
justify-content: center;
`

export const ImagePc = styled.Image`
  width: 95px;
  text-align: center;
  height: 65px;
  margin: 0 auto;
`

export const ContainerLives = styled.View`
  padding: 15px 0px;
`

export const SectionBtn = styled.View`
  display: flex;
  padding: 10px 0px;
  flex-direction: row;
`

export const SectionCadst = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding-top: 10px;
`

export const SectionList = styled.View``;

export const Header = styled.View``;
