import React, {useState, useRef, useEffect} from 'react';
import {ScrollView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import DropdownAlert from 'react-native-dropdownalert';
import {
  Container,
  Logo,
  TextInput,
  Content,
  Button,
  Title,
  Card,
} from '../../components';
import {Header, Section, SectionButton, SectionHeader} from './styles';
import {texts} from '../../config';
import {recoveryPassword} from '../../redux/auth';
import {theme} from '../../resources';

const Recovery = props => {
  const dropDownAlertRef = useRef();
  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);
  const [isFetchRecovery, setIsFetchRecovery] = useState(false);
  const [email, setEmail] = useState('');

  useEffect(() => {
    if (isFetchRecovery) {
      if (auth.isSuccessRecovery) {
        props.navigation.goBack();
        setIsFetchRecovery(false);
      } else if (!auth.isSuccessRecovery && !auth.isFetchRecovery) {
        dropDownAlertRef.current.alertWithType(
          'error',
          texts.errorRecovery,
          auth.message,
        );
        setIsFetchRecovery(false);
      }
    }
  }, [auth]);

  return (
    <>
      <Container>
        <ScrollView>
          <Content>
            <Header>
              <Logo />
            </Header>
            <Section>
              <Card>
                <SectionHeader>
                  <Title color={theme.sixthColor} align="left" margin="0px">
                    {texts.recoveryPassword}
                  </Title>
                  <Button
                    title={texts.doLogin}
                    type="transparent"
                    onPress={() => props.navigation.goBack()}
                  />
                </SectionHeader>
                <TextInput
                  keyboardType="email-address"
                  autoCapitalize="none"
                  placeholder={texts.yourEmail}
                  value={email}
                  onChangeText={value => setEmail(value)}
                />

                <SectionButton>
                  <Button
                    loading={auth.isFetchRecovery}
                    title={texts.recoveryPassword}
                    type="borded"
                    onPress={() => {
                      setIsFetchRecovery(true);
                      dispatch(recoveryPassword(email));
                    }}
                  />
                </SectionButton>
              </Card>
            </Section>
          </Content>
        </ScrollView>
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};

export default Recovery;
