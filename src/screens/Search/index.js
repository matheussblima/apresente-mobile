import React, { useState, useEffect } from 'react';
import { FlatList } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import {
  Container,
  HeaderBack,
  Content,
  Title,
  Tabs,
  Avatar,
  Subtitle,
  ListHorizontalImages,
  Division,
} from '../../components';
import {
  Header,
  Image,
  SearchInput,
  SearchContent,
  SectionInfo,
  ContentHeader,
  SectionList,
  SectionTab,
  SectionEvents,
  ListArtist,
  ListArtistInfo,
} from './styles';
import { findEventFilter, clearEventsFilter } from '../../redux/events';
import { setFilter } from '../../redux/filter';
import { texts } from '../../config';
import { images, theme } from '../../resources';

let timeout = 0;

const Search = props => {
  const dispatch = useDispatch();
  const events = useSelector(state => state.events);
  const [page, setPage] = useState(1);
  const [searchValue, setSearchValue] = useState('');
  const [hasMoreEvents, setHasMoreEvents] = useState(true);
  const [tab, setTab] = useState(1);

  useEffect(() => {
    dispatch(clearEventsFilter());
    dispatch(findEventFilter());
  }, []);

  const getEvents = () => {
    setHasMoreEvents(events.paginationInfoEventFilter.lastPage >= page + 1);
    if (events.paginationInfoEventFilter.lastPage >= page + 1) {
      dispatch(findEventFilter({ page: page + 1 }));
      setPage(page + 1);
    }
  };

  const onSearch = search => {
    setSearchValue(search);
    if (timeout) clearTimeout(timeout);
    timeout = setTimeout(() => {
      dispatch(clearEventsFilter());
      dispatch(findEventFilter({ search }));
    }, 500);
  };

  const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 50
    );
  };

  const renderLives = () => {
    return (
      <ListHorizontalImages
        onPress={item => {
          props.navigation.navigate('Event', { event: item });
        }}
        data={events.events
          .map(event => ({
            ...event,
            id: event.codEvento,
            image: { uri: event.linkImage },
          }))
          .sort(
            (a, b) =>
              new Date(a.data.split(' ')[0]) - new Date(b.data.split(' ')[0]),
          )}
      />
    );
  };

  const renderArtist = () => {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={[
          {
            id: '1',
            name: 'Murilo Coutos',
            lives: '26 Lives publicadas - 2.345 convidados',
            avatar:
              'https://noverbal.es/uploads/blog/rostro-de-un-criminal.jpg',
          },
          {
            id: '12',
            name: 'Murilo Coutos',
            lives: '26 Lives publicadas - 2.345 convidados',
            avatar:
              'https://noverbal.es/uploads/blog/rostro-de-un-criminal.jpg',
          },
          {
            id: '13',
            name: 'Murilo Coutos',
            lives: '26 Lives publicadas - 2.345 convidados',
            avatar:
              'https://noverbal.es/uploads/blog/rostro-de-un-criminal.jpg',
          },
          {
            id: '14',
            name: 'Murilo Coutos',
            lives: '26 Lives publicadas - 2.345 convidados',
            avatar:
              'https://noverbal.es/uploads/blog/rostro-de-un-criminal.jpg',
          },
        ]}
        renderItem={({ item }) => {
          return (
            <>
              <ListArtist onPress={item => {
                props.navigation.navigate('Artist', { event: item });
              }
              }>
                <Avatar marginRight={wp('3.5%')} size={wp('16%')} source={{ uri: item.avatar }} />
                <ListArtistInfo>
                  <Title
                    size={wp('9%')}
                    color={theme.sixthColor}
                    align="left"
                    margin="0px">
                    {item.name}
                  </Title>
                  <Subtitle margin="0px" align="left" color={theme.sixthColor}>
                    {item.lives}
                  </Subtitle>
                </ListArtistInfo>
              </ListArtist>
              <Division />
            </>
          );
        }}
        keyExtractor={item => item.id.toString()}
      />
    );
  };

  return (
    <Container>
      <ParallaxScroll
        onScroll={({ nativeEvent }) => {
          if (isCloseToBottom(nativeEvent) && hasMoreEvents) {
            getEvents();
          }
        }}
        scrollEventThrottle={400}
        onEndReached={getEvents}
        onEndReachedThreshold={0.1}
        parallaxHeight={hp('34%')}
        parallaxBackgroundScrollSpeed={5}
        parallaxForegroundScrollSpeed={2.5}
        isHeaderFixed
        headerHeight={50}
        isBackgroundScalable={false}
        renderParallaxForeground={() => (
          <ContentHeader>
            <Content>
              <Header>
                <HeaderBack
                  isLogo={false}
                  onPress={() => {
                    props.navigation.goBack();
                    dispatch(setFilter({ city: {}, category: {}, period: {} }));
                  }}>
                  <SearchContent>
                    <SearchInput
                      placeholder={texts.searchEvent}
                      value={searchValue}
                      onChangeText={value => onSearch(value)}
                    />
                    <Image source={images.search} resizeMode="contain" />
                  </SearchContent>
                </HeaderBack>
              </Header>
              <SectionInfo>
                <Title
                  size={wp('9%')}
                  weight="normal"
                  color={theme.primaryColor}
                  align="left"
                  margin="0px">
                  {events.isSuccessEventFilter ? events.eventsFilter.length : 0}{' '}
                  {texts.lives}
                </Title>
                <Title
                  size={wp('9%')}
                  color={theme.primaryColor}
                  align="left"
                  margin="0px">
                  {texts.findeds}
                </Title>
                <SectionTab>
                  <Tabs
                    onPressTab={item => {
                      setTab(item);
                    }}
                    tabOneTitle={texts.lives}
                    tabTwoTitle={texts.artists}
                    selectedColor={theme.primaryColor}
                    textColorSelected={theme.fourthColor}
                    textColorUnSelected={theme.primaryColor}
                    styleTabOne={{
                      borderTopLeftRadius: 24,
                      borderBottomLeftRadius: 24,
                      borderWidth: 1,
                      borderColor: theme.primaryColor,
                    }}
                    styleTabTwo={{
                      borderTopRightRadius: 24,
                      borderBottomRightRadius: 24,
                      borderWidth: 1,
                      borderColor: theme.primaryColor,
                    }}
                    selected={tab}
                  />
                </SectionTab>
              </SectionInfo>
            </Content>
          </ContentHeader>
        )}>
        <SectionEvents>
          <Content>
            {tab === 1 ? (
              <>
                <SectionList>{renderLives()}</SectionList>
                <SectionList>{renderLives()}</SectionList>
              </>
            ) : (
                <SectionList>{renderArtist()}</SectionList>
              )}
          </Content>
        </SectionEvents>
      </ParallaxScroll>
    </Container>
  );
};

export default Search;
