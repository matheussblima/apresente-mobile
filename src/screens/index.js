import Login from './Login';
import SingUp from './SingUp';
import Recovery from './Recovery';
import RecoveryVerifyEmail from './RecoveryVerifyEmail';
import CardData from './CardData';
import Home from './Home';
import Event from './Event';
import Payment from './Payment';
import Invoice from './Invoice';
import InvoiceError from './InvoiceError';
import Search from './Search';
import Filter from './Filter';
import Config from './Config';
import Tickets from './Tickets';
import Cards from './Cards';
import CardEdit from './CardEdit';
import TicketRequest from './TicketRequest';
import Help from './Help';
import TermsPolicies from './TermsPolicies';
import HelpInfo from './HelpInfo';
import EditPerfil from './EditPerfil';
import SelectCard from './SelectCard';
import MyLives from './MyLives';
import Artist from './Artist';
import Live from './Live';
import Dashboard from './Dashboard';
import NewLiveBank from './NewLiveBank';
import NewLiveData from './NewLiveData';
import Onboarding from './Onboarding';
import Transmit from './Transmit';

export {
  Login,
  SingUp,
  Recovery,
  RecoveryVerifyEmail,
  Onboarding,
  CardData,
  Home,
  Event,
  Payment,
  Live,
  Invoice,
  Search,
  Filter,
  Config,
  Tickets,
  Cards,
  CardEdit,
  TicketRequest,
  Help,
  TermsPolicies,
  HelpInfo,
  EditPerfil,
  SelectCard,
  MyLives,
  Artist,
  Dashboard,
  NewLiveBank,
  NewLiveData,
  InvoiceError,
  Transmit,
};
