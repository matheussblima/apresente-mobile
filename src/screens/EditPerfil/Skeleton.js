import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export default () => {
  return (
    <SkeletonPlaceholder>
      <View style={{width: '100%', height: 40, marginTop: 16}} />
      <View style={{width: '100%', height: 40, marginTop: 16}} />
      <View style={{width: '100%', height: 40, marginTop: 16}} />
      <View style={{width: '100%', height: 40, marginTop: 16}} />
      <View style={{width: '100%', height: 40, marginTop: 16}} />
    </SkeletonPlaceholder>
  );
};
