import React, {useState, useRef, useEffect} from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {ScrollView} from 'react-native';
import VMasker from 'vanilla-masker';
import {useDispatch, useSelector} from 'react-redux';
import DropdownAlert from 'react-native-dropdownalert';
import {
  Container,
  Logo,
  TextInput,
  Content,
  Button,
  HeaderClose,
  Title,
} from '../../components';
import Skeleton from './Skeleton';
import {Header, SectionInput, SectionButton} from './styles';
import {texts} from '../../config';
import {getProfile, editProfile} from '../../redux/auth';

const EditPerfil = props => {
  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);
  const [email, setEmail] = useState('');
  const [isFetchEditProfile, setIsFetchEditProfile] = useState(false);
  const [cell, setCell] = useState('');
  const [cpf, setCpf] = useState('');
  const [name, setName] = useState('');
  const [cep, setCep] = useState('');
  const [houseNumber, setHouseNumber] = useState('');
  const dropDownAlertRef = useRef();

  useEffect(() => {
    dispatch(getProfile());
  }, []);

  useEffect(() => {
    setName(auth.profile.nome);
    setCpf(VMasker.toPattern(auth.profile.cpf, '999.999.999-99'));
    setCell(VMasker.toPattern(auth.profile.celular, '(99) 99999-9999'));
    setCep(VMasker.toPattern(auth.profile.cep, '99999-999'));
    setEmail(auth.profile.email);
    setHouseNumber(auth.profile.numero);

    if (isFetchEditProfile && auth.isSuccessEditProfile) {
      props.navigation.goBack();
      setIsFetchEditProfile(false);
    }
  }, [auth]);

  return (
    <>
      <Container>
        <HeaderClose onPress={() => props.navigation.goBack()} />
        <ScrollView>
          <Content>
            <Header>
              <Logo />
              <Title margin="16px 0px 0px 0px">{texts.editProfile}</Title>
            </Header>
            {auth.isSuccessGetProfile ? (
              <>
                <KeyboardAwareScrollView>
                  <SectionInput>
                    <TextInput
                      editable={false}
                      placeholder={texts.yourName}
                      value={name}
                      keyboardType="default"
                      onChangeText={value => setName(value)}
                    />
                    <TextInput
                      editable={false}
                      placeholder={texts.cpf}
                      keyboardType="numeric"
                      value={cpf}
                      onChangeText={value => {
                        const mask = VMasker.toPattern(value, '999.999.999-99');
                        setCpf(mask);
                      }}
                    />
                    <TextInput
                      editable={false}
                      placeholder={texts.yourEmail}
                      value={email}
                      keyboardType="email-address"
                      autoCapitalize="none"
                      onChangeText={value => setEmail(value)}
                    />
                    <TextInput
                      placeholder={texts.yourCell}
                      value={cell}
                      keyboardType="phone-pad"
                      onChangeText={value => {
                        const mask = VMasker.toPattern(
                          value,
                          '(99) 99999-9999',
                        );
                        setCell(mask);
                      }}
                    />
                    <TextInput
                      placeholder={texts.cep}
                      keyboardType="numeric"
                      value={cep}
                      onChangeText={value => {
                        const mask = VMasker.toPattern(value, '99999-999');
                        setCep(mask);
                      }}
                    />
                    <TextInput
                      placeholder={texts.houseNumber}
                      keyboardType="numeric"
                      value={houseNumber}
                      onChangeText={value => setHouseNumber(value)}
                    />
                  </SectionInput>
                </KeyboardAwareScrollView>
                <SectionButton>
                  <Button
                    loading={auth.isFetchEditProfile}
                    title={texts.edit}
                    type="normal"
                    onPress={() => {
                      dispatch(editProfile({cell, cep, houseNumber}));
                      setIsFetchEditProfile(true);
                      props.navigation.goBack();
                    }}
                  />
                </SectionButton>
              </>
            ) : (
              <Skeleton />
            )}
          </Content>
        </ScrollView>
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};

export default EditPerfil;
