import React, {useEffect, useState} from 'react';
import {FlatList, ActivityIndicator} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {
  Container,
  HeaderBack,
  Content,
  Title,
  Subtitle,
  Division,
} from '../../components';
import {getFaqs} from '../../redux/faq';
import {Header, SectionList, ContainerList} from './styles';
import {theme} from '../../resources';
import {texts} from '../../config';

const Help = props => {
  const dispatch = useDispatch();
  const faq = useSelector(states => states.faq);
  const [page, setPage] = useState(1);

  useEffect(() => {
    dispatch(getFaqs({}));
  }, [getFaqs]);

  const renderKeys = item => item.codFaq.toString();
  const listItems = ({item}) => {
    return (
      <>
        <ContainerList
          onPress={() => {
            props.navigation.navigate('HelpInfo', {faq: item});
          }}>
          <Subtitle margin="0" align="left" color={theme.sixthColor}>
            {item.pergunta}
          </Subtitle>
        </ContainerList>
        <Division />
      </>
    );
  };

  const paginationGetFaq = () => {
    dispatch(getFaqs({page: page + 1}));
    setPage(page + 1);
  };

  return (
    <Container>
      <HeaderBack
        onPress={() => {
          props.navigation.goBack();
        }}
      />
      <Content style={{flex: 1}}>
        <Header>
          <Title margin="8px 0 0 0" align="left" color={theme.sixthColor}>
            {texts.help}
          </Title>
          <Subtitle margin="0" align="left" color={theme.sixthColor}>
            {texts.seeFaq}
          </Subtitle>
        </Header>
        <SectionList>
          <FlatList
            renderItem={item => listItems(item, props)}
            onEndReached={paginationGetFaq}
            onEndReachedThreshold={0.1}
            data={faq.faqs}
            keyExtractor={renderKeys}
            ListFooterComponent={() => {
              return faq.isFetch ? (
                <ActivityIndicator style={{marginTop: 16}} size="large" />
              ) : null;
            }}
            refreshing={faq.isFetch}
          />
        </SectionList>
      </Content>
    </Container>
  );
};

export default Help;
