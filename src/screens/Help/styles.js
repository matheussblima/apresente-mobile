import styled from 'styled-components/native';

export const Header = styled.View``;

export const SectionList = styled.View`
  flex: 1;
  margin-top: 24px;
`;

export const ContainerList = styled.TouchableOpacity`
  margin: 8px 4px;
  padding: 8px 16px;
  border-radius: 8px;
`;
