import React, {useEffect} from 'react';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import {useSelector, useDispatch} from 'react-redux';
import {Title, Subtitle, HeaderClose, Division, Button} from '../../components';
import {
  SectionInfo,
  ImageEvent,
  OverlayImage,
  Header,
  Container,
  ContainerCard,
  SectionButton,
  Icon,
} from './styles';
import {theme, images} from '../../resources';
import {texts} from '../../config';
import Skeleton from './Skeleton';
import {findOrder} from '../../redux/order';

const Event = props => {
  const cart = useSelector(state => state.cart);
  const {isSuccessGetOrder} = useSelector(state => state.order);

  return (
    <Container>
      <ParallaxScroll
        isHeaderFixed={false}
        parallaxHeight={hp('20%')}
        parallaxBackgroundScrollSpeed={5}
        parallaxForegroundScrollSpeed={2.5}
        renderHeader={() => (
          <HeaderClose
            onPress={() => props.navigation.goBack()}
            colorIcon={theme.fourthColor}
          />
        )}
        renderParallaxBackground={() => (
          <>
            <OverlayImage />
            <ImageEvent
              source={{
                uri: cart.event.linkImage,
              }}
            />
          </>
        )}>
        <SectionInfo>
          <Header>
            <Icon source={images.smileysBad} />
            <Title numberOfLines={2} margin="0" color={theme.sixthColor}>
              {texts.noGo}
            </Title>
            <Subtitle numberOfLines={1} color={theme.sixthColor} margin="0">
              {texts.paymentNo}
            </Subtitle>
          </Header>
          <Division />
          <ContainerCard>
            <Subtitle color={theme.sixthColor} margin="0">
              {texts.notAutorized}
            </Subtitle>
          </ContainerCard>
          <Division />
          <SectionButton>
            <Button
              title={texts.changePayment}
              type="borded"
              onPress={() => props.navigation.navigate('SelectCard')}
            />
          </SectionButton>
        </SectionInfo>
      </ParallaxScroll>
    </Container>
  );
};

export default Event;
