import React, {useState, useEffect} from 'react';
import {ScrollView, View} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import VMasker from 'vanilla-masker';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {
  HeaderSection,
  Section,
  PerfilAvatarSection,
  SectionTab,
  SectionButton,
  Row,
  HeaderCard,
  SectionCards,
  SectionCard,
  SectionIcon,
  Icon,
  ItemsCard,
} from './styles';
import {
  Container,
  Avatar,
  Title,
  TextInput,
  Button,
  Card,
  Content,
  Subtitle,
  Division,
} from '../../components';
import {theme, images} from '../../resources';
import {texts} from '../../config';
import {deleteCard} from '../../redux/card';

const Config = props => {
  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);
  const [email, setEmail] = useState('');
  const [tab, setTab] = useState(1);
  const [cell, setCell] = useState('');
  const [cep, setCep] = useState('');
  const [name, setName] = useState('');
  const [cpf, setCpf] = useState('');
  const [facebook, setFacebook] = useState('');
  const [instagram, setInstagram] = useState('');
  const [twitter, setTwitter] = useState('');
  const [youTube, setYoutube] = useState('');
  const [bank, setBank] = useState('');
  const [agencyBank, setAgencyBank] = useState('');
  const [acount, setAcount] = useState('');
  const {card} = useSelector(state => state.card);
  const [digitBank, setDigitBank] = useState('');
  const [typeCount, setTypeCount] = useState('');

  console.log(auth.auth.data.adress);

  useEffect(() => {
    setName(auth.auth.data.nome);
    setEmail(auth.auth.data.email);
    setCell(VMasker.toPattern(auth.auth.data.celular, '(99) 99999-9999'));
    setCep(VMasker.toPattern(auth.auth.data.address.cep, '99999-999'));
    setCpf(VMasker.toPattern(auth.auth.data.cpf, '999.999.999-99'));
  }, []);

  const renderDados = () => {
    return (
      <>
        <Title
          size={wp('6%')}
          weight="normal"
          color={theme.tenthColor}
          align="left"
          margin="0px">
          {texts.yourDataPrivaity}
        </Title>
        <Subtitle margin="0px" align="left" color={theme.thirteenthColor}>
          {texts.forYourSecuryDontAlter}
        </Subtitle>
        <KeyboardAwareScrollView>
          <TextInput
            editable={false}
            placeholder={texts.yourName}
            value={name}
            keyboardType="default"
            onChangeText={value => setName(value)}
          />
          <TextInput
            editable={false}
            placeholder={texts.yourEmail}
            value={email}
            keyboardType="email-address"
            autoCapitalize="none"
            onChangeText={value => setEmail(value)}
          />
          <TextInput
            editable={false}
            placeholder={texts.yourCPF}
            value={cpf}
            keyboardType="numeric"
            onChangeText={value => {
              const mask = VMasker.toPattern(value, '999.999.999-99');
              setCpf(mask);
            }}
          />
          <TextInput
            placeholder={texts.yourCell}
            value={cell}
            keyboardType="phone-pad"
            onChangeText={value => {
              const mask = VMasker.toPattern(value, '(99) 99999-9999');
              setCell(mask);
            }}
          />
          <TextInput
            placeholder={texts.cep}
            value={cep}
            keyboardType="numeric"
            onChangeText={value => {
              const mask = VMasker.toPattern(value, '99999-999');
              setCep(mask);
            }}
          />
          <SectionButton>
            <Button title={texts.edit} type="borded" fontWeight="bold" />
          </SectionButton>
        </KeyboardAwareScrollView>
      </>
    );
  };

  const renderNetwork = () => {
    return (
      <>
        <Title
          size={wp('6%')}
          weight="normal"
          color={theme.tenthColor}
          align="left"
          margin="0px">
          {texts.yourNetworks}
        </Title>
        <KeyboardAwareScrollView>
          <TextInput
            editable={false}
            placeholder={texts.facebook}
            value={facebook}
            keyboardType="default"
            onChangeText={value => setFacebook(value)}
          />
          <TextInput
            editable={false}
            placeholder={texts.instagram}
            value={instagram}
            keyboardType="default"
            onChangeText={value => setInstagram(value)}
          />
          <TextInput
            editable={false}
            placeholder={texts.twitter}
            value={twitter}
            keyboardType="default"
            onChangeText={value => setTwitter(value)}
          />
          <TextInput
            placeholder={texts.youtube}
            value={youTube}
            keyboardType="default"
            onChangeText={value => setYoutube(value)}
          />
          <SectionButton>
            <Button title={texts.edit} type="borded" fontWeight="bold" />
          </SectionButton>
        </KeyboardAwareScrollView>
      </>
    );
  };

  const renderDataBank = () => {
    return (
      <>
        <Title
          size={wp('6%')}
          weight="normal"
          color={theme.tenthColor}
          align="left"
          margin="0px">
          {texts.bankData}
        </Title>
        <Subtitle margin="0px" align="left" color={theme.thirteenthColor}>
          {texts.samePersonBankDeposit}
        </Subtitle>
        <KeyboardAwareScrollView>
          <TextInput
            placeholder={texts.bank}
            value={bank}
            keyboardType="default"
            onChangeText={value => setBank(value)}
          />
          <TextInput
            placeholder={texts.agencyBank}
            value={agencyBank}
            keyboardType="default"
            onChangeText={value => setAgencyBank(value)}
          />
          <Row>
            <TextInput
              placeholder={texts.acount}
              value={acount}
              keyboardType="default"
              onChangeText={value => setAcount(value)}
            />
            <TextInput
              placeholder={texts.digit}
              value={digitBank}
              keyboardType="default"
              onChangeText={value => setDigitBank(value)}
            />
          </Row>
          <TextInput
            placeholder={texts.typeAcount}
            value={typeCount}
            keyboardType="default"
            onChangeText={value => setTypeCount(value)}
          />
          <SectionButton>
            <Button title={texts.edit} type="borded" fontWeight="bold" />
          </SectionButton>
        </KeyboardAwareScrollView>
      </>
    );
  };

  const renderMyCards = () => {
    return (
      <>
        <HeaderCard>
          <Title
            size={wp('6%')}
            weight="normal"
            color={theme.tenthColor}
            align="left"
            margin="0px">
            {texts.yourCads}
          </Title>
          <Button
            onPress={() =>
              props.navigation.navigate('CardData', {previousScreen: 'Cards'})
            }
            height="44px"
            padding="1px 16px"
            sizeText={`${wp('5%')}px`}
            title={texts.newCardInsert}
            type="normal"
            fontWeight="bold"
          />
        </HeaderCard>
        <SectionCards>
          <Division />

          {card.map((cardValue, i) => (
            <SectionCard key={i}>
              <ItemsCard>
                <Row>
                  <SectionIcon>
                    <Icon
                      source={
                        images[cardValue.brand.toLowerCase()] ||
                        images.cardDefault
                      }
                      resizeMode="contain"
                    />
                    <Subtitle
                      numberOfLines={1}
                      color={theme.sixthColor}
                      align="left"
                      margin="0px 8px">
                      {cardValue.brand}
                    </Subtitle>
                  </SectionIcon>
                  <Subtitle
                    numberOfLines={1}
                    color={theme.sixthColor}
                    align="left"
                    margin="0px 16px">
                    {cardValue.numberCard.slice(-4).padStart(8, '*')}
                  </Subtitle>
                </Row>
                <View>
                  <Button
                    color={theme.twelfthColor}
                    borderColor={theme.twelfthColor}
                    onPress={() => dispatch(deleteCard(cardValue.numberCard))}
                    height="30px"
                    padding="1px 12px"
                    sizeText={`${wp('4%')}px`}
                    title={texts.remove}
                    type="borded"
                    fontWeight="bold"
                  />
                </View>
              </ItemsCard>
            </SectionCard>
          ))}
        </SectionCards>
      </>
    );
  };

  return (
    <Container>
      <ScrollView>
        <Content>
          <HeaderSection>
            <PerfilAvatarSection>
              <Avatar
                size={wp('22%')}
                sizeText={wp('10%')}
                initialsName={auth.auth.data.nome.slice(0, 2)}
              />
              <Title
                size={wp('9%')}
                color={theme.primaryColor}
                align="left"
                margin="0px 24px">
                {texts.myProfile.split(' ').join('\n')}
              </Title>
            </PerfilAvatarSection>
            <SectionTab>
              <Button
                onPress={() => setTab(1)}
                height="26px"
                padding="1px 8px"
                sizeText={`${wp('2.4%')}px`}
                title={texts.myData}
                type={tab === 1 ? 'normal' : 'borded'}
                fontWeight="bold"
              />
              <Button
                onPress={() => setTab(2)}
                height="26px"
                padding="1px 8px"
                sizeText={`${wp('2.4%')}px`}
                title={texts.myNetWork}
                type={tab === 2 ? 'normal' : 'borded'}
                fontWeight="bold"
              />
              <Button
                onPress={() => setTab(3)}
                height="26px"
                padding="1px 8px"
                sizeText={`${wp('2.4%')}px`}
                title={texts.myDataBank}
                type={tab === 3 ? 'normal' : 'borded'}
                fontWeight="bold"
              />
              <Button
                onPress={() => setTab(4)}
                height="26px"
                padding="1px 8px"
                sizeText={`${wp('2.4%')}px`}
                title={texts.myCards}
                type={tab === 4 ? 'normal' : 'borded'}
                fontWeight="bold"
              />
            </SectionTab>
          </HeaderSection>
          <Section>
            <Card>
              {tab === 1 ? renderDados() : undefined}
              {tab === 2 ? renderNetwork() : undefined}
              {tab === 3 ? renderDataBank() : undefined}
              {tab === 4 ? renderMyCards() : undefined}
            </Card>
          </Section>
        </Content>
      </ScrollView>
    </Container>
  );
};

export default Config;
