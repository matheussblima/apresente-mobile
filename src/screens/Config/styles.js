import styled from 'styled-components/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const HeaderSection = styled.View`
  margin: 20px 0px;
`;

export const Section = styled.View`
  margin: 24px 0px;
`;

export const ItemList = styled.View`
  margin: 16px 0px;
  flex-direction: row;
  align-items: center;
`;

export const ItemListLeft = styled.View`
  flex: 4;
`;

export const ItemListRight = styled.View`
  flex: 1;
  align-items: flex-end;
  justify-content: center;
`;

export const ImageList = styled.Image`
  height: ${wp('9%')}px;
  width: ${wp('9%')}px;
  margin-right: 8px;
`;

export const PerfilAvatarSection = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const SectionTab = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 32px;
`;

export const SectionButton = styled.View`
  margin-top: 32px;
`;

export const Row = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const HeaderCard = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const SectionCards = styled.View`
  margin-top: 64px;
`;

export const ItemsCard = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 12px;
  justify-content: space-between;
`;

export const SectionCard = styled.View``;

export const SectionIcon = styled.View`
  display: flex;
  flex-direction: row;
  margin-left: 10px;
  align-items: center;
`;

export const Icon = styled.Image`
  width: 30px;
  height: 30px;
`;
