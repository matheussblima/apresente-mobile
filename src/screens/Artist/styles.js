import styled from 'styled-components/native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';


export const ContainerList = styled.TouchableOpacity`
  margin: 8px 4px;
  padding: 8px 16px;
  background-color: #fff;
  shadow-color: ${props => props.theme.sixthColor};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.22;
  shadow-radius: 2.22px;
  elevation: 3;
  border-radius: 8px;
`;

export const Status = styled.View`
  margin-top: 8px;
  border-radius: 10px;
  display: flex;
  flex-direction: row;
  background-color: ${props => props.theme.twelfthColor};
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 8px;
  justify-content: space-between;
`;

export const Image = styled.Image`
  width: ${wp('5%')}px;
  height: ${wp('5%')}px;
`;

export const Touch = styled.TouchableOpacity`
  background-color: ${props => props.theme.thirdColor};
  border-radius: ${wp('6%')}px;
  margin: 0 6px;
  align-items: center;
  padding: 4px;
  justify-content: center;
  width: ${wp('10%')}px;
  height: ${wp('10%')}px;
`;

export const ListArtist = styled.View`
  display: flex;
  flex-direction: row;
  margin-bottom: 5px;
  margin-top: 0px;
`;

export const ListArtistInfo = styled.View`
  display: flex;
  flex: 1;
`;

export const SearchInput = styled.TextInput`
  font-size: ${props => props.fontSize || wp('5%')}px;
  flex: 1;
  color: ${props => props.theme.sixthColor || props.underlineColor};
`;

export const SearchContent = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-radius: 8px;
  height: ${wp('14%')}px;
  padding: 0px 12px;
  color: ${props => props.theme.sixthColor || props.underlineColor};
  margin: 12px 0px 0px 0px;
  margin-right: 8px;
  background-color: ${props => props.theme.fourthColor};
  shadow-color: ${props => props.theme.sixthColor};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.22;
  shadow-radius: 2.22px;
`;

export const SectionInfo = styled.View`
  margin: 8px 0px;
  margin-top: 10px;
  justify-content: space-between;
`;

export const ContentHeader = styled.View``;

export const SectionList = styled.View`
  margin-top: 20px;
`;

export const SectionEvents = styled.View`
padding-bottom: 10px;
`;

export const SectionTab = styled.View`
  margin: 16px 0px;
`;

export const ButtonIcons = styled.TouchableOpacity``;
export const SectionIcons = styled.View`
  display: flex;
  flex-direction: row;
  padding-top: 30px;
`;
export const Icons = styled.Image`
  width: 32px;
  height: 32px;
  margin-left: 10px;
`;
