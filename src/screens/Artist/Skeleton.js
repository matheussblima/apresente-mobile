import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export default () => {
  return (
    <SkeletonPlaceholder>
      <View style={{width: '40%', height: 32, marginVertical: 8}} />
      <View style={{width: '100%', height: 150, marginVertical: 8}} />
      <View style={{width: '100%', height: 150, marginVertical: 8}} />
      <View style={{width: '100%', height: 150, marginVertical: 8}} />
      <View style={{width: '100%', height: 150, marginVertical: 8}} />
      <View style={{width: '100%', height: 150, marginVertical: 8}} />
    </SkeletonPlaceholder>
  );
};
