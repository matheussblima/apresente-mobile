import React, {useState, useEffect} from 'react';
import {FlatList, Share, Image, TouchableOpacity} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import moment from 'moment';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import Skeleton from './Skeleton';
import {
  Container,
  HeaderBack,
  Content,
  EventInfo,
  Title,
  Avatar,
  Subtitle,
  Division,
} from '../../components';
import {
  Header,
  Image as Imagers,
  SearchInput,
  SearchContent,
  ContentHeader,
  SectionList,
  SectionEvents,
  ListArtist,
  ListArtistInfo,
  SectionIcons,
  Icons,
  ButtonIcons,
  ContainerList,
  Status,
} from './styles';
import {findEventFilter, clearEventsFilter} from '../../redux/events';
import {setFilter} from '../../redux/filter';
import {texts} from '../../config';
import {setLikes} from '../../redux/like';
import {findOrders} from '../../redux/order';
import {images, theme} from '../../resources';

let timeout = 0;

const Artist = props => {
  const dispatch = useDispatch();
  const events = useSelector(state => state.events);
  const [page, setPage] = useState(1);
  const [searchValue, setSearchValue] = useState('');
  const [hasMoreEvents, setHasMoreEvents] = useState(true);
  const [like, setLike] = useState(false);
  const {eventLike} = useSelector(state => state.like);
  const order = useSelector(states => states.order);
  const eventParam = props.route.params;

  useEffect(() => {
    dispatch(clearEventsFilter());
    dispatch(findEventFilter());
    dispatch(findOrders());
    setLike(
      !!eventLike.find(
        likeValue => eventParam.event.codEvento === likeValue.codeEvent,
      ),
    );
  }, [findOrders]);

  const onShare = async () => {
    try {
      await Share.share({
        message: eventParam.event.link,
      });
    } catch (error) {
      alert(error.message);
    }
  };

  const getEvents = () => {
    setHasMoreEvents(events.paginationInfoEventFilter.lastPage >= page + 1);
    if (events.paginationInfoEventFilter.lastPage >= page + 1) {
      dispatch(findEventFilter({page: page + 1}));
      setPage(page + 1);
    }
  };

  const onSearch = search => {
    setSearchValue(search);
    if (timeout) clearTimeout(timeout);
    timeout = setTimeout(() => {
      dispatch(clearEventsFilter());
      dispatch(findEventFilter({search}));
    }, 500);
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 50
    );
  };

  const renderItem = ({item}) => {
    return (
      <ContainerList activeOpacity={0.8}>
        <EventInfo
          image={{uri: item.linkImage}}
          title={item.nameEvent}
          height={wp('36%')}
          overTitle={moment(item.dateEvent).format('ddd, DD MMM YYYY HH:mm')}
          artist
          precoLive="$ 9"
        />
      </ContainerList>
    );
  };

  const renderKeys = item => item.idOrder.toString();

  const renderArtist = () => {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={[
          {
            id: '1',
            name: 'Murilo',
            sobreName: 'Couto',
            lives: '26 Lives publicadas - 2.345 convidados',
            avatar:
              'https://noverbal.es/uploads/blog/rostro-de-un-criminal.jpg',
          },
        ]}
        renderItem={({item}) => {
          return (
            <>
              <Title
                size={wp('9%')}
                weight="normal"
                color={theme.primaryColor}
                align="left"
                margin="0px 0px 0px 0px">
                {texts.artista}
              </Title>
              <ListArtist>
                <Avatar
                  marginRight={wp('3.5%')}
                  size={wp('16%')}
                  source={{uri: item.avatar}}
                />
                <ListArtistInfo>
                  <Title
                    size={wp('9%')}
                    weight="bold"
                    color={theme.primaryColor}
                    align="left"
                    margin="0px">
                    {item.name}
                  </Title>
                  <Title
                    size={wp('9%')}
                    weight="bold"
                    color={theme.primaryColor}
                    align="left"
                    margin="-5px 0px 0px 0px">
                    {item.sobreName}
                  </Title>
                </ListArtistInfo>
                <SectionIcons>
                  <ButtonIcons
                    onPress={() => {
                      dispatch(setLikes(eventParam.event.codEvento));
                      setLike(!like);
                    }}>
                    <Icons
                      source={like ? images.like : images.unlike}
                      resizeMode="contain"
                    />
                  </ButtonIcons>
                  <ButtonIcons onPress={onShare}>
                    <Icons source={images.share} resizeMode="contain" />
                  </ButtonIcons>
                </SectionIcons>
              </ListArtist>
              <Subtitle
                fontStyle="italic"
                size={wp('4%')}
                margin="0px"
                align="left"
                color={theme.sixthColor}>
                {item.lives}
              </Subtitle>
              <Division style={{marginTop: 10}} />
            </>
          );
        }}
        keyExtractor={item => item.id.toString()}
      />
    );
  };

  return (
    <Container>
      <ParallaxScroll
        onScroll={({nativeEvent}) => {
          if (isCloseToBottom(nativeEvent) && hasMoreEvents) {
            getEvents();
          }
        }}
        scrollEventThrottle={400}
        onEndReached={getEvents}
        onEndReachedThreshold={0.1}
        parallaxHeight={hp('100%')}
        parallaxBackgroundScrollSpeed={5}
        parallaxForegroundScrollSpeed={2.5}
        isHeaderFixed
        headerHeight={50}
        isBackgroundScalable={false}
        renderParallaxForeground={() => (
          <ContentHeader>
            <Content>
              <Header>
                <HeaderBack
                  isLogo={false}
                  onPress={() => {
                    props.navigation.goBack();
                    dispatch(setFilter({city: {}, category: {}, period: {}}));
                  }}>
                  <SearchContent>
                    <SearchInput
                      placeholder={texts.searchEvent}
                      value={searchValue}
                      onChangeText={value => onSearch(value)}
                    />
                    <Imagers source={images.search} resizeMode="contain" />
                  </SearchContent>
                </HeaderBack>
              </Header>
            </Content>
            <SectionEvents>
              <Content>
                <SectionList>{renderArtist()}</SectionList>
              </Content>
            </SectionEvents>
            <Content>
              <Title
                size={wp('9%')}
                color={theme.primaryColor}
                align="left"
                margin="0px 0px -5px 0px">
                {texts.nextsExperiences}
              </Title>
              {order.isSuccess ? (
                <SectionList>
                  {order.orders.length > 0 ? (
                    <FlatList
                      showsVerticalScrollIndicator={false}
                      data={order.orders}
                      renderItem={items => renderItem(items)}
                      keyExtractor={renderKeys}
                    />
                  ) : (
                    <Subtitle align="center" color={theme.sixthColor}>
                      {texts.ticketEmpty}
                    </Subtitle>
                  )}
                </SectionList>
              ) : (
                <Skeleton />
              )}
            </Content>
          </ContentHeader>
        )}
      />
    </Container>
  );
};

export default Artist;
