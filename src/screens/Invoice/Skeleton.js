import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export default () => {
  return (
    <SkeletonPlaceholder>
      <View
        style={{
          width: '100%',
          height: 30,
          marginBottom: 16,
          marginTop: 16,
        }}
      />
      <View
        style={{
          width: '100%',
          height: 150,
          marginBottom: 16,
          alignSelf: 'center',
        }}
      />
      <View
        style={{
          width: '100%',
          height: 30,
          marginBottom: 16,
          alignSelf: 'center',
        }}
      />
      <View
        style={{
          width: '100%',
          height: 30,
          marginBottom: 16,
          alignSelf: 'center',
        }}
      />
      <View
        style={{
          width: '100%',
          height: 30,
          marginBottom: 16,
          alignSelf: 'center',
        }}
      />
      <View
        style={{
          width: '100%',
          height: 30,
          marginBottom: 16,
          alignSelf: 'center',
        }}
      />
    </SkeletonPlaceholder>
  );
};
