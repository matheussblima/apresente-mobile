import React, {useEffect} from 'react';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import {useSelector, useDispatch} from 'react-redux';
import {Title, Subtitle, HeaderClose, Division, Button} from '../../components';
import {
  SectionInfo,
  ImageEvent,
  OverlayImage,
  Header,
  Container,
  ContainerCard,
  SectionButton,
  Icon,
} from './styles';
import {theme, images} from '../../resources';
import {texts} from '../../config';
import Skeleton from './Skeleton';
import {findOrder} from '../../redux/order';

const Event = props => {
  const dispatch = useDispatch();
  const cart = useSelector(state => state.cart);
  const {order, isSuccessGetOrder} = useSelector(state => state.order);
  const {orderId} = props.route.params;

  useEffect(() => {
    dispatch(findOrder(orderId));
  }, []);

  return (
    <Container>
      <ParallaxScroll
        isHeaderFixed={false}
        parallaxHeight={hp('20%')}
        parallaxBackgroundScrollSpeed={5}
        parallaxForegroundScrollSpeed={2.5}
        renderHeader={() => (
          <HeaderClose
            onPress={() => props.navigation.goBack()}
            colorIcon={theme.fourthColor}
          />
        )}
        renderParallaxBackground={() => (
          <>
            <OverlayImage />
            <ImageEvent
              source={{
                uri: cart.event.linkImage,
              }}
            />
          </>
        )}>
        <SectionInfo>
          <Header>
            <Icon source={images.entertainment} />
            <Title numberOfLines={2} margin="0" color={theme.sixthColor}>
              {texts.congratulation}
            </Title>
            <Subtitle numberOfLines={1} color={theme.sixthColor} margin="0">
              {texts.paymentMade}
            </Subtitle>
          </Header>
          <Division />
          {isSuccessGetOrder ? (
            <>
              <ContainerCard>
                <Subtitle numberOfLines={1} color={theme.sixthColor} margin="0">
                  {texts.autorization}
                </Subtitle>
                <Title numberOfLines={2} margin="0" color={theme.sixthColor}>
                  #{order.idOrder}-{order.digito}
                </Title>
              </ContainerCard>
              <Division />
              <SectionButton>
                <Button
                  iconLeft={images.play}
                  title={texts.toWatch}
                  type="normal"
                  backgroundColor={theme.twelfthColor}
                  onPress={() => {}}
                />
                <Button
                  title={texts.goToListShop}
                  type="transparent"
                  onPress={() => {}}
                />
              </SectionButton>
            </>
          ) : (
            <Skeleton />
          )}
        </SectionInfo>
      </ParallaxScroll>
    </Container>
  );
};

export default Event;
