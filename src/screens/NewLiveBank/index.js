import React, {useState} from 'react';
import {ScrollView} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  Container,
  Content,
  Title,
  HeaderBack,
  Card,
  TextInput,
  Button,
} from '../../components';
import {texts} from '../../config';
import {
  Header,
  SectionText,
  SectionBankData,
  SectionButton,
  Row,
  SectionNetwork,
} from './styles';
import {theme} from '../../resources';

export default ({navigation}) => {
  const [network, setNetwork] = useState();
  const [agencyBank, setAgencyBank] = useState('');
  const [acount, setAcount] = useState('');
  const [digitBank, setDigitBank] = useState('');
  const [typeCount, setTypeCount] = useState('');

  return (
    <Container>
      <Content>
        <Header>
          <HeaderBack isLogo={false} onPress={() => navigation.goBack()}>
            <Title
              size={wp('9%')}
              color={theme.sixthColor}
              align="left"
              margin="0px">
              {`${texts.singUp}\n${texts.NewLive}`}
            </Title>
          </HeaderBack>
        </Header>
        <ScrollView>
          <SectionText>
            <Title size={wp('4%')} color={theme.sixthColor} margin="8px 0px">
              {texts.textFistLive}
            </Title>
          </SectionText>
          <SectionNetwork>
            <Card>
              <Title
                size={wp('3.4%')}
                align="left"
                color={theme.seventhColors}
                margin="0px 0px -12px 0px">
                {texts.yourNetworks}
              </Title>
              <TextInput
                placeholder={`@${texts.instagram}`}
                value={network}
                keyboardType="default"
                onChangeText={value => setNetwork(value)}
              />
            </Card>
          </SectionNetwork>
          <SectionBankData>
            <Title
              size={wp('9%')}
              align="left"
              color={theme.primaryColor}
              margin="0px 0px 16px 0px">
              {texts.myDataBank}
            </Title>
            <Card>
              <KeyboardAwareScrollView>
                <RNPickerSelect
                  placeholder={{}}
                  style={{
                    inputIOS: {
                      fontSize: 19,
                      color: theme.primaryColor,
                      borderBottomWidth: 1,
                      paddingBottom: 10,
                      borderColor: theme.primaryColor,
                    },
                    inputAndroid: {
                      fontSize: 19,
                      color: theme.primaryColor,
                      borderBottomWidth: 1,
                      paddingBottom: 10,
                      borderColor: theme.primaryColor,
                    },
                  }}
                  value="Axé"
                  onValueChange={value => console.log(value)}
                  items={[
                    {label: 'Santander', value: 'axe'},
                    {label: 'Itaú', value: 'Sertanejo'},
                  ]}
                />
                <TextInput
                  placeholder={texts.agencyBank}
                  value={agencyBank}
                  keyboardType="default"
                  onChangeText={value => setAgencyBank(value)}
                />
                <Row>
                  <TextInput
                    placeholder={texts.acount}
                    value={acount}
                    keyboardType="default"
                    onChangeText={value => setAcount(value)}
                  />
                  <TextInput
                    marginLeft="16px"
                    placeholder={texts.digit}
                    value={digitBank}
                    keyboardType="default"
                    onChangeText={value => setDigitBank(value)}
                  />
                </Row>
                <TextInput
                  placeholder={texts.typeAcount}
                  value={typeCount}
                  keyboardType="default"
                  onChangeText={value => setTypeCount(value)}
                />
              </KeyboardAwareScrollView>
            </Card>
          </SectionBankData>
          <SectionButton>
            <Button
              onPress={() => navigation.navigate('NewLiveData')}
              backgroundActive="transparent"
              title={texts.next}
              type="borded"
              fontWeight="bold"
            />
          </SectionButton>
        </ScrollView>
      </Content>
    </Container>
  );
};
