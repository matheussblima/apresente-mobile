import styled from 'styled-components';

export const Header = styled.View``;

export const SectionText = styled.View``;

export const SectionBankData = styled.View`
  margin-top: 32px;
`;

export const SectionNetwork = styled.View`
  margin-top: 32px;
`;

export const SectionButton = styled.View`
  margin-top: 32px;
`;

export const Row = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
