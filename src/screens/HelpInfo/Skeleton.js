import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export default () => {
  return (
    <SkeletonPlaceholder>
      <View style={{width: '100%', height: 30, marginBottom: 30}} />
      <View style={{width: '100%', height: 60, marginBottom: 16}} />
      <View style={{width: '100%', height: 100, marginBottom: 16}} />
      <View style={{width: '100%', height: 100, marginBottom: 16}} />
      <View style={{width: '100%', height: 100, marginBottom: 16}} />
    </SkeletonPlaceholder>
  );
};
