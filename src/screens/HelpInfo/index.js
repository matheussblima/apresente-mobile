import React, {useEffect} from 'react';
import {ScrollView} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {useSelector, useDispatch} from 'react-redux';
import HTML from 'react-native-render-html';
import Skeleton from './Skeleton';
import {Container, HeaderBack, Content, Title} from '../../components';
import {getFaq} from '../../redux/faq';
import {theme} from '../../resources';
import {Header, SectionInfo} from './styles';

const HelpInfo = props => {
  const {faq} = props.route.params;
  const dispatch = useDispatch();
  const faqInfo = useSelector(states => states.faq);

  useEffect(() => {
    dispatch(getFaq(faq.codFaq));
  }, [getFaq]);

  return (
    <Container>
      <HeaderBack
        onPress={() => {
          props.navigation.goBack();
        }}
      />
      <ScrollView>
        <Content>
          {faqInfo.isSuccessFaqInfo ? (
            <>
              <Header>
                <Title margin="8px 0 0 0" align="left" color={theme.sixthColor}>
                  {faqInfo.faqInfo.pergunta}
                </Title>
              </Header>
              <SectionInfo>
                <HTML
                  html={`<div style="font-size: ${wp('5%')}; color=${
                    theme.sixthColor
                  };">${faqInfo.faqInfo.resposta}</div>`}
                />
              </SectionInfo>
            </>
          ) : (
            <Skeleton />
          )}
        </Content>
      </ScrollView>
    </Container>
  );
};

export default HelpInfo;
