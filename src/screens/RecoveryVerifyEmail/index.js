import React from 'react';
import {ScrollView} from 'react-native';
import {
  Container,
  Logo,
  Subtitle,
  Content,
  Button,
  HeaderClose,
  Title,
} from '../../components';
import {Header, SectionInfo, SectionButton} from './styles';
import {texts} from '../../config';

const RecoveryVerifyEmail = () => {
  return (
    <Container>
      <HeaderClose />
      <ScrollView>
        <Content>
          <Header>
            <Logo />
            <Title margin="16px 0px 0px 0px">{texts.varifyEmail}</Title>
          </Header>
          <SectionInfo>
            <Subtitle margin="0px">
              {texts.sendedEmaiFor} {'matheussblima@gmail.com '}
              {texts.withInstructionsPassword}
            </Subtitle>
          </SectionInfo>
          <SectionButton>
            <Button
              title={texts.doLogin}
              type="transparent"
              fontWeight="bold"
            />
          </SectionButton>
        </Content>
      </ScrollView>
    </Container>
  );
};

export default RecoveryVerifyEmail;
