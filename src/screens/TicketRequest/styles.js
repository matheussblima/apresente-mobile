import styled from 'styled-components/native';

export const SectionInfoEnvent = styled.View`
  padding: 16px 0px;
`;

export const SectionInfo = styled.View`
  padding: 16px 0px;
`;

export const SectionBottom = styled.View`
  padding: 16px 0px;
`;

export const Status = styled.View`
  margin-top: 20px;
  border-radius: 20px;
  background-color: ${props => props.theme.fifthColor};
`;
