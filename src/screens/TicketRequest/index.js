import React from 'react';
import {Clipboard} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import moment from 'moment';
import {
  Container,
  HeaderClose,
  Title,
  EventInfo,
  Division,
  Content,
  Button,
  Subtitle,
} from '../../components';
import {texts} from '../../config';
import {theme} from '../../resources';
import currencyFormat from '../../utility/currencyFormat';
import {SectionInfoEnvent, SectionInfo, Status} from './styles';

const TicketRequest = props => {
  const {order} = props.route.params;

  return (
    <Container>
      <HeaderClose onPress={() => props.navigation.navigate('Home')} />
      <Title color={theme.sixthColor}>{texts.request}</Title>
      <Title margin="0" size={wp('8%')} color={theme.sixthColor}>
        #{order.idOrder}-{order.digito}
      </Title>
      <Content>
        <SectionInfoEnvent>
          <Division />
          <EventInfo
            image={{uri: order.linkImage}}
            title={order.nameEvent}
            overTitle={moment(order.dateEvent).format('ddd, DD MMM YYYY HH:mm')}
            subTitle={order.local}
          />
          <Division />
        </SectionInfoEnvent>
        <SectionInfo>
          <Title margin="0" size={wp('10%')} color={theme.sixthColor}>
            {currencyFormat(parseFloat(order.paymentPrice), true)}
          </Title>
          <Subtitle margin="0 0 0" color={theme.sixthColor}>
            {order.paymentMethod}
          </Subtitle>
          {order.paymentMethod === 'BOLETO' ? (
            <>
              <Subtitle margin="16px 0 0 0">{order.gatewayBoletoCod}</Subtitle>
              <Button
                title={texts.copy}
                type="transparent"
                onPress={() => Clipboard.setString(order.gatewayBoletoCod)}
              />
            </>
          ) : (
            undefined
          )}

          {order.paymentMethod === 'CARTAO' ? (
            <Subtitle margin="0" color={theme.sixthColor}>
              {order.paymentParcel}x de{' '}
              {currencyFormat(parseFloat(order.paymentPriceParcel || 0), true)}
            </Subtitle>
          ) : (
            undefined
          )}

          <Status>
            <Subtitle color={theme.sixthColor}>{order.statusVenda}</Subtitle>
          </Status>
        </SectionInfo>
        <Division />
      </Content>
    </Container>
  );
};

export default TicketRequest;
