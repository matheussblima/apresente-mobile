import {api} from '../config';

// Actions
const ORDER_REQUEST = 'safeticket/order/ORDER_REQUEST';
const ORDER_SUCCESS = 'safeticket/order/ORDER_SUCCESS';
const ORDER_FAILURE = 'safeticket/order/ORDER_FAILURE';

const GET_ORDER_REQUEST = 'safeticket/order/GET_ORDER_REQUEST';
const GET_ORDER_SUCCESS = 'safeticket/order/GET_ORDER_SUCCESS';
const GET_ORDER_FAILURE = 'safeticket/order/GET_ORDER_FAILURE';

const ORDER_CREATE_REQUEST = 'safeticket/order/ORDER_CREATE_REQUEST';
const ORDER_CREATE_SUCCESS = 'safeticket/order/ORDER_CREATE_SUCCESS';
const ORDER_CREATE_FAILURE = 'safeticket/order/ORDER_CREATE_FAILURE';

const initialState = {
  orders: [],
  order: {},
};

// Reducer
function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case ORDER_REQUEST:
      return {...state, isFetch: true, isSuccess: false};
    case ORDER_SUCCESS:
      return {
        ...state,
        isFetch: false,
        isSuccess: true,
        orders: action.payload,
      };
    case ORDER_FAILURE:
      return {
        ...state,
        isFetch: false,
        isSuccess: false,
        message: action.message,
      };

    case GET_ORDER_REQUEST:
      return {...state, isFetchGetOrder: true, isSuccessGetOrder: false};
    case GET_ORDER_SUCCESS:
      return {
        ...state,
        isFetchGetOrder: false,
        isSuccessGetOrder: true,
        order: action.payload,
      };
    case GET_ORDER_FAILURE:
      return {
        ...state,
        isFetchGetOrder: false,
        isSuccessGetOrder: false,
        message: action.message,
      };

    case ORDER_CREATE_REQUEST:
      return {...state, isFetchOrderCreate: true, isSuccessOrderCreate: false};
    case ORDER_CREATE_SUCCESS:
      return {
        ...state,
        isFetchOrderCreate: false,
        isSuccessOrderCreate: true,
        createOrder: action.payload,
      };
    case ORDER_CREATE_FAILURE:
      return {
        ...state,
        isFetchOrderCreate: false,
        isSuccessOrderCreate: false,
        message: action.message,
      };
    default:
      return state;
  }
}

export const findOrders = () => (dispatch, getState) => {
  const auth = getState().auth.auth || {};

  dispatch({type: ORDER_REQUEST});
  return api
    .get('user/orders', {
      headers: {
        userToken: auth.data.keyapp,
      },
    })
    .then(response => {
      const {data, status} = response.data.response;
      if (status === 'OK') {
        return dispatch({type: ORDER_SUCCESS, payload: data});
      }
      return dispatch({type: ORDER_FAILURE, message: 'Erro ao buscar ordens'});
    })
    .catch(() => {
      dispatch({type: ORDER_FAILURE, message: 'Erro ao buscar ordens'});
    });
};

export const findOrder = orderID => (dispatch, getState) => {
  const auth = getState().auth.auth || {};

  dispatch({type: GET_ORDER_REQUEST});
  return api
    .get(`user/order/${orderID}`, {
      headers: {
        userToken: auth.data.keyapp,
      },
    })
    .then(response => {
      const {data, status} = response.data.response;
      if (status === 'OK') {
        return dispatch({type: GET_ORDER_SUCCESS, payload: data});
      }
      return dispatch({
        type: GET_ORDER_FAILURE,
        message: 'Erro ao buscar ordens',
      });
    })
    .catch(() => {
      dispatch({type: GET_ORDER_FAILURE, message: 'Erro ao buscar ordens'});
    });
};

export const createOrder = cart => (dispatch, getState) => {
  const auth = getState().auth.auth || {};
  dispatch({type: ORDER_CREATE_REQUEST});

  if (!auth.data) {
    return dispatch({
      type: ORDER_CREATE_FAILURE,
      message: 'O usuário não efetuou o login',
      isFetchOrderCreate: false,
      isSuccessOrderCreate: false,
    });
  }

  return api
    .post(
      'order',
      {
        event: cart.codEvent,
        forma_pgto: cart.type,
        parcela: cart.parcel,
        ticket: cart.ticket,
        discount: cart.discount || null,
      },
      {
        headers: {
          userToken: auth.data.keyapp,
        },
      },
    )
    .then(response => {
      const {data, status, erros} = response.data.response;
      if (status === 'OK') {
        return dispatch({
          type: ORDER_CREATE_SUCCESS,
          payload: data,
          isFetchOrderCreate: false,
          isSuccessOrderCreate: true,
        });
      }
      return dispatch({
        type: ORDER_CREATE_FAILURE,
        message: erros[0] || 'Erro ao criar ordem de compra',
        isFetchOrderCreate: false,
        isSuccessOrderCreate: false,
      });
    })
    .catch(() => {
      return dispatch({
        type: ORDER_CREATE_FAILURE,
        message: 'Erro ao criar ordem de compra',
        isFetchOrderCreate: false,
        isSuccessOrderCreate: false,
      });
    });
};

export default reducer;
