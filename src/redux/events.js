import AsyncStorage from '@react-native-community/async-storage';
import {persistReducer} from 'redux-persist';
import {uniq, size} from 'underscore';
import {api} from '../config';
import createUrl from '../utility/createUrl';

// Actions
const EVENTS_REQUEST = 'safeticket/events/EVENTS_REQUEST';
const EVENTS_SUCCESS = 'safeticket/events/EVENTS_SUCCESS';
const EVENTS_FAILURE = 'safeticket/events/EVENTS_FAILURE';

const EVENTS_TICKET_REQUEST = 'safeticket/events/EVENTS_TICKET_REQUEST';
const EVENTS_TICKET_SUCCESS = 'safeticket/events/EVENTS_TICKET_SUCCESS';
const EVENTS_TICKET_FAILURE = 'safeticket/events/EVENTS_TICKET_FAILURE';

const EVENT_REQUEST = 'safeticket/events/EVENT_REQUEST';
const EVENT_SUCCESS = 'safeticket/events/EVENT_SUCCESS';
const EVENT_FAILURE = 'safeticket/events/EVENT_FAILURE';

const EVENT_FILTER_REQUEST = 'safeticket/events/EVENT_FILTER_REQUEST';
const EVENT_FILTER_SUCCESS = 'safeticket/events/EVENT_FILTER_SUCCESS';
const EVENT_FILTER_FAILURE = 'safeticket/events/EVENT_FILTER_FAILURE';

const SET_LAST_SEEN_EVENT_SUCCESS =
  'safeticket/events/SET_LAST_SEEN_EVENT_SUCCESS';
const SET_LAST_SEEN_EVENT_FAILURE =
  'safeticket/events/SET_LAST_SEEN_EVENT_FAILURE';

const initialState = {
  eventsFilter: [],
  eventsLastSeen: [],
  events: [],
  ticket: [],
};

// Reducer
const reducer = function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case EVENTS_REQUEST:
      return {...state, isFetch: true, isSuccess: false};
    case EVENTS_SUCCESS:
      return {
        ...state,
        isFetch: false,
        isSuccess: true,
        events: action.payload,
        paginationInfo: action.paginationInfo,
      };
    case EVENTS_FAILURE:
      return {
        ...state,
        isFetch: false,
        isSuccess: false,
        message: action.message,
      };

    case SET_LAST_SEEN_EVENT_SUCCESS:
      return {
        ...state,
        isFetchSetLastSeenEvent: false,
        isSuccessSetLastSeenEvent: true,
        eventsLastSeen: action.payload,
      };
    case SET_LAST_SEEN_EVENT_FAILURE:
      return {
        ...state,
        isFetchSetLastSeenEvent: false,
        isSuccessSetLastSeenEvent: false,
        message: action.message,
      };

    case EVENT_FILTER_REQUEST:
      return {...state, isFetchEventFilter: true, isSuccessEventFilter: false};
    case EVENT_FILTER_SUCCESS:
      return {
        ...state,
        isFetchEventFilter: false,
        isSuccessEventFilter: true,
        eventsFilter: action.payload,
        paginationInfoEventFilter: action.paginationInfo,
      };
    case EVENT_FILTER_FAILURE:
      return {
        ...state,
        isFetchEventFilter: false,
        isSuccessEventFilter: false,
        message: action.message,
      };

    case EVENTS_TICKET_REQUEST:
      return {...state, isFetchTicket: true, isSuccessTicket: false};
    case EVENTS_TICKET_SUCCESS:
      return {
        ...state,
        isFetchTicket: false,
        isSuccessTicket: true,
        ticket: action.payload,
      };
    case EVENTS_TICKET_FAILURE:
      return {
        ...state,
        isFetchTicket: false,
        isSuccessTicket: false,
        message: action.message,
      };

    case EVENT_REQUEST:
      return {...state, isFetchEvent: true, isSuccessEvent: false};
    case EVENT_SUCCESS:
      return {
        ...state,
        isFetchEvent: false,
        isSuccessEvent: true,
        event: action.payload,
      };
    case EVENT_FAILURE:
      return {
        ...state,
        isFetchEvent: false,
        isSuccessEvent: false,
        message: action.message,
      };
    default:
      return state;
  }
};

const persistConfig = {
  key: 'events',
  storage: AsyncStorage,
  whitelist: ['eventsLastSeen'],
};

export const clearEvents = () => dispatch => {
  dispatch({
    type: EVENTS_SUCCESS,
    payload: [],
    paginationInfo: {totalResults: 0},
  });
};

export const clearEventsFilter = () => dispatch => {
  dispatch({
    type: EVENT_FILTER_SUCCESS,
    payload: [],
    paginationInfo: {totalResults: 0},
  });
};

export const findEventByNearby = ({nearby, limit, page, search} = {}) => (
  dispatch,
  getState,
) => {
  dispatch({type: EVENTS_REQUEST});
  const dataEvent = getState().events.events || [];
  const dataFilter = getState().filter.filter || {};

  const query = createUrl({
    nearby,
    max: limit || 10,
    page: page || 1,
    search,
    category:
      size(dataFilter.category) > 0 ? dataFilter.category.cod : undefined,
    codcity: size(dataFilter.city) > 0 ? dataFilter.city.codCidade : undefined,
    period: size(dataFilter.period) > 0 ? dataFilter.period.id : undefined,
  });

  return api
    .get(`event${query}`)
    .then(response => {
      const {data} = response.data.response;
      const {paginationInfo} = response.data;
      const concatEvents = dataEvent.concat(data);
      dispatch({
        type: EVENTS_SUCCESS,
        payload: uniq(concatEvents, event => event.codEvento),
        paginationInfo: {...paginationInfo},
      });
    })
    .catch(() => {
      dispatch({type: EVENTS_FAILURE, message: 'Erro ao buscar eventos'});
    });
};

export const findEventFilter = (queries = {}) => (dispatch, getState) => {
  dispatch({type: EVENT_FILTER_REQUEST});
  const dataEvent = getState().events.eventsFilter || [];
  const dataFilter = getState().filter.filter || {};

  const query = createUrl({
    nearby: size(dataFilter.city) > 0 ? dataFilter.city.codCidade : undefined,
    max: queries.limit || 10,
    page: queries.page || 1,
    search: queries.search,
    category:
      size(dataFilter.category) > 0 ? dataFilter.category.cod : undefined,
    codcity: size(dataFilter.city) > 0 ? dataFilter.city.codCidade : undefined,
    period: size(dataFilter.period) > 0 ? dataFilter.period.id : undefined,
  });

  return api
    .get(`event${query}`)
    .then(response => {
      const {data} = response.data.response;
      const {paginationInfo} = response.data;
      const concatEvents = dataEvent.concat(data);

      return dispatch({
        type: EVENT_FILTER_SUCCESS,
        payload: uniq(concatEvents, event => event.codEvento),
        paginationInfo: {...paginationInfo},
      });
    })
    .catch(() => {
      return dispatch({
        type: EVENT_FILTER_FAILURE,
        message: 'Erro ao buscar eventos',
      });
    });
};

export const findTickets = codEvent => dispatch => {
  dispatch({type: EVENTS_TICKET_REQUEST});
  return api
    .get(`event/${codEvent}/tickets`)
    .then(response => {
      const {data} = response.data.response;
      dispatch({type: EVENTS_TICKET_SUCCESS, payload: data});
    })
    .catch(() => {
      dispatch({
        type: EVENTS_TICKET_FAILURE,
        message: 'Erro ao buscar ticket',
      });
    });
};

export const findEventByCod = codEvent => dispatch => {
  dispatch({type: EVENT_REQUEST});
  return api
    .get(`event/${codEvent}`)
    .then(response => {
      const {data} = response.data.response;
      dispatch({type: EVENT_SUCCESS, payload: data[0]});
    })
    .catch(() => {
      dispatch({
        type: EVENT_FAILURE,
        message: 'Erro ao buscar ticket',
      });
    });
};

export const setLastSeenEvents = event => (dispatch, getState) => {
  const dataEvent = getState().events.eventsLastSeen || [];
  const auth = getState().auth.auth || {};

  if (Object.keys(auth.data).length === 0) {
    return dispatch({
      type: SET_LAST_SEEN_EVENT_FAILURE,
      message: 'Erro ao salvar evento - Nenhum usuário logado',
    });
  }

  try {
    const auxEvents = dataEvent.slice(0, 10);
    auxEvents.unshift({
      ...event,
      userToken: auth.data.keyapp,
    });

    return dispatch({
      type: SET_LAST_SEEN_EVENT_SUCCESS,
      payload: uniq(auxEvents, value => value.codEvento),
    });
  } catch (error) {
    return dispatch({
      type: SET_LAST_SEEN_EVENT_FAILURE,
      message: 'Erro ao salvar evento',
    });
  }
};

export const clearLastSeenEvents = () => dispatch => {
  return dispatch({
    type: SET_LAST_SEEN_EVENT_SUCCESS,
    payload: [],
  });
};

export default persistReducer(persistConfig, reducer);
