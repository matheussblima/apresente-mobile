import Geolocation from '@react-native-community/geolocation';

// Actions
const GET_LOCATION_REQUEST = 'safeticket/cart/GET_LOCATION_REQUEST';
const GET_LOCATION_SUCCESS = 'safeticket/cart/GET_LOCATION_SUCCESS';
const GET_LOCATION_FAILURE = 'safeticket/cart/GET_LOCATION_FAILURE';

const initialState = {
  location: {},
};

// Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_LOCATION_REQUEST:
      return {
        ...state,
        isFetch: true,
        isSuccess: false,
      };
    case GET_LOCATION_SUCCESS:
      return {
        ...state,
        isFetch: false,
        isSuccess: true,
        location: action.payload,
      };
    case GET_LOCATION_FAILURE:
      return {
        ...state,
        isFetch: false,
        isSuccess: false,
        message: action.message,
      };

    default:
      return state;
  }
}

export const getLocation = () => dispatch => {
  dispatch({type: GET_LOCATION_REQUEST});
  try {
    return Geolocation.getCurrentPosition(
      info => {
        return dispatch({
          type: GET_LOCATION_SUCCESS,
          payload: info.coords,
        });
      },
      () => {
        return dispatch({
          type: GET_LOCATION_FAILURE,
          message: 'Erro ao buscar location',
        });
      },
      {enableHighAccuracy: false, timeout: 15000, maximumAge: 1000},
    );
  } catch {
    return dispatch({
      type: GET_LOCATION_FAILURE,
      message: 'Erro ao buscar location',
    });
  }
};
