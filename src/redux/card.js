import AsyncStorage from '@react-native-community/async-storage';
import {persistReducer} from 'redux-persist';
import {MoipValidator} from 'moip-sdk-js';
import {texts} from '../config';

// Actions
const CARD_REQUEST = 'safeticket/card/CARD_REQUEST';
const CARD_SUCCESS = 'safeticket/card/CARD_SUCCESS';
const CARD_FAILURE = 'safeticket/card/CARD_FAILURE';
const CARD_CLEAR = 'safeticket/card/CARD_CLEAR';

const CARD_EDIT_REQUEST = 'safeticket/card/CARD_EDIT_REQUEST';
const CARD_EDIT_SUCCESS = 'safeticket/card/CARD_EDIT_SUCCESS';
const CARD_EDIT_FAILURE = 'safeticket/card/CARD_EDIT_FAILURE';

const CARD_DELETE_REQUEST = 'safeticket/card/CARD_DELETE_REQUEST';
const CARD_DELETE_SUCCESS = 'safeticket/card/CARD_DELETE_SUCCESS';
const CARD_DELETE_FAILURE = 'safeticket/card/CARD_DELETE_FAILURE';

const initialState = {
  card: [],
};

// Reducer
function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CARD_REQUEST:
      return {...state, isFetch: true, isSuccess: false};
    case CARD_SUCCESS:
      return {
        ...state,
        isFetch: false,
        isSuccess: true,
        card: action.payload,
      };
    case CARD_FAILURE:
      return {
        ...state,
        isFetch: false,
        isSuccess: false,
        message: action.message,
      };

    case CARD_EDIT_REQUEST:
      return {...state, isFetchEdit: true, isSuccessEdit: false};
    case CARD_EDIT_SUCCESS:
      return {
        ...state,
        isFetchEdit: false,
        isSuccessEdit: true,
        card: action.payload,
      };
    case CARD_EDIT_FAILURE:
      return {
        ...state,
        isFetchEdit: false,
        isSuccessEdit: false,
        message: action.message,
      };

    case CARD_DELETE_REQUEST:
      return {...state, isFetchDelete: true, isSuccessDelete: false};
    case CARD_DELETE_SUCCESS:
      return {
        ...state,
        isFetchDelete: false,
        isSuccessDelete: true,
        card: action.payload,
      };
    case CARD_DELETE_FAILURE:
      return {
        ...state,
        isFetchDelete: false,
        isSuccessDelete: false,
        message: action.message,
      };

    case CARD_CLEAR:
      return {card: action.payload};
    default:
      return state;
  }
}

const persistConfig = {
  key: 'card',
  storage: AsyncStorage,
  blacklist: ['isFetch'],
};

export const setCard = card => async (dispatch, getState) => {
  dispatch({type: CARD_REQUEST});
  const data = getState().card.card || [];
  const auth = getState().auth.auth || {};
  const messageError = texts.verifyData;

  if (
    !card.numberCard ||
    !card.nameCard ||
    !card.month ||
    !card.year ||
    !card.cvv ||
    !card.cep ||
    !card.cpf
  ) {
    return dispatch({
      type: CARD_FAILURE,
      message: messageError,
    });
  }

  if (!MoipValidator.isValidNumber(card.numberCard)) {
    return dispatch({
      type: CARD_FAILURE,
      message: 'Número do cartão invalido',
    });
  }

  if (!MoipValidator.isSecurityCodeValid(card.numberCard, card.cvv)) {
    return dispatch({
      type: CARD_FAILURE,
      message: 'O CVV do cartão está invalido',
    });
  }

  if (!MoipValidator.isExpiryDateValid(card.month, card.year)) {
    return dispatch({
      type: CARD_FAILURE,
      message: 'A validade do cartão é invalida',
    });
  }

  const cardExist = data.find(value => value.numberCard === card.numberCard);

  if (cardExist) {
    return dispatch({
      type: CARD_FAILURE,
      message: 'Número de cartão já existente',
    });
  }

  let auxData = data.slice(0);

  if (card.isDefault && data.length > 0) {
    const updateDefault = auxData.map(value => {
      return {
        ...value,
        isDefault: false,
      };
    });

    auxData = updateDefault;
  }

  auxData.push({
    ...card,
    nameCard: card.nameCard.toUpperCase(),
    ...MoipValidator.cardType(card.numberCard),
    userToken: auth.data.keyapp,
    isDefault: data.length === 0 ? true : card.isDefault,
  });
  return dispatch({type: CARD_SUCCESS, payload: auxData});
};

export const editCard = card => async (dispatch, getState) => {
  const data = getState().card.card || [];
  const auth = getState().auth.auth || {};
  const messageError = texts.verifyData;

  dispatch({type: CARD_EDIT_REQUEST});

  if (
    !card.numberCard ||
    !card.nameCard ||
    !card.month ||
    !card.year ||
    !card.cvv ||
    !card.cep ||
    !card.cpf
  ) {
    return dispatch({
      type: CARD_FAILURE,
      message: messageError,
    });
  }

  if (!MoipValidator.isValidNumber(card.numberCard)) {
    return dispatch({
      type: CARD_EDIT_FAILURE,
      message: 'Número do cartão invalido',
    });
  }

  if (!MoipValidator.isSecurityCodeValid(card.numberCard, card.cvv)) {
    return dispatch({
      type: CARD_EDIT_FAILURE,
      message: 'O CVV do cartão está invalido',
    });
  }

  if (!MoipValidator.isExpiryDateValid(card.month, card.year)) {
    return dispatch({
      type: CARD_EDIT_FAILURE,
      message: 'A validade do cartão é invalida',
    });
  }

  const auxData = data.slice(0);
  const cardFindIndex = auxData.findIndex(
    value => value.numberCard === card.numberCard,
  );

  if (cardFindIndex !== -1) {
    if (card.isDefault) {
      const updateDefault = auxData.map(value => {
        return {
          ...value,
          isDefault: false,
        };
      });

      updateDefault[cardFindIndex] = {
        ...card,
        nameCard: card.nameCard.toUpperCase(),
        ...MoipValidator.cardType(card.numberCard),
        userToken: auth.data.keyapp,
        isDefault: card.isDefault,
      };

      return dispatch({type: CARD_EDIT_SUCCESS, payload: updateDefault});
    }

    auxData[cardFindIndex] = {
      ...card,
      nameCard: card.nameCard.toUpperCase(),
      ...MoipValidator.cardType(card.numberCard),
      userToken: auth.data.keyapp,
      isDefault: card.isDefault,
    };

    return dispatch({type: CARD_EDIT_SUCCESS, payload: auxData});
  }

  return dispatch({
    type: CARD_EDIT_FAILURE,
    message: 'Cartão não encontrado',
  });
};

export const deleteCard = numberCard => async (dispatch, getState) => {
  const data = getState().card.card || [];
  dispatch({type: CARD_DELETE_REQUEST});

  if (!numberCard) {
    return dispatch({
      type: CARD_DELETE_FAILURE,
      message: 'Nenhum cartão encontrado',
    });
  }

  const auxData = data.slice(0);
  const cardFindIndex = auxData.findIndex(
    value => value.numberCard === numberCard,
  );

  if (cardFindIndex !== -1) {
    auxData.splice(cardFindIndex, 1);

    if (auxData.length > 0) {
      auxData[0] = {...auxData[0], isDefault: true};
    }
    return dispatch({
      type: CARD_SUCCESS,
      payload: auxData,
    });
  }

  return dispatch({
    type: CARD_DELETE_FAILURE,
    message: 'Nenhum cartão encontrado',
  });
};

export const clearCard = () => dispatch => {
  dispatch({
    type: CARD_CLEAR,
    payload: undefined,
  });
};

export default persistReducer(persistConfig, reducer);
