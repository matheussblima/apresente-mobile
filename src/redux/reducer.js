import {combineReducers} from 'redux';
import events from './events';
import citys from './citys';
import auth from './auth';
import card from './card';
import cart from './cart';
import payment from './payment';
import category from './category';
import filter from './filter';
import order from './order';
import faq from './faq';
import terms from './terms';
import geolocation from './geolocation';
import like from './like';
import push from './push';

export default combineReducers({
  events,
  citys,
  card,
  auth,
  payment,
  cart,
  category,
  order,
  filter,
  faq,
  geolocation,
  terms,
  like,
  push,
});
