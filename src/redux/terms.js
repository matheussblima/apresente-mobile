import {api} from '../config';

// Actions
const TERMS_REQUEST = 'safeticket/citys/TERMS_REQUEST';
const TERMS_SUCCESS = 'safeticket/citys/TERMS_SUCCESS';
const TERMS_FAILURE = 'safeticket/citys/TERMS_FAILURE';

// Reducer
export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case TERMS_REQUEST:
      return {...state, isFetch: true, isSuccess: false};
    case TERMS_SUCCESS:
      return {
        ...state,
        isFetch: false,
        isSuccess: true,
        terms: action.payload,
      };
    case TERMS_FAILURE:
      return {
        ...state,
        isFetch: false,
        isSuccess: false,
        message: action.message,
      };
    default:
      return state;
  }
}

export const getTerms = () => dispatch => {
  dispatch({type: TERMS_REQUEST});
  return api
    .get('appdoc/1')
    .then(response => {
      const {data, status} = response.data.response;
      if (status === 'OK') {
        return dispatch({
          type: TERMS_SUCCESS,
          payload: data,
        });
      }
      return dispatch({type: TERMS_FAILURE, message: 'Erro ao buscar termos'});
    })
    .catch(() => {
      dispatch({type: TERMS_FAILURE, message: 'Erro ao buscar termos'});
    });
};
