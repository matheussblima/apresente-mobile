// Actions
const FILTER_CREATE_SUCCESS = 'safeticket/filter/FILTER_CREATE_SUCCESS';
const FILTER_CREATE_FAILURE = 'safeticket/filter/FILTER_CREATE_FAILURE';

// Reducer
export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case FILTER_CREATE_SUCCESS:
      return {
        ...state,
        isSuccessCreate: true,
        filter: action.filter,
      };
    case FILTER_CREATE_FAILURE:
      return {
        ...state,
        isSuccess: false,
        message: action.message,
      };
    default:
      return state;
  }
}

export const setFilter = ({city, category, period}) => dispatch => {
  return dispatch({
    type: FILTER_CREATE_SUCCESS,
    filter: {city: {...city}, category: {...category}, period: {...period}},
  });
};
