import AsyncStorage from '@react-native-community/async-storage';
import {persistReducer} from 'redux-persist';
import {clearLastSeenEvents} from './events';
import {api, texts} from '../config';

// Actions
const AUTH_REQUEST = 'safeticket/AUTH/AUTH_REQUEST';
const AUTH_SUCCESS = 'safeticket/AUTH/AUTH_SUCCESS';
const AUTH_FAILURE = 'safeticket/AUTH/AUTH_FAILURE';

const GET_PROFILE_REQUEST = 'safeticket/AUTH/GET_PROFILE_REQUEST';
const GET_PROFILE_SUCCESS = 'safeticket/AUTH/GET_PROFILE_SUCCESS';
const GET_PROFILE_FAILURE = 'safeticket/AUTH/GET_PROFILE_FAILURE';

const EDIT_PROFILE_REQUEST = 'safeticket/AUTH/EDIT_PROFILE_REQUEST';
const EDIT_PROFILE_SUCCESS = 'safeticket/AUTH/EDIT_PROFILE_SUCCESS';
const EDIT_PROFILE_FAILURE = 'safeticket/AUTH/EDIT_PROFILE_FAILURE';

const RECOVERY_REQUEST = 'safeticket/AUTH/RECOVERY_REQUEST';
const RECOVERY_SUCCESS = 'safeticket/AUTH/RECOVERY_SUCCESS';
const RECOVERY_FAILURE = 'safeticket/AUTH/RECOVERY_FAILURE';

const LOGOFF_SUCCESS = 'safeticket/AUTH/LOGOFF_SUCCESS';

const CREATE_USER_REQUEST = 'safeticket/AUTH/CREATE_USER_REQUEST';
const CREATE_USER_SUCCESS = 'safeticket/AUTH/CREATE_USER_SUCCESS';
const CREATE_USER_FAILURE = 'safeticket/AUTH/CREATE_USER_FAILURE';

const initialState = {
  profile: {
    emptyFields: [],
  },
  auth: {data: {}},
};

// Reducer
function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case AUTH_REQUEST:
      return {...state, isFetchAuth: true, isSuccessAuth: false};
    case AUTH_SUCCESS:
      return {
        ...state,
        isFetchAuth: false,
        isSuccessAuth: true,
        auth: action.payload,
      };
    case AUTH_FAILURE:
      return {
        ...state,
        isFetchAuth: false,
        isSuccessAuth: false,
        message: action.message,
      };

    case RECOVERY_REQUEST:
      return {...state, isFetchRecovery: true, isSuccessRecovery: false};
    case RECOVERY_SUCCESS:
      return {
        ...state,
        isFetchRecovery: false,
        isSuccessRecovery: true,
        recovery: action.payload,
      };
    case RECOVERY_FAILURE:
      return {
        ...state,
        isFetchRecovery: false,
        isSuccessRecovery: false,
        message: action.message,
      };

    case CREATE_USER_REQUEST:
      return {...state, isFetchCreateUser: true, isSuccessCreateUser: false};
    case CREATE_USER_SUCCESS:
      return {
        ...state,
        isFetchCreateUser: false,
        isSuccessCreateUser: true,
        createUser: action.payload,
      };
    case CREATE_USER_FAILURE:
      return {
        ...state,
        isFetchCreateUser: false,
        isSuccessCreateUser: false,
        message: action.message,
      };

    case GET_PROFILE_REQUEST:
      return {...state, isFetchGetProfile: true, isSuccessGetProfile: false};
    case GET_PROFILE_SUCCESS:
      return {
        ...state,
        isFetchGetProfile: false,
        isSuccessGetProfile: true,
        profile: action.payload,
      };
    case GET_PROFILE_FAILURE:
      return {
        ...state,
        isFetchGetProfile: false,
        isSuccessGetProfile: false,
        message: action.message,
      };

    case EDIT_PROFILE_REQUEST:
      return {...state, isFetchEditProfile: true, isSuccessEditProfile: false};
    case EDIT_PROFILE_SUCCESS:
      return {
        ...state,
        isFetchEditProfile: false,
        isSuccessEditProfile: true,
        editProfile: action.payload,
      };
    case EDIT_PROFILE_FAILURE:
      return {
        ...state,
        isFetchEditProfile: false,
        isSuccessEditProfile: false,
        message: action.message,
      };

    case LOGOFF_SUCCESS:
      return {
        ...state,
        isFetchAuth: false,
        isSuccessAuth: false,
        isSuccessLogoff: true,
        auth: action.payload,
      };
    default:
      return state;
  }
}

const persistConfig = {
  key: 'auth',
  storage: AsyncStorage,
  blacklist: [
    'isFetchCreateUser',
    'isSuccessCreateUser',
    'isSuccessGetProfile',
    'isFetchGetProfile',
    'isSuccessEditProfile',
    'isFetchEditProfile',
    'createUser',
    'message',
  ],
};

export const login = (email, password) => (dispatch, getState) => {
  const push = getState().push.push || {};

  dispatch({type: AUTH_REQUEST});
  const messageError = texts.verifyData;

  if (!email || !password) {
    return new Promise(resolve => {
      resolve(
        dispatch({
          type: AUTH_FAILURE,
          message: messageError,
        }),
      );
    });
  }

  return api
    .post('user/login', {
      email,
      password,
      push: {
        push_id: push.push_id,
        system: push.system,
        system_version: push.system_version,
        brand: push.brand,
      },
    })
    .then(response => {
      const data = response.data.response;

      if (data.status === 'OK') {
        return dispatch({type: AUTH_SUCCESS, payload: data});
      }
      return dispatch({
        type: AUTH_FAILURE,
        message: messageError,
      });
    })
    .catch(() => {
      dispatch({
        type: AUTH_FAILURE,
        message: messageError,
      });
    });
};

export const createUser = user => async (dispatch, getState) => {
  const push = getState().push.push || {};

  const messageError = texts.verifyData;
  dispatch({type: CREATE_USER_REQUEST});

  if (
    !user.email ||
    !user.password ||
    !user.cell ||
    !user.name ||
    !user.cpf ||
    !user.cep ||
    !user.houseNumber ||
    !user.dateBorn
  ) {
    return new Promise(resolve => {
      resolve(
        dispatch({
          type: AUTH_FAILURE,
          message: messageError,
        }),
      );
    });
  }

  api.get(`local/cep/${user.cep}`).then(responseCep => {
    const {status} = responseCep.data.response;
    if (status === 'OK') {
      api
        .post('user', {
          customer: {
            email: user.email,
            fullname: user.name,
            password: user.password,
            phone: user.cell,
            birthDate: user.dateBorn,
            taxDocument: {
              type: 'CPF',
              number: user.cpf,
            },
            address: {
              zipCode: user.cep,
              number: user.houseNumber,
            },
            push: {
              push_id: push.push_id,
              system: push.system,
              system_version: push.system_version,
              brand: push.brand,
            },
          },
        })
        .then(response => {
          const data = response.data.response;
          if (data.status === 'OK') {
            return dispatch({type: CREATE_USER_SUCCESS, payload: data});
          }
          return dispatch({
            type: CREATE_USER_FAILURE,
            message: data.erros[0] || messageError,
          });
        })
        .catch(() => {
          return dispatch({
            type: CREATE_USER_FAILURE,
            message: messageError,
          });
        });
    }

    return dispatch({
      type: CREATE_USER_FAILURE,
      message: 'Verifique o CEP digitado',
    });
  });

  return dispatch({
    type: CREATE_USER_FAILURE,
    message: 'Verifique ao criar conta',
  });
};

export const logoff = () => dispatch => {
  dispatch(clearLastSeenEvents());
  return dispatch({type: LOGOFF_SUCCESS, payload: initialState.auth});
};

export const recoveryPassword = email => dispatch => {
  dispatch({type: RECOVERY_REQUEST});
  const messageError = texts.verifyData;

  if (!email) {
    return new Promise(resolve => {
      resolve(
        dispatch({
          type: RECOVERY_FAILURE,
          message: messageError,
        }),
      );
    });
  }

  return api
    .get(`user/recovery/${email.replace('@', '%40')}`)
    .then(response => {
      const data = response.data.response;
      if (data.status === 'OK') {
        return dispatch({type: RECOVERY_SUCCESS, payload: data});
      }
      return dispatch({
        type: RECOVERY_FAILURE,
        message: messageError,
      });
    })
    .catch(() => {
      dispatch({
        type: RECOVERY_FAILURE,
        message: messageError,
      });
    });
};

export const getProfile = () => (dispatch, getState) => {
  const auth = getState().auth.auth || {};
  dispatch({type: GET_PROFILE_REQUEST});
  const messageError = texts.verifyData;

  return api
    .get('user/profile', {
      headers: {
        userToken: auth.data.keyapp,
      },
    })
    .then(response => {
      const data = response.data.response;
      if (data.status === 'OK') {
        return dispatch({type: GET_PROFILE_SUCCESS, payload: data.data});
      }
      return dispatch({
        type: GET_PROFILE_FAILURE,
        message: messageError,
      });
    })
    .catch(() => {
      dispatch({
        type: GET_PROFILE_FAILURE,
        message: messageError,
      });
    });
};

export const editProfile = user => async (dispatch, getState) => {
  const auth = getState().auth.auth || {};
  dispatch({type: EDIT_PROFILE_REQUEST});
  const messageError = texts.verifyData;

  if (!user.cell || !user.cep || !user.houseNumber) {
    return new Promise(resolve => {
      resolve(
        dispatch({
          type: AUTH_FAILURE,
          message: messageError,
        }),
      );
    });
  }

  const responseCep = await api.get(`local/cep/${user.cep}`);
  const {status} = responseCep.data.response;

  if (status === 'OK') {
    return api
      .post(
        'user/profile',
        {
          phone: user.cell,
          cep: user.cep,
          numero: user.houseNumber,
        },
        {
          headers: {
            userToken: auth.data.keyapp,
          },
        },
      )
      .then(response => {
        const data = response.data.response;
        if (data.status === 'OK') {
          return dispatch({type: EDIT_PROFILE_SUCCESS, payload: data.data});
        }
        return dispatch({
          type: EDIT_PROFILE_FAILURE,
          message: messageError,
        });
      })
      .catch(() => {
        return dispatch({
          type: EDIT_PROFILE_FAILURE,
          message: messageError,
        });
      });
  }
  return dispatch({
    type: EDIT_PROFILE_FAILURE,
    message: 'Verifique o CEP digitado',
  });
};

export default persistReducer(persistConfig, reducer);
