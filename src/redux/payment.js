import {MoipCreditCard} from 'moip-sdk-js';
import {RSA} from 'react-native-rsa-native';
import {api} from '../config';

// Actions
const PAYMENTS_INFO_REQUEST = 'safeticket/payments/PAYMENTS_INFO_REQUEST';
const PAYMENTS_INFO_SUCCESS = 'safeticket/payments/PAYMENTS_INFO_SUCCESS';
const PAYMENTS_INFO_FAILURE = 'safeticket/payments/PAYMENTS_INFO_FAILURE';

const PAYMENTS_CREATE_REQUEST = 'safeticket/payments/PAYMENTS_CREATE_REQUEST';
const PAYMENTS_CREATE_SUCCESS = 'safeticket/payments/PAYMENTS_CREATE_SUCCESS';
const PAYMENTS_CREATE_FAILURE = 'safeticket/payments/PAYMENTS_CREATE_FAILURE';

const PAYMENTS_VALIDATE_REQUEST =
  'safeticket/payments/PAYMENTS_VALIDATE_REQUEST';
const PAYMENTS_VALIDATE_SUCCESS =
  'safeticket/payments/PAYMENTS_VALIDATE_SUCCESS';
const PAYMENTS_VALIDATE_FAILURE =
  'safeticket/payments/PAYMENTS_VALIDATE_FAILURE';

const initialState = {
  paymentsInfo: {},
};

// Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case PAYMENTS_INFO_REQUEST:
      return {...state, isFetch: true, isSuccess: false};
    case PAYMENTS_INFO_SUCCESS:
      return {
        ...state,
        isFetch: false,
        isSuccess: true,
        paymentsInfo: action.payload,
      };
    case PAYMENTS_INFO_FAILURE:
      return {
        ...state,
        isFetch: false,
        isSuccess: false,
        message: action.message,
      };

    case PAYMENTS_VALIDATE_REQUEST:
      return {...state, isFetchValidate: true, isSuccessValidate: false};
    case PAYMENTS_VALIDATE_SUCCESS:
      return {
        ...state,
        isFetchValidate: false,
        isSuccessValidate: true,
        validateCart: action.payload,
      };
    case PAYMENTS_VALIDATE_FAILURE:
      return {
        ...state,
        isFetchValidate: false,
        isSuccessValidate: false,
        message: action.message,
      };

    case PAYMENTS_CREATE_REQUEST:
      return {...state, isFetchCreate: true, isSuccessCreate: false};
    case PAYMENTS_CREATE_SUCCESS:
      return {
        ...state,
        isFetchCreate: false,
        isSuccessCreate: true,
        paymentCreate: action.payload,
      };
    case PAYMENTS_CREATE_FAILURE:
      return {
        ...state,
        isFetchCreate: false,
        isSuccessCreate: false,
        message: action.message,
      };
    default:
      return state;
  }
}

export const getPaymentsInfo = codEvent => dispatch => {
  dispatch({type: PAYMENTS_INFO_REQUEST});
  return api
    .get(`event/${codEvent}/payments`)
    .then(response => {
      const {data} = response.data.response;
      dispatch({type: PAYMENTS_INFO_SUCCESS, payload: data});
    })
    .catch(() => {
      dispatch({
        type: PAYMENTS_INFO_FAILURE,
        message: 'Erro ao buscar informações de pagamento',
      });
    });
};

export const validateCart = cart => dispatch => {
  dispatch({type: PAYMENTS_VALIDATE_REQUEST});

  return api
    .post('order/validate/cart', {
      event: cart.codEvent,
      forma_pgto: cart.type,
      parcela: cart.parcel,
      ticket: cart.ticket,
      discount: cart.discount || null,
    })
    .then(response => {
      const {data, status, erros} = response.data.response;
      if (status === 'OK') {
        return dispatch({
          type: PAYMENTS_VALIDATE_SUCCESS,
          payload: data,
          isFetchValidate: false,
          isSuccessValidate: true,
        });
      }
      return dispatch({
        type: PAYMENTS_VALIDATE_FAILURE,
        message: erros[0] || 'Erro ao validar compra',
        isFetchValidate: false,
        isSuccessValidate: false,
      });
    })
    .catch(() => {
      return dispatch({
        type: PAYMENTS_VALIDATE_FAILURE,
        message: 'Erro ao validar compra',
        isFetchValidate: false,
        isSuccessValidate: false,
      });
    });
};

export const createPayment = ({keyOrder, card, gateway, typePayment}) => async (
  dispatch,
  getState,
) => {
  dispatch({type: PAYMENTS_CREATE_REQUEST});
  const auth = getState().auth.auth || {};
  let payment = {
    cardHash: null,
    cardName: null,
    cardValidMonth: null,
    cardValidYear: null,
    payment_method_id: null,
    cardNumber: null,
    cardCvv: null,
  };

  if (typePayment === 'cartao') {
    if (!gateway) {
      return dispatch({
        type: PAYMENTS_CREATE_FAILURE,
        message: 'Não foi possivel realizar a compra, tente novamente',
        isFetchCreate: false,
        isSuccessCreate: false,
      });
    }

    const pubKey = gateway[0].pkey;
    const gatewayName = gateway[0].name;

    if (!card) {
      return dispatch({
        type: PAYMENTS_CREATE_FAILURE,
        message: 'Você não possui um cartão cadastrado',
        isFetchCreate: false,
        isSuccessCreate: false,
      });
    }

    if (
      !card.numberCard ||
      !card.nameCard ||
      !card.month ||
      !card.year ||
      !card.cvv
    ) {
      return dispatch({
        type: PAYMENTS_CREATE_FAILURE,
        message: 'Você não possui um cartão cadastrado',
        isFetchCreate: false,
        isSuccessCreate: false,
      });
    }

    switch (gatewayName) {
      case 'moip':
        payment = await paymentMoip({card, pubKey});
        break;
      case 'ingresse':
        payment = await paymentIngresse({card});
        break;
      default:
        payment = await paymentMoip({card, pubKey});
        break;
    }
  }

  if (!auth.data) {
    return dispatch({
      type: PAYMENTS_CREATE_FAILURE,
      message: 'O usuário não efetuou o login',
      isFetchCreate: false,
      isSuccessCreate: false,
    });
  }

  return api
    .post(
      `sale/${keyOrder}/payment`,
      {
        payment: {
          ...payment,
        },
      },
      {
        headers: {
          userToken: auth.data.keyapp,
        },
      },
    )
    .then(response => {
      const {data, status, erros} = response.data.response;
      if (status === 'OK') {
        return dispatch({
          type: PAYMENTS_CREATE_SUCCESS,
          payload: data,
          isFetchCreate: false,
          isSuccessCreate: true,
        });
      }
      return dispatch({
        type: PAYMENTS_CREATE_FAILURE,
        message: erros[0] || 'Erro ao criar compra',
        isFetchCreate: false,
        isSuccessCreate: false,
      });
    })
    .catch(() => {
      return dispatch({
        type: PAYMENTS_CREATE_FAILURE,
        message: 'Erro ao criar compra',
        isFetchCreate: false,
        isSuccessCreate: false,
      });
    });
};

const paymentMoip = async ({card, pubKey}) => {
  const hash = await MoipCreditCard.setEncrypter(RSA, 'react-native')
    .setPubKey(pubKey)
    .setCreditCard({
      number: card.numberCard,
      cvc: card.cvv,
      expirationMonth: card.month,
      expirationYear: card.year,
    })
    .hash();

  const payment = {
    cardHash: hash.toString().replace(/(\r\n|\n|\r)/gm, ''),
    cardName: card.nameCard,
    cardValidMonth: card.month,
    cardValidYear: card.year,
    payment_method_id: '',
    cardNumber: card.numberCard,
    cardCvv: card.cvv,
  };

  return payment;
};

const paymentIngresse = async ({card}) => {
  const payment = {
    cardHash: null,
    cardName: card.nameCard,
    cardValidMonth: card.month,
    cardValidYear: card.year,
    payment_method_id: '',
    cardNumber: card.numberCard,
    cardCvv: card.cvv,
  };

  return payment;
};
