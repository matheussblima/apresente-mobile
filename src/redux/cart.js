// Actions
const CART_CREATE_SUCCESS = 'safeticket/cart/CART_CREATE_SUCCESS';
const CART_CREATE_FAILURE = 'safeticket/cart/CART_CREATE_FAILURE';

const CART_DELETE_SUCCESS = 'safeticket/cart/CART_DELETE_SUCCESS';
const CART_DELETE_FAILURE = 'safeticket/cart/CART_DELETE_FAILURE';

// Reducer
export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case CART_CREATE_SUCCESS:
      return {
        ...state,
        isSuccessCreate: true,
        items: action.items,
        total: action.total,
        event: action.event,
      };
    case CART_CREATE_FAILURE:
      return {
        ...state,
        isSuccess: false,
        message: action.message,
      };

    case CART_DELETE_SUCCESS:
      return {
        ...state,
        isSuccessDelete: true,
        items: action.items,
        total: action.total,
        event: action.event,
      };
    case CART_DELETE_FAILURE:
      return {
        ...state,
        isSuccess: false,
        message: action.message,
      };

    default:
      return state;
  }
}

export const setCart = ({event, ticket}) => dispatch => {
  try {
    const totalTicket = parseFloat(ticket.valor) + parseFloat(ticket.vlTaxa);
    const discounts = 0;

    return dispatch({
      type: CART_CREATE_SUCCESS,
      items: [
        {
          ...ticket,
          id: ticket.codLote,
          name: ticket.nome,
          type: ticket.tipo,
          qtd: 1,
        },
      ],
      event,
      total: {
        totalTicket,
        discounts,
        total: totalTicket - discounts,
      },
    });
  } catch {
    return dispatch({
      type: CART_CREATE_FAILURE,
      message: 'Erro ao criar carrinho',
    });
  }
};

export const deleteItemCart = item => (dispatch, getState) => {
  try {
    const cart = getState().cart || [];
    const auxItems = cart.items.slice(0);
    const newItems = auxItems.filter(value => value.codLote !== item.codLote);

    let totalTicket = 0;
    const discounts = 0;

    newItems.forEach(value => {
      totalTicket +=
        value.qtd * (parseFloat(value.valor) + parseFloat(value.vlTaxa));
    });

    return dispatch({
      type: CART_DELETE_SUCCESS,
      ...cart,
      items: newItems,
      total: {totalTicket, discounts, total: totalTicket - discounts},
    });
  } catch {
    return dispatch({
      type: CART_DELETE_FAILURE,
      message: 'Erro ao deletar item',
    });
  }
};
