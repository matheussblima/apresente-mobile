// Actions
const LIKE_CREATE_SUCCESS = 'safeticket/like/LIKE_CREATE_SUCCESS';
const LIKE_CREATE_FAILURE = 'safeticket/like/LIKE_CREATE_FAILURE';

const initialState = {
  eventLike: [],
};

// Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LIKE_CREATE_SUCCESS:
      return {
        ...state,
        isSuccessCreate: true,
        eventLike: action.payload,
      };
    case LIKE_CREATE_FAILURE:
      return {
        ...state,
        isSuccess: false,
        message: action.message,
      };

    default:
      return state;
  }
}

export const setLikes = codeEvent => (dispatch, getState) => {
  const like = getState().like || [];
  const auxLike = like.eventLike.slice(0);

  const indexItem = auxLike.findIndex(
    likeValue => likeValue.codeEvent === codeEvent,
  );

  if (indexItem === -1) {
    auxLike.push({codeEvent});
  } else {
    auxLike.splice(indexItem, 1);
  }

  try {
    return dispatch({
      type: LIKE_CREATE_SUCCESS,
      payload: auxLike,
    });
  } catch {
    return dispatch({
      type: LIKE_CREATE_FAILURE,
      message: 'Erro like',
    });
  }
};
