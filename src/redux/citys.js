import {api} from '../config';

// Actions
const CITYS_REQUEST = 'safeticket/citys/CITYS_REQUEST';
const CITYS_SUCCESS = 'safeticket/citys/CITYS_SUCCESS';
const CITYS_FAILURE = 'safeticket/citys/CITYS_FAILURE';

// Reducer
export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case CITYS_REQUEST:
      return {...state, isFetch: true, isSuccess: false};
    case CITYS_SUCCESS:
      return {
        ...state,
        isFetch: false,
        isSuccess: true,
        citys: action.payload,
      };
    case CITYS_FAILURE:
      return {
        ...state,
        isFetch: false,
        isSuccess: false,
        message: action.message,
      };
    default:
      return state;
  }
}

export const findCitys = coords => dispatch => {
  dispatch({type: CITYS_REQUEST});
  return api
    .post('city', {
      longnitude: coords ? coords.longitude : '',
      latitude: coords ? coords.latitude : '',
    })
    .then(response => {
      const {data} = response.data.response;
      return dispatch({
        type: CITYS_SUCCESS,
        payload: data,
        isFetch: false,
        isSuccess: true,
      });
    })
    .catch(() => {
      return dispatch({
        type: CITYS_FAILURE,
        message: 'Erro ao buscar cidades',
        isFetch: false,
        isSuccess: false,
      });
    });
};
