import AsyncStorage from '@react-native-community/async-storage';
import {persistReducer} from 'redux-persist';
import {api} from '../config';

// Actions
const CATEGORY_REQUEST = 'safeticket/card/CATEGORY_REQUEST';
const CATEGORY_SUCCESS = 'safeticket/card/CATEGORY_SUCCESS';
const CATEGORY_FAILURE = 'safeticket/card/CATEGORY_FAILURE';

// Reducer
function reducer(state = {}, action = {}) {
  switch (action.type) {
    case CATEGORY_REQUEST:
      return {...state, isFetch: true, isSuccess: false};
    case CATEGORY_SUCCESS:
      return {
        ...state,
        isFetch: false,
        isSuccess: true,
        categories: action.payload,
      };
    case CATEGORY_FAILURE:
      return {
        ...state,
        isFetch: false,
        isSuccess: false,
        message: action.message,
      };
    default:
      return state;
  }
}

const persistConfig = {
  key: 'category',
  storage: AsyncStorage,
  blacklist: ['isFetch'],
};

export const findCategorys = () => dispatch => {
  dispatch({type: CATEGORY_REQUEST});
  return api
    .get('category')
    .then(response => {
      const {data} = response.data.response;
      dispatch({type: CATEGORY_SUCCESS, payload: data});
    })
    .catch(() => {
      dispatch({type: CATEGORY_FAILURE, message: 'Erro ao buscar categorias'});
    });
};

export default persistReducer(persistConfig, reducer);
