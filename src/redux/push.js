import {api} from '../config';

// Actions
const PUSH_REQUEST = 'safeticket/push/PUSH_REQUEST';
const PUSH_SUCCESS = 'safeticket/push/PUSH_SUCCESS';
const PUSH_FAILURE = 'safeticket/push/PUSH_FAILURE';

const initialState = {
  push: {},
};

// Reducer
function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case PUSH_REQUEST:
      return {...state, isFetch: true, isSuccess: false};
    case PUSH_SUCCESS:
      return {
        ...state,
        isFetch: false,
        isSuccess: true,
        push: action.payload,
      };
    case PUSH_FAILURE:
      return {
        ...state,
        isFetch: false,
        isSuccess: false,
        message: action.message,
      };

    default:
      return state;
  }
}

export const setPush = ({
  hashPush,
  system,
  systemVersion,
  brand,
}) => dispatch => {
  dispatch({type: PUSH_REQUEST});
  return api
    .post('push', {
      push_id: hashPush,
      system,
      system_version: systemVersion,
      brand,
    })
    .then(response => {
      const {data, status} = response.data.response;
      if (status === 'OK') {
        return dispatch({type: PUSH_SUCCESS, payload: data});
      }
      return dispatch({type: PUSH_FAILURE, message: 'Erro push'});
    })
    .catch(() => {
      dispatch({type: PUSH_FAILURE, message: 'Erro push'});
    });
};

export default reducer;
