import {uniq} from 'underscore';
import {api} from '../config';

// Actions
const FAQ_REQUEST = 'safeticket/citys/FAQ_REQUEST';
const FAQ_SUCCESS = 'safeticket/citys/FAQ_SUCCESS';
const FAQ_FAILURE = 'safeticket/citys/FAQ_FAILURE';

const FAQ_INFO_REQUEST = 'safeticket/citys/FAQ_INFO_REQUEST';
const FAQ_INFO_SUCCESS = 'safeticket/citys/FAQ_INFO_SUCCESS';
const FAQ_INFO_FAILURE = 'safeticket/citys/FAQ_INFO_FAILURE';

// Reducer
export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case FAQ_REQUEST:
      return {...state, isFetch: true, isSuccess: false};
    case FAQ_SUCCESS:
      return {
        ...state,
        isFetch: false,
        isSuccess: true,
        faqs: action.payload,
        paginationInfo: action.paginationInfo,
      };
    case FAQ_FAILURE:
      return {
        ...state,
        isFetch: false,
        isSuccess: false,
        message: action.message,
      };
    case FAQ_INFO_REQUEST:
      return {...state, isFetchFaqInfo: true, isSuccessFaqInfo: false};
    case FAQ_INFO_SUCCESS:
      return {
        ...state,
        isFetchFaqInfo: false,
        isSuccessFaqInfo: true,
        faqInfo: action.payload,
      };
    case FAQ_INFO_FAILURE:
      return {
        ...state,
        isFetchFaqInfo: false,
        isSuccessFaqInfo: false,
        message: action.message,
      };
    default:
      return state;
  }
}

export const getFaqs = ({limit, page}) => (dispatch, getState) => {
  const dataFaq = getState().faq.faqs || [];
  dispatch({type: FAQ_REQUEST});
  return api
    .get(`faq?max=${limit || 10}&page=${page || 1}`)
    .then(response => {
      const {data, status} = response.data.response;
      const {paginationInfo} = response.data;
      if (status === 'OK') {
        const concatFaq = dataFaq.concat(data);

        return dispatch({
          type: FAQ_SUCCESS,
          payload: uniq(concatFaq, faq => faq.codFaq),
          paginationInfo: {...paginationInfo},
        });
      }
      return dispatch({type: FAQ_FAILURE, message: 'Erro ao buscar faq'});
    })
    .catch(() => {
      dispatch({type: FAQ_FAILURE, message: 'Erro ao buscar faq'});
    });
};

export const getFaq = faqId => dispatch => {
  dispatch({type: FAQ_INFO_REQUEST});
  return api
    .get(`faq/${faqId}`)
    .then(response => {
      const {data, status} = response.data.response;
      if (status === 'OK') {
        return dispatch({
          type: FAQ_INFO_SUCCESS,
          payload: data[0],
        });
      }
      return dispatch({type: FAQ_INFO_FAILURE, message: 'Erro ao buscar faq'});
    })
    .catch(() => {
      dispatch({type: FAQ_INFO_FAILURE, message: 'Erro ao buscar faq'});
    });
};
