const currencyFormat = (num, identification) => {
  return `${identification ? 'R$ ' : ''}${num
    .toFixed(2)
    .replace('.', ',')
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
};

export default currencyFormat;
