const createUrl = params => {
  let url = '';
  let i = 0;
  Object.keys(params).forEach(keys => {
    if (params[keys]) {
      if (i === 0) {
        i = 1;
        url += '?';
      } else {
        url += '&';
      }
      url += `${keys}=${params[keys]}`;
    }
  });

  return url;
};

export default createUrl;
