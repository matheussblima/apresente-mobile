import React from 'react';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import styled from 'styled-components/native';
import Subtitle from '../Subtitle';
import Title from '../Title';
import { theme } from '../../resources';

const Container = styled.View`
  flex-direction: row;
  padding: 0px 0px;
  align-items: center;
`;

const Images = styled.Image`
  height: ${props => props.height || wp('52%')}px;
  width: ${wp('30%')}px;
  margin-left: -1.6px;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
`;

const Borders = styled.View`
  border-bottom-width: 1px;
  border-color: ${theme.sixthColor};
  margin: 7px 0px 7px 0px;
`

const SectionTexts = styled.View`
  flex: 1;
`;
const SectionImages = styled.View`
  margin-right: 10px;
  margin-left: -15px;
  margin-top: -15px;
  margin-bottom: -15px;
`;

const SectionVw = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  flex-wrap: wrap;
`

const SectionTex = styled.View`
`

const EventInfo = props => {
  return (
    <Container>
      <SectionImages>
        <Images source={props.image} height={props.height} resizeMode="cover" />
      </SectionImages>
      <SectionTexts>
        {props.bordes ? (
          <>
            <Subtitle
              color={theme.sixthColor}
              size={`${wp('4%')}`}
              margin="-5px 0px 1px 0px"
              align="left">
              {props.autor}
            </Subtitle>
            <Subtitle
              color={theme.sixthColor}
              weight="bold"
              size={`${wp('5%')}`}
              margin="0px"
              numberOfLines={1}
              align="left">
              {props.subTitle}
            </Subtitle>
            <Borders />
            <Title
              color={theme.sixthColor}
              numberOfLines={2}
              size={`${wp('6%')}`}
              margin="0px"
              align="left">
              {props.title}
            </Title>
            <Subtitle
              color={theme.sixthColor}
              size={`${wp('4%')}`}
              margin="0px"
              align="left">
              {props.overTitle}
            </Subtitle>
          </>
        ) : (
            <>
              <Title
                color={theme.sixthColor}
                numberOfLines={2}
                size={`${wp('6%')}`}
                margin="0px"
                align="left">
                {props.title}
              </Title>
              <Subtitle
                color={theme.sixthColor}
                size={`${wp('4%')}`}
                margin="0px"
                align="left">
                {props.overTitle}
              </Subtitle>
              <Borders />
              <SectionVw>
                {props.artist ? (
                  <Subtitle
                    color={theme.sixthColor}
                    size={`${wp('6%')}`}
                    margin="0px"
                    weight="bold"
                    align="left">
                    {props.precoLive}
                  </Subtitle>
                ) : (
                    undefined
                  )}
                <SectionTex>
                  <Subtitle
                    color={theme.primaryColor}
                    size={`${wp('4%')}`}
                    margin="0px"
                    align="left">
                    {props.valorLive}
                  </Subtitle>
                  <Subtitle
                    color={theme.primaryColor}
                    size={`${wp('5%')}`}
                    margin="0px 0px 5px 0px"
                    weight="bold"
                    align="left">
                    {props.valor1}
                  </Subtitle>
                </SectionTex>
                <SectionTex>
                  <Subtitle
                    color={theme.sixthColor}
                    size={`${wp('4%')}`}
                    margin="0px"
                    align="left">
                    {props.ingresso}
                  </Subtitle>
                  <Subtitle
                    color={theme.sixthColor}
                    size={`${wp('5%')}`}
                    margin="0px 0px 5px 0px"
                    weight="bold"
                    align="left">
                    {props.valor2}
                  </Subtitle>
                </SectionTex>
                <SectionTex>
                  <Subtitle
                    color={theme.sixthColor}
                    size={`${wp('4%')}`}
                    margin="0px"
                    align="left">
                    {props.faturamento}
                  </Subtitle>
                  <Subtitle
                    color={theme.sixthColor}
                    weight="bold"
                    size={`${wp('5%')}`}
                    margin="0px 0px 5px 0px"
                    align="left">
                    {props.valor3}
                  </Subtitle>
                </SectionTex>
              </SectionVw>
            </>
          )}
        {props.children}
      </SectionTexts>
    </Container>
  );
};

export default EventInfo;
