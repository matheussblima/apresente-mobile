import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styled from 'styled-components/native';

const Container = styled.View`
  width: ${wp('6%')}px;
  height: ${wp('6%')}px;
  background-color: ${props => props.theme.ninethColor};
  justify-content: center;
  align-items: center;
  border-radius: ${wp('6%')}px;
`;

const Text = styled.Text`
  color: ${props => props.theme.fourthColor};
  font-size: ${wp('3%')}px;
  font-weight: bold;
`;

export default props => {
  return (
    <Container style={props.style}>
      <Text>{props.value || ''}</Text>
    </Container>
  );
};
