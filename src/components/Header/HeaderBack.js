import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styled from 'styled-components/native';
import {images} from '../../resources';
import Logo from '../Logo';

const Container = styled.View`
  flex-direction: row;
  padding-top: 6px;
  padding-bottom: 6px;
  padding-left: ${props => props.paddingHorizontal || wp('2%')}px;
  padding-right: ${props => props.paddingHorizontal || wp('2%')}px;
  align-items: flex-start;
`;

const SectionButton = styled.View`
  display: flex;
  flex-direction: row;
`;

const SectionLogo = styled.View``;

const Image = styled.Image`
  width: ${wp('6%')}px;
  height: ${wp('6%')}px;
  tint-color: ${props => props.theme.sixthColor};
`;

const Touch = styled.TouchableOpacity`
  width: ${wp('10%')}px;
  height: ${wp('10%')}px;
  display: flex;
  justify-content: center;
`;

const HeaderBack = props => {
  return (
    <Container>
      {props.isBack ? (
        <SectionButton>
          <Touch {...props} activeOpacity={0.4} onPress={props.onPress}>
            <Image source={images.back} resizeMode="contain" />
          </Touch>
        </SectionButton>
      ) : (
        <SectionButton />
      )}
      {props.isLogo ? (
        <SectionLogo>
          <Logo width={wp('34%')} height={wp('8%')} />
        </SectionLogo>
      ) : (
        undefined
      )}
      {props.children}
    </Container>
  );
};

HeaderBack.defaultProps = {
  isBack: true,
  isLogo: true,
};

export default HeaderBack;
