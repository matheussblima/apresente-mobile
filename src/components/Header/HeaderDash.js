import React from 'react';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import styled from 'styled-components/native';
import { images, theme } from '../../resources';
import {
    Title,
    Button,
} from '../../components';
import { texts } from '../../config';


const TouchableOpacity = styled.TouchableOpacity`
`

const Image = styled.Image`
`

const SectionBtn = styled.View`
  display: flex;
  padding: 15px 0px 10px 0px;
  flex-direction: row;
`

const SectionCadst = styled.View`
  display: flex;
  flex-direction: row;
  padding-top: 10px;
  padding-bottom: 20px;
`

const SectionPersonal = styled.View`
  display: flex;
  flex-direction: row;
  padding-bottom: 10px;
  border-bottom-width: 1px;
  border-color: #707070;
`
const ListSect = styled.View`
  margin-left: 10px;
  width: 65%;
`

const ListDate = styled.View`
  display: flex;
  flex-direction: row;
`
const Header = styled.View``;

const HeaderDash = props => {
    return (
        <Header>
            <SectionCadst>
                <TouchableOpacity onPress={props.back}>
                    <Image
                        source={images.arrowLeft} />
                </TouchableOpacity>
                <Title
                    size={wp('8%')}
                    color={theme.tenthColor}
                    align="left"
                    margin="0px 5px 0px 20px">
                    {`${texts.myLive} ${texts.myLive2}`}
                </Title>
            </SectionCadst>
            <SectionPersonal>
                <Image style={{
                    width: wp('23%'),
                    borderRadius: 100,
                    height: wp('23%')

                }}
                    source={images.exemple} />
                <ListSect>
                    <Title
                        numberOfLines={2}
                        size={wp('6%')}
                        color={theme.tenthColor}
                        align="left"
                        margin="0px ">
                        Lavagem do Senhor do Bomfim 2021
                    </Title>
                    <ListDate>
                        <Button
                            sizeText={wp('3.7%')}
                            margin="0px 0px 0px 0px"
                            backgroundColor={theme.seventhColors}
                            title="Sábado"
                            paddigBtn="0px 7px"
                            fontWeight="bold"
                            type="rounded"
                        />
                        <Title
                            numberOfLines={1}
                            size={wp('4%')}
                            color={theme.fourteenthColor}
                            align="left"
                            margin="5px 0px 0px 5px">
                            16 mar 2020 - 22pm
                        </Title>
                    </ListDate>
                </ListSect>
            </SectionPersonal>
            <SectionBtn>
                <Button
                    onPress={props.routePress}
                    height="26px"
                    padding="1px 7px"
                    margin="0px 5px 0px 0px"
                    sizeText={`${wp('3%')}px`}
                    title="Dashboard"
                    backgroundActive={props.activeDash}
                    color={props.activeColorDash}
                    type="borded"
                    fontWeight="bold"
                />
                <Button
                    onPress={props.routePress2}
                    height="26px"
                    padding="1px 7px"
                    margin="0px 5px 0px 0px"
                    sizeText={`${wp('3%')}px`}
                    title="Participantes"
                    backgroundActive={props.activePart}
                    color={props.activeColorPart}
                    type="borded"
                    fontWeight="bold"
                />
                <Button
                    onPress={props.routePress3}
                    height="26px"
                    padding="1px 7px"
                    margin="0px 5px 0px 0px"
                    sizeText={`${wp('3%')}px`}
                    title="Editar Live"
                    backgroundActive={props.activeEdit}
                    color={props.activeColorEdit}
                    type="borded"
                    fontWeight="bold"
                />
                <Button
                    onPress={props.routePress4}
                    height="26px"
                    padding="1px 7px"
                    sizeText={`${wp('3%')}px`}
                    title="Transmitir"
                    backgroundActive={theme.twelfthColor}
                    color={theme.fourthColor}
                    borderColor={theme.twelfthColor}
                    type="borded"
                    fontWeight="bold"
                />
            </SectionBtn>
            <Title
                numberOfLines={2}
                size={wp('9%')}
                color={theme.primaryColor}
                align="left"
                margin="5px 0px">
                {props.title}
            </Title>
        </Header>
    )
}

export default HeaderDash;