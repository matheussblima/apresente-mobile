import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styled from 'styled-components/native';
import {images} from '../../resources';
import Logo from '../Logo';

const Container = styled.View`
  flex-direction: row;
  padding-top: 6px;
  padding-bottom: 6px;
  padding-left: ${props => props.paddingHorizontal || wp('5%')}px;
  padding-right: ${props => props.paddingHorizontal || wp('5%')}px;
  background-color: ${props => props.theme.fourthColor};
`;

const SectionButton = styled.View`
  flex-direction: row;
  flex: 1;
  justify-content: flex-end;
  align-items: center;
`;

const SectionLogo = styled.View``;

const Image = styled.Image`
  width: ${wp('5%')}px;
  height: ${wp('5%')}px;
`;

const Touch = styled.TouchableOpacity`
  background-color: ${props => props.theme.thirdColor};
  border-radius: ${wp('6%')}px;
  margin: 0 6px;
  align-items: center;
  padding: 4px;
  justify-content: center;
  width: ${wp('10%')}px;
  height: ${wp('10%')}px;
`;

const HeaderClose = props => {
  return (
    <Container>
      <SectionLogo>
        <Logo />
      </SectionLogo>
      <SectionButton>
        <Touch {...props} activeOpacity={0.4} onPress={props.onPressSearch}>
          <Image source={images.search} resizeMode="contain" />
        </Touch>
      </SectionButton>
    </Container>
  );
};

export default HeaderClose;
