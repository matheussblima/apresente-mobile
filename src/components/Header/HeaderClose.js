import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styled from 'styled-components/native';
import {images} from '../../resources';

const Container = styled.SafeAreaView``;

const Section = styled.View`
  margin-top: 8px;
`;

const SectionImage = styled.View`
  align-items: flex-end;
  padding: 6px 20px;
`;

const Image = styled.Image`
  width: ${wp('6%')}px;
  height: ${wp('6%')}px;
  tint-color: ${props => props.colorIcon || props.theme.tenthColor};
`;

const Touch = styled.TouchableOpacity``;

const HeaderClose = props => {
  return (
    <Container style={props.style}>
      <SectionImage>
        <Touch {...props} activeOpacity={0.4}>
          <Image
            colorIcon={props.colorIcon}
            source={images.close}
            resizeMode="contain"
          />
        </Touch>
      </SectionImage>
      <Section>{props.children}</Section>
    </Container>
  );
};

export default HeaderClose;
