import styled from 'styled-components/native';

export default styled.View`
  border-bottom-width: 1px;
  border-color: ${props => props.color || props.theme.sixthColor};
  margin: 4px 0px;
`;
