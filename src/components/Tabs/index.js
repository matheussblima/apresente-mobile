import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styled from 'styled-components/native';
import {images, theme} from '../../resources';
import Title from '../Title';

const Container = styled.View``;

const SectionButtons = styled.View`
  flex-direction: row;
  width: 100%;
`;

const Touch = styled.TouchableOpacity`
  align-items: center;
  flex: 1;
  background-color: ${props =>
    props.selected
      ? props.selectedColor || props.theme.eighthColor
      : 'transparent'};
`;

const Image = styled.Image`
  flex-direction: row;
  width: 80px;
  height: 80px;
`;

export default props => {
  return (
    <Container>
      <SectionButtons>
        <Touch
          style={props.styleTabOne}
          selectedColor={props.selectedColor}
          activeOpacity={0.6}
          disabled={props.disabled === 1}
          selected={props.selected === 1}
          onPress={() => props.onPressTab(1)}>
          {props.tabOneImage ? (
            <Image
              style={props.disabled === 1 ? {opacity: 0.4} : {}}
              source={images.tabOneImage}
              resizeMode="contain"
            />
          ) : (
            undefined
          )}
          <Title
            style={props.disabled === 1 ? {opacity: 0.4} : {}}
            size={wp('4.5%')}
            color={
              props.selected === 1
                ? props.textColorSelected || theme.sixthColor
                : props.textColorUnSelected
            }>
            {props.tabOneTitle}
          </Title>
        </Touch>
        <Touch
          style={props.styleTabTwo}
          selectedColor={props.selectedColor}
          activeOpacity={0.6}
          disabled={props.disabled === 2}
          selected={props.selected === 2}
          onPress={() => props.onPressTab(2)}>
          {props.tabTwoImage ? (
            <Image
              style={props.disabled === 2 ? {opacity: 0.4} : {}}
              source={props.tabTwoImage}
              resizeMode="contain"
            />
          ) : (
            undefined
          )}
          <Title
            style={props.disabled === 2 ? {opacity: 0.4} : {}}
            size={wp('4.5%')}
            color={
              props.selected === 2
                ? props.textColorSelected || theme.sixthColor
                : props.textColorUnSelected
            }>
            {props.tabTwoTitle}
          </Title>
        </Touch>
      </SectionButtons>
      {props.children}
    </Container>
  );
};
