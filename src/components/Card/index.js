import styled from 'styled-components/native';

export default styled.View`
  background-color: #fff;
  border-radius: 16px;
  padding: 24px;
  shadow-color: ${props => props.theme.sixthColor};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.22;
  shadow-radius: 2.22px;
`;
