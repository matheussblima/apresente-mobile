import styled from 'styled-components/native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

export default styled.Text`
  color: ${props => props.color || props.theme.primaryColor};
  text-align: ${props => props.align || 'center'};
  font-size: ${props => props.size || wp('5%')}px;
  margin: ${props => props.margin || `${wp('2%')}px`};
  font-weight: ${props => props.weight || 'normal'};
  padding-bottom: ${props => props.paddBottom || '0'};
  font-style: ${props => props.fontStyle || 'normal'};
`;
