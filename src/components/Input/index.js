import React from 'react';
import styled from 'styled-components/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import Title from '../Title';
import {theme} from '../../resources';

const Container = styled.View`
  flex: 1;
  margin-left: ${props => props.marginLeft || '0px'};
`;
const Input = styled.TextInput`
  font-size: ${props => props.fontSize || wp('5%')}px;
  border-bottom-width: 1px;
  border-color: ${props =>
    !props.editable
      ? props.theme.sixthColor
      : props.theme.primaryColor || props.underlineColor};
  padding: ${props => props.padding || '10px 12px'};
  color: ${props =>
    !props.editable
      ? props.theme.sixthColor
      : props.theme.primaryColor || props.underlineColor};
  margin: 12px 0px;
`;

const TextInput = props => {
  return (
    <Container marginLeft={props.marginLeft}>
      <Title
        size={wp('3.4%')}
        align="left"
        color={theme.seventhColors}
        margin="0px 0px -12px 0px">
        {props.label}
      </Title>
      <Input {...props} underlineColorAndroid="rgba(0,0,0,0)" />
    </Container>
  );
};

TextInput.defaultProps = {
  editable: true,
};

export default TextInput;
