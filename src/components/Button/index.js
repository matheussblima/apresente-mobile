import React from 'react';
import { ActivityIndicator } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import styled, { css } from 'styled-components/native';
import { theme } from '../../resources';

const TouchableOpacity = styled.TouchableOpacity`

  align-items: ${props => props.align || 'center'};
  margin: ${props => props.margin || '0px'};
  justify-content: center;
  padding: ${props => props.padding || '8px'}; 

  ${props => props.type === 'transparent' && css``}
  ${props =>
    props.type === 'normal' &&
    css`
      height: ${props.height || '60px'};
      background-color: ${props.backgroundColor || props.theme.primaryColor};
      border-radius: ${props.radius || `${wp('2%')}px`};
    `}

  ${props =>
    props.type === 'borded' &&
    css`
      height: ${props.height || '60px'};
      border-radius: ${props.radius || `${wp('2%')}px`};
      border-color: ${props.borderColor || props.theme.primaryColor};
      background-color: ${props.backgroundActive || props.theme.fourthColor};
      border-width: 1px;
    `}

    ${props =>
    props.type === 'rounded' &&
    css`
        height: ${props.height || '28px'};
        border-radius: ${wp('6%')}px;
        padding: ${props => props.paddigBtn || '0px 20px'};
        background-color: ${props.backgroundColor || props.theme.primaryColor};
      `}
`;

const Text = styled.Text`

  font-weight: ${props => props.fontWeight || 'normal'};
  font-size:  ${props => props.sizeText || `${hp('2.4%')}px`};

  ${props =>
    props.type === 'normal' &&
    css`
      color: ${props.color || props.theme.fourthColor};
    `}

  ${props =>
    props.type === 'transparent' &&
    css`
      color: ${props.color || props.theme.primaryColor};
      text-decoration: none;
      text-decoration-color: ${props.color || props.theme.primaryColor};
    `}

  ${props =>
    props.type === 'borded' &&
    css`
      color: ${props.color || props.theme.primaryColor};
    `}

  ${props =>
    props.type === 'rounded' &&
    css`
      color: ${props.color || props.theme.fourthColor};
      font-weight: ${props.fontWeight || 'normal'};
      font-size: ${props.sizeText || `${hp('2%')}px`};
    `}
 
`;

const Icon = styled.Image`
  width: ${wp('6%')}px;
  height: ${wp('6%')}px;
  margin-left: ${wp('4%')}px;
  margin-right: ${wp('4%')}px;
`;

const Row = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const Button = props => {
  return (
    <TouchableOpacity disabled={props.loading} {...props} activeOpacity={0.8}>
      {props.loading ? (
        <ActivityIndicator
          size="small"
          color={
            props.type === 'borded' || props.type === 'transparent'
              ? theme.primaryColor
              : theme.fourthColor
          }
        />
      ) : (
          <Row>
            {props.iconLeft ? <Icon source={props.iconLeft} /> : undefined}
            <Text {...props}>{props.title}</Text>
            {props.iconRight ? <Icon source={props.iconRight} /> : undefined}
          </Row>
        )}
    </TouchableOpacity>
  );
};

Button.defaultProps = {
  type: 'normal',
};

export default Button;
