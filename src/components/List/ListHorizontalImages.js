import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styled from 'styled-components/native';
import moment from 'moment';
import Subtitle from '../Subtitle';
import Division from '../Division';
import {theme} from '../../resources';
import Avatar from '../Avatar';

const FlatList = styled.FlatList``;
const Container = styled.View`
  margin-top: ${wp('5%')}px;
  padding-bottom: 8px;
  shadow-color: ${props => props.theme.sixthColor};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.22;
  shadow-radius: 2.22px;
`;
const Touchable = styled.TouchableOpacity`
  margin-right: ${wp('3%')}px;
`;

const Image = styled.Image`
  width: ${wp('66%')}px;
  height: ${wp('40%')}px;
  border-top-left-radius: 24px;
  border-top-right-radius: 24px;
  background-color: ${props => props.theme.sixthColor};
`;

const Section = styled.View`
  background-color: ${props => props.theme.fourthColor};
  border-bottom-left-radius: 24px;
  border-bottom-right-radius: 24px;
  width: ${wp('66%')}px;
  height: ${wp('38%')}px;
  padding: 8px;
`;

const SectionBottom = styled.View`
  flex: 1;
  overflow: hidden;
  flex-direction: row;
  display: flex;
`;

const SectionInfo = styled.View`
  flex: 1;
  display: flex;
  margin-top: 8px;
  justify-content: center;
`;

const renderItem = ({item}, props) => {
  return (
    <Container>
      <Touchable onPress={() => props.onPress(item)} activeOpacity={0.8}>
        <Image resizeMode="cover" source={item.image} />
        <Section>
          <Subtitle
            margin={`0px 0px 0px ${wp('2%')}px`}
            color={theme.sixthColor}
            weight="bold"
            align="left"
            numberOfLines={1}>
            {item.nome}
          </Subtitle>
          <Subtitle
            size={wp('4%')}
            margin={`0px 0px ${wp('2%')}px ${wp('2%')}px`}
            color={theme.sixthColor}
            align="left"
            numberOfLines={1}>
            {moment(item.data).format('dddd, DD MMM YYYY HH:mm')}
          </Subtitle>
          <Division />
          <SectionBottom>
            <Avatar
              size={wp('12%')}
              source={{
                uri:
                  'https://noverbal.es/uploads/blog/rostro-de-un-criminal.jpg',
              }}
            />
            <SectionInfo>
              <Subtitle
                weight="bold"
                color={theme.sixthColor}
                margin={`0px 0px 0px ${wp('2%')}px`}
                align="left"
                numberOfLines={1}>
                Murilo Couto
              </Subtitle>
              <Subtitle
                size={wp('4%')}
                margin={`0px 0px ${wp('2%')}px ${wp('2%')}px`}
                color={theme.sixthColor}
                align="left"
                numberOfLines={1}>
                26 Lives | 234 espectadores
              </Subtitle>
            </SectionInfo>
          </SectionBottom>
        </Section>
      </Touchable>
    </Container>
  );
};

const renderKeys = item => item.id.toString();

const ListHorizontalImages = props => {
  return (
    <FlatList
      horizontal
      showsHorizontalScrollIndicator={false}
      data={props.data}
      renderItem={items => renderItem(items, props)}
      keyExtractor={renderKeys}
    />
  );
};

export default ListHorizontalImages;
