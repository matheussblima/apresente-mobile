import React from 'react';
import {FlatList} from 'react-native';
import styled from 'styled-components/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import Title from '../Title';
import Subtitle from '../Subtitle';
import {theme, images} from '../../resources';

const Container = styled.View`
  padding: 8px 0px;
  flex-direction: row;
  align-items: center;
  border-bottom-width: 1px;
  border-color: ${props => props.theme.sixthColor};
  width: 100%;
`;

const SectionName = styled.View`
  flex: 1.5;
  justify-content: center;
  padding: 4px;
`;

const SectionValue = styled.View`
  justify-content: center;
  padding: 4px;
`;

const SectionIcon = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  padding: 4px;
`;

const Touch = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
`;

const Icon = styled.Image`
  width: 30px;
  height: 30px;
`;

const renderItem = ({item}, props) => {
  return (
    <Container>
      <SectionName>
        <Title
          numberOfLines={2}
          color={theme.sixthColor}
          size={`${wp('4%')}`}
          align="left"
          margin="0">
          {item.type}
        </Title>
        <Subtitle
          numberOfLines={2}
          color={theme.sixthColor}
          size={`${wp('4%')}`}
          align="left"
          margin="0">
          {item.name}
        </Subtitle>
      </SectionName>
      <SectionValue>
        <Title
          numberOfLines={1}
          color={theme.sixthColor}
          align="left"
          margin="0">
          {item.qtd}
        </Title>
      </SectionValue>
      <SectionIcon>
        <Touch activeOpacity={0.5} onPress={() => props.onPressDelete(item)}>
          <Icon source={images.trash} resizeMode="contain" />
        </Touch>
      </SectionIcon>
    </Container>
  );
};

const renderKeys = item => item.id.toString();
const ListSale = props => {
  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      data={props.data}
      renderItem={items => renderItem(items, props)}
      keyExtractor={renderKeys}
    />
  );
};

ListSale.defaultProps = {
  setValue: () => 0,
};

export default ListSale;
