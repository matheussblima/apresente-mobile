import React from 'react';
import {FlatList} from 'react-native';
import styled from 'styled-components/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import Title from '../Title';
import Subtitle from '../Subtitle';
import {theme, images} from '../../resources';
import currencyFormat from '../../utility/currencyFormat';

const Container = styled.View`
  padding: 8px 0px;
  flex-direction: row;
  align-items: center;
  border-bottom-width: 1px;
  border-color: ${props => props.theme.sixthColor};
  width: 100%;
`;

const SectionName = styled.View`
  flex: 1.5;
  justify-content: center;
  padding: 4px;
`;

const SectionValue = styled.View`
  justify-content: center;
  padding: 4px;
`;

const SectionCount = styled.View`
  flex: 1.5;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 4px;
`;

const Touch = styled.TouchableOpacity`
  width: 30px;
  height: 30px;
  justify-content: center;
  align-items: center;
  border-radius: 15px;
  border: 1px solid ${props => props.theme.primaryColor};
  margin: 6px;
`;

const Icon = styled.Image`
  height: ${wp('4%')}px;
  width: ${wp('4%')}px;
  tint-color: ${props => props.theme.primaryColor};
`;

const renderItem = ({item}, props) => {
  return (
    <Container>
      <SectionName>
        <Title
          numberOfLines={2}
          color={theme.sixthColor}
          size={`${wp('4%')}`}
          align="left"
          margin="0">
          {item.type}
        </Title>
        <Subtitle
          numberOfLines={2}
          color={theme.sixthColor}
          size={`${wp('4%')}`}
          align="left"
          margin="0">
          {item.name}
        </Subtitle>
      </SectionName>
      <SectionValue>
        <Title
          numberOfLines={1}
          color={theme.sixthColor}
          align="left"
          margin="0">
          {currencyFormat(parseFloat(item.value, false))}
        </Title>
        <Subtitle
          numberOfLines={1}
          size={`${wp('4%')}`}
          align="left"
          margin="0">
          sem taxa
        </Subtitle>
      </SectionValue>
      <SectionCount>
        <Touch activeOpacity={0.5} onPress={() => props.onPressLess(item)}>
          <Icon source={images.minus} />
        </Touch>
        <Title color={theme.sixthColor} align="left" margin="0">
          {(props.setValue && props.setValue(item)) || 0}
        </Title>
        <Touch activeOpacity={0.5} onPress={() => props.onPressMore(item)}>
          <Icon source={images.more} />
        </Touch>
      </SectionCount>
    </Container>
  );
};

const renderKeys = item => item.id.toString();
const ListSale = props => {
  return (
    <FlatList
      scrollEnabled={false}
      showsVerticalScrollIndicator={false}
      data={props.data}
      renderItem={items => renderItem(items, props)}
      keyExtractor={renderKeys}
    />
  );
};

ListSale.defaultProps = {
  setValue: () => 0,
};

export default ListSale;
