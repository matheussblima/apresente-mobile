import React from 'react';
import {ActivityIndicator} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styled from 'styled-components/native';
import Subtitle from '../Subtitle';
import Title from '../Title';
import {theme} from '../../resources';

const ContainerList = styled.View`
  margin-bottom: ${wp('5%')}px;
  background-color: ${props => props.theme.fourthColor};
  flex: 1;
  border-radius: 8px;
  shadow-color: ${props => props.theme.sixthColor};
  shadow-offset: 0px 1px;
  shadow-opacity: 0.22;
  shadow-radius: 2.22px;
  elevation: 3;
`;

const Touchable = styled.TouchableOpacity``;

const Image = styled.Image`
  height: ${wp('40%')}px;
  border-radius: 8px;
  background-color: ${props => props.theme.sixthColor};
`;

const SectionImages = styled.View`
  border-radius: 8px;
`;

const SectionTexts = styled.View`
  background-color: ${props => props.theme.fourthColor};
  flex: 1;
  padding: 10px 8px;
  border-radius: 8px;
`;

const Container = styled.ScrollView``;

const renderItem = (item, props) => {
  return (
    <ContainerList key={item.id}>
      <Touchable onPress={() => props.onPress(item)} activeOpacity={0.8}>
        <SectionImages>
          <Image resizeMode="cover" source={item.image} />
        </SectionImages>
        <SectionTexts>
          <Subtitle
            color={theme.sixthColor}
            size={`${wp('4%')}`}
            margin="0px"
            align="left">
            {item.overTitle}
          </Subtitle>
          <Title
            color={theme.sixthColor}
            numberOfLines={2}
            size={`${wp('5%')}`}
            margin="0px"
            align="left">
            {item.title}
          </Title>
          <Subtitle
            color={theme.sixthColor}
            size={`${wp('4%')}`}
            margin="0px"
            align="left">
            {item.subTitle}
          </Subtitle>
        </SectionTexts>
      </Touchable>
    </ContainerList>
  );
};

const ListEvent = props => {
  return (
    <>
      <Container>{props.data.map(items => renderItem(items, props))}</Container>
      {props.loading ? (
        <ActivityIndicator size="large" color={theme.sixthColor} />
      ) : (
        undefined
      )}
    </>
  );
};

export default ListEvent;
