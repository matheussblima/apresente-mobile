import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styled from 'styled-components/native';

const Container = styled.View``;

const ContainerItem = styled.View`
  margin-top: ${wp('5%')}px;
  flex-direction: row;
  flex-wrap: wrap;
`;
const Touchable = styled.TouchableOpacity``;

const Image = styled.Image`
  width: ${wp('30%')}px;
  height: ${wp('30%')}px;
  background-color: ${props => props.theme.sixthColor};
`;

const renderItem = props => {
  return (
    <ContainerItem>
      {props.data.map((item, key) => {
        return (
          <Touchable
            key={key}
            onPress={() => props.onPress(item)}
            activeOpacity={0.8}>
            <Image resizeMode="cover" source={item.image} />
          </Touchable>
        );
      })}
    </ContainerItem>
  );
};

const ListSideBySideImages = props => {
  return <Container>{renderItem(props)}</Container>;
};

export default ListSideBySideImages;
