import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styled from 'styled-components/native';

const Container = styled.View``;

const ContainerItem = styled.View`
  margin-top: ${wp('5%')}px;
  flex-direction: row;
  flex-wrap: wrap;
`;

const renderItem = props => {
  return (
    <ContainerItem>
      {props.data.map((item, index) => {
        return props.renderItem(item, index);
      })}
    </ContainerItem>
  );
};

const ListSideBySide = props => {
  return <Container>{renderItem(props)}</Container>;
};

ListSideBySide.defaultProps = {
  data: [],
  renderItem: () => {},
};

export default ListSideBySide;
