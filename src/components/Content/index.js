import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styled from 'styled-components/native';

const Container = styled.View`
  padding-left: ${props => props.paddingHorizontal || wp('4.9%')}px;
  padding-right: ${props => props.paddingHorizontal || wp('4.9%')}px;
`;

const Content = props => {
  return <Container style={props.style}>{props.children}</Container>;
};

export default Content;
