import React from 'react';
import styled from 'styled-components/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {theme} from '../../resources';

const Container = styled.View`
  margin: 12px 0px;
  flex-direction: row;
  align-items: center;
`;
const RNSwitch = styled.Switch``;

const Label = styled.Text`
  margin-left: 8px;
  font-size: ${props => props.fontSize || wp('4%')}px;
  color: ${props => props.theme.primaryColor || props.color};
`;

const Switch = props => {
  return (
    <Container>
      <RNSwitch
        {...props}
        thumbColor={theme.fourthColor || props.color}
        trackColor={{true: theme.primaryColor || props.color}}
      />
      <Label {...props}>{props.title}</Label>
    </Container>
  );
};

export default Switch;
