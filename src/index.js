import React from 'react';
import {View, ActivityIndicator, YellowBox} from 'react-native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';
import {ThemeProvider} from 'styled-components/native';
import configureStore, {persistor} from './redux/store';
import {Routes} from './config';
import {theme} from './resources';

require('moment/locale/pt-br');

YellowBox.ignoreWarnings(['VirtualizedLists should never be nested']);

const store = configureStore();

const LoadingView = () => {
  return (
    <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
      <ActivityIndicator size="small" color="#000" />
    </View>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={<LoadingView />} persistor={persistor}>
        <ThemeProvider theme={theme}>
          <Routes />
        </ThemeProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
