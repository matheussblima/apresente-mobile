import axios from 'axios';

const enviroments = {
  BASE_URL: 'https://api.safeticket.com.br/v1/',
  SELLERID: 'SS03-454215465-6545',
  API_TOKEN_SAFETICK: '40110eef5abdff3409961fdcf32d0e00',
};

export default axios.create({
  baseURL: enviroments.BASE_URL,
  headers: {
    Authorization: enviroments.API_TOKEN_SAFETICK,
    SellerId: enviroments.SELLERID,
    'Content-Type': 'application/json',
  },
  validateStatus: status => status <= 500,
});
