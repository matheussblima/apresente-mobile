const videoConfig = {
  preset: 1,
  bitrate: 500000,
  profile: 1,
  fps: 15,
  videoFrontMirror: false,
};

const audioConfig = {bitrate: 32000, profile: 1, samplerate: 44100};

const outputUrl = '';

export default {videoConfig, audioConfig, outputUrl};
