import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Image, Platform} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import OneSignal from 'react-native-onesignal';
import DeviceInfo from 'react-native-device-info';
import {setPush} from '../redux/push';

import {
  Login,
  SingUp,
  Recovery,
  Live,
  InvoiceError,
  RecoveryVerifyEmail,
  CardData,
  Home,
  Event,
  Invoice,
  Filter,
  Config,
  Search,
  Cards,
  CardEdit,
  HelpInfo,
  NewLiveBank,
  Tickets,
  MyLives,
  NewLiveData,
  Help,
  TicketRequest,
  TermsPolicies,
  SelectCard,
  EditPerfil,
  Artist,
  Dashboard,
  Onboarding,
  Transmit,
} from '../screens';
import {images} from '../resources';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function Tabs() {
  const auth = useSelector(state => state.auth);
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: '#0093D0',
        inactiveTintColor: 'white',
        activeBackgroundColor: '#4B4B4B',
        inactiveBackgroundColor: '#4B4B4B',
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          let image;

          switch (route.name) {
            case 'Home':
              image = images.home;
              break;
            case 'Search':
              image = images.search;
              break;
            case 'Compras':
              image = images.ticket;
              break;
            case 'MyLives':
              image = images.cloud;
              break;
            case 'Perfil':
              image = images.user;
              break;
            default:
              break;
          }

          return (
            <Image
              resizeMode="contain"
              source={image}
              style={{width: size, height: size, tintColor: color}}
            />
          );
        },
      })}>
      <Tab.Screen
        options={{
          tabBarLabel: 'Home',
        }}
        name="Home"
        component={Home}
      />
      <Tab.Screen name="Search" component={Search} />
      <Tab.Screen
        name="Compras"
        component={auth.isSuccessAuth ? Tickets : Login}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Minhas lives',
        }}
        name="MyLives"
        component={auth.isSuccessAuth ? MyLives : Login}
      />
      <Tab.Screen
        name="Perfil"
        component={auth.isSuccessAuth ? Config : Login}
      />
    </Tab.Navigator>
  );
}

function App() {
  const auth = useSelector(state => state.auth);
  const {card} = useSelector(state => state.card);
  const push = useSelector(state => state.push);
  const dispatch = useDispatch();

  useEffect(() => {
    // Remove this method to stop OneSignal Debugging
    OneSignal.setLogLevel(6, 0);

    // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
    OneSignal.init('5bbf4126-5e4b-4fa4-b2c0-73834dcbfa7a');

    OneSignal.addEventListener('received', onReceived);
    OneSignal.addEventListener('opened', onOpened);
    OneSignal.addEventListener('ids', onIds);
    return () => {
      OneSignal.removeEventListener('received', onReceived);
      OneSignal.removeEventListener('opened', onOpened);
      OneSignal.removeEventListener('ids', onIds);
    };
  }, []);

  const onReceived = notification => {
    console.log('Notification received: ', notification);
  };

  const onOpened = openResult => {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  };

  const onIds = device => {
    dispatch(
      setPush({
        hashPush: device.userId,
        system: Platform.OS,
        systemVersion: Platform.Version,
        brand: `${DeviceInfo.getBrand()}/${DeviceInfo.getModel()}`,
      }),
    );
    console.log({
      hashPush: device.userId,
      system: Platform.OS,
      systemVersion: Platform.Version,
      brand: `${DeviceInfo.getBrand()}/${DeviceInfo.getModel()}`,
    });
  };

  let paymentRoute = Login;

  if (auth.isSuccessAuth) {
    if (card.length > 0) {
      paymentRoute = SelectCard;
    } else {
      paymentRoute = CardData;
    }
  }

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={push.isSuccess ? 'Home' : 'Wellcome'}
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="SingUp" component={SingUp} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="InvoiceError" component={InvoiceError} />
        <Stack.Screen name="Transmit" component={Transmit} />
        <Stack.Screen name="Onboarding-1" component={Onboarding.Step1} />
        <Stack.Screen name="Onboarding-2" component={Onboarding.Step2} />
        <Stack.Screen name="Onboarding-3" component={Onboarding.Step3} />
        <Stack.Screen name="Onboarding-4" component={Onboarding.Step4} />
        <Stack.Screen name="Wellcome" component={Onboarding.Wellcome} />
        <Stack.Screen name="Live" component={Live} />
        <Stack.Screen name="Home" component={Tabs} />
        <Stack.Screen name="NewLiveBank" component={NewLiveBank} />
        <Stack.Screen name="NewLiveData" component={NewLiveData} />
        <Stack.Screen name="Filter" component={Filter} />
        <Stack.Screen name="Payment" component={paymentRoute} />
        <Stack.Screen name="HelpInfo" component={HelpInfo} />
        <Stack.Screen name="Invoice" component={Invoice} />
        <Stack.Screen name="SelectCard" component={SelectCard} />
        <Stack.Screen name="Event" component={Event} />
        <Stack.Screen name="Recovery" component={Recovery} />
        <Stack.Screen name="CardData" component={CardData} />
        <Stack.Screen name="Search" component={Search} />
        <Stack.Screen name="CardEdit" component={CardEdit} />
        <Stack.Screen name="Cards" component={Cards} />
        <Stack.Screen name="EditPerfil" component={EditPerfil} />
        <Stack.Screen name="Help" component={Help} />
        <Stack.Screen name="TermsPolicies" component={TermsPolicies} />
        <Stack.Screen name="TicketRequest" component={TicketRequest} />
        <Stack.Screen
          name="RecoveryVerifyEmail"
          component={RecoveryVerifyEmail}
        />
        <Stack.Screen name="Artist" component={Artist} />
        <Stack.Screen name="Dashboard" component={Dashboard} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
